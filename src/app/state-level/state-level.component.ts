import { Component, OnInit} from '@angular/core';
import { MouseEvent } from '@agm/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { GraphsService } from '../../services/graphs/graphs.service';
import { GetDigitalAssetType } from './../digital-asset-dashboard/digital-asset-dashboard.services';

declare var $: any;
@Component({
  selector: 'app-state-level',
  templateUrl: './state-level.component.html',
  styleUrls: ['./state-level.component.css']
})
export class StateLevelComponent implements OnInit {
  severity1: boolean;
  deficientVar: boolean;
  excessVar: boolean;
  moderateVar: boolean;
  goodVar: boolean;
  severeVar: boolean;
  noiseVar: boolean;
  oVar: boolean;
  noVar: boolean;
  soVar: boolean;
  coVar: boolean;
  severity: any;
  type: any;
  city: any;
  area: any;
  selectedCityName: any;
  smartCityInfo: any;
  todayDate: any;
  
  data111 = [
    {
      "name": "Coimbatore",
      "series": [
        {
          "name": "Distribution Volume",
          "value": 1422
        },
        {
          "name": "Capacity",
          "value": 1600
        },
      ]
    },
    {
      "name": "Madurai",
      "series": [
        {
          "name": "Distribution Volume",
          "value": 1322
        },
        {
          "name": "Capacity",
          "value": 1500
        },
      ]
    },
    {
      "name": "Salem",
      "series": [
        {
          "name": "Distribution Volume",
          "value": 559
        },
        {
          "name": "Capacity",
          "value": 238
        },
      ]
    },
    {
      "name": "Thanjavur",
      "series": [
        {
          "name": "Distribution Volume",
          "value": 395
        },
        {
          "name": "Capacity",
          "value": 398
        },
      ]
    },
    {
      "name": "Tiruchirapalli",
      "series": [
        {
          "name": "Distribution Volume",
          "value": 842
        },
        {
          "name": "Capacity",
          "value": 1023
        },
      ]
    },
    {
      "name": "Vellore",
      "series": [
        {
          "name": "Distribution Volume",
          "value": 544
        },
        {
          "name": "Capacity",
          "value": 655
        },
      ]
    },
    {
      "name": "Tirunelveli",
      "series": [
        {
          "name": "Distribution Volume",
          "value": 1477
        },
        {
          "name": "Capacity",
          "value": 1497
        },
      ]
    },
    {
      "name": "Tiruppur",
      "series": [
        {
          "name": "Distribution Volume",
          "value": 791
        },
        {
          "name": "Capacity",
          "value": 855
        },
      ]
    },
    {
      "name": "Thoothukudi",
      "series": [
        {
          "name": "Distribution Volume",
          "value": 1359
        },
        {
          "name": "Capacity",
          "value": 1477
        },
      ]
    },
    {
      "name": "Erode",
      "series": [
        {
          "name": "Distribution Volume",
          "value": 1477
        },
        {
          "name": "Capacity",
          "value": 925
        },
      ]
    },
  ]
  zoom: any = 8;
  zoom1: any = 8;
  // initial center position for the map
	lat: number = 11.3272889;
  lng: number = 78.07734;
  lat1: number = 11.3272889;
  lng1: number = 78.07734;
  zoomControl: boolean = true;
  clickedMarker(label: any, index: number) {
    this.router.navigate(['parent/environment', label.city, this.airPurityStatus]);
    console.log(`clicked the marker: ${JSON.stringify(label) || index}`);
    this.area = label.label;
    this.city = label.city;
    this.type = label.type;
    this.severity = label.severity;
    // debugger;
    if (label.type == 'Severe') {
      this.severeVar = true;
      this.goodVar = false;
      this.moderateVar = false;
    } else if (label.type == "Good") {
      this.goodVar = true;
      this.severeVar = false;
      this.moderateVar = false;
    } else if (label.type == "Moderate") {
      this.moderateVar = true;
      this.goodVar = false;
      this.severeVar = false;
    }

  }

  clickedMarker1(label: any, index: number) {
    console.log(`clicked the marker: ${JSON.stringify(label) || index}`);
    this.area = label.label;
    this.city = label.city;
    this.type = label.type;
    this.severity = label.severity;
    if (this.type == 'Excess Suply') {
      this.excessVar = true;
      this.deficientVar = false;
    } else if (this.type == 'Deficient') {
      this.deficientVar = true;
      this.excessVar = false;
    }
  }
  mapClicked($event: MouseEvent) {
    console.log('helloooooooooo', $event);
  }
  mapClicked1($event: MouseEvent) {
    console.log('helloooooooooo', $event);
  }
  mouseOver($event: MouseEvent, label: any, city: any, severity: any, type: any, infoWindow, gm) {
    this.area = label;
    this.city = city;
    this.type = type;
    this.severity = severity;
    if (type == 'Severe') {
      this.severeVar = true;
      this.goodVar = false;
      this.moderateVar = false;
    } else if (type == "Good") {
      this.goodVar = true;
      this.severeVar = false;
      this.moderateVar = false;
    } else if (type == "Moderate") {
      this.moderateVar = true;
      this.goodVar = false;
      this.severeVar = false;
    }

    console.log('overrrrrrrrrrrrr ', label, city, severity, type);
    if (gm.lastOpen != null) {
      gm.lastOpen.close();
    }

    gm.lastOpen = infoWindow;

    infoWindow.open();
  }
  mouseOver1($event: MouseEvent, label: any, city: any, severity: any, type: any, infoWindow, gm) {
    // debugger;
    this.area = label;
    this.city = city;
    this.type = type;
    this.severity = severity;
    if (this.type == 'Excess Suply') {
      this.excessVar = true;
      this.deficientVar = false;
    } else if (this.type == 'Deficient') {
      this.deficientVar = true;
      this.excessVar = false;
    }
    if (gm.lastOpen != null) {
      gm.lastOpen.close();
    }

    gm.lastOpen = infoWindow;

    infoWindow.open();
  }

  markerDragEnd(m: marker, $event: MouseEvent) {
    console.log('dragEnd', m, $event);
  }
  
  soClick(data) {
    this.airPurityStatus = data;
    this.soVar = true;
    this.severity1 = false;
    this.noVar = false;
    this.oVar = false;
    this.noiseVar = false;
    this.coVar = false;
    this.markers = [
      {
        lat: 11.0168,
        lng: 76.9558,
        // label: 'R', Coimbatore
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Coimbatore',
        severity: '24.3 µg/m³',
        type: 'Good',
      },
      {
        lat: 11.6643,
        lng: 78.1460,
        // label: 'R', Salem
        draggable: true,
        iconUrl: '../assets/images/yellow.png',
        city: 'Salem',
        severity: '34.3 µg/m³',
        type: 'Moderate',
      },
      {
        lat: 9.9252,
        lng: 78.1198,
        // label: 'R', madurai
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Madurai',
        severity: '7.11 µg/m³',
        type: 'Good',
      },
      {
        lat: 10.7870,
        lng: 79.1378,
        // label: 'R', Thanjavur
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Thanjavur',
        severity: '3.98 µg/m³',
        type: 'Good',
      },
      {
        lat: 10.7905,
        lng: 78.7047,
        // label: 'R',
        draggable: true,
        iconUrl: '../assets/images/red.png',
        city: 'Tiruchirappalli',
        severity: '40 µg/m³',
        type: 'Severe',
      },
      {
        lat: 12.9165,
        lng: 79.1325,
        // label: 'R', vellore
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Vellore',
        severity: '7.76 µg/m³',
        type: 'Good',
      },
      {
        lat: 8.7139,
        lng: 77.7567,
        // label: 'R', Tirunelveli
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Tirunelveli',
        severity: '7.14 µg/m³',
        type: 'Good',
      },
      {
        lat: 11.1085,
        lng: 77.3411,
        // label: 'R', Tiruppur
        draggable: true,
        iconUrl: '../assets/images/red.png',
        city: 'Tiruppur',
        severity: '46 µg/m³',
        type: 'Severe',
      },
      {
        lat: 8.7642,
        lng: 78.1348,
        // label: 'R', Thoothkudi
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Thoothkudi',
        severity: '7.14 µg/m³',
        type: 'Good',
      },
      {
        lat: 11.3410,
        lng: 77.7172,
        // label: 'R', Erode
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Erode',
        severity: '24.3 µg/m³',
        type: 'Good',
      },
    ]
  }
  noClick(data) {
    this.airPurityStatus = data;
    this.noVar = true;
    this.soVar = false;
    this.severity1 = false;
    this.oVar = false;
    this.noiseVar = false;
    this.coVar = false;
    this.markers = [
      {
        lat: 11.0168,
        lng: 76.9558,
        // label: 'R', Coimbatore
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Coimbatore',
        severity: '15.2 µg/m³',
        type: 'Good',
      },
      {
        lat: 11.6643,
        lng: 78.1460,
        // label: 'R', Salem
        draggable: true,
        iconUrl: '../assets/images/yellow.png',
        city: 'Salem',
        severity: '25.2 µg/m³',
        type: 'Moderate',
      },
      {
        lat: 9.9252,
        lng: 78.1198,
        // label: 'R', madurai
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Madurai',
        severity: '10.38 µg/m³',
        type: 'Good',
      },
      {
        lat: 10.7870,
        lng: 79.1378,
        // label: 'R', Thanjavur
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Thanjavur',
        severity: '22.31 µg/m³',
        type: 'Good',
      },
      {
        lat: 10.7905,
        lng: 78.7047,
        // label: 'R',
        draggable: true,
        iconUrl: '../assets/images/red.png',
        city: 'Tiruchirappalli',
        severity: '52 µg/m³',
        type: 'Severe',
      },
      {
        lat: 12.9165,
        lng: 79.1325,
        // label: 'R', vellore
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'vellore',
        severity: '20.48 µg/m³',
        type: 'Good',
      },
      {
        lat: 8.7139,
        lng: 77.7567,
        // label: 'R', Tirunelveli
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Tirunelveli',
        severity: '10.29 µg/m³',
        type: 'Good',
      },
      {
        lat: 11.1085,
        lng: 77.3411,
        // label: 'R', Tiruppur
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Tiruppur',
        severity: '15.2 µg/m³',
        type: 'Good',
      },
      {
        lat: 8.7642,
        lng: 78.1348,
        // label: 'R', Thoothkudi
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Thoothkudi',
        severity: '10.29 µg/m³',
        type: 'Good',
      },
      {
        lat: 11.3410,
        lng: 77.7172,
        // label: 'R', Erode
        draggable: true,
        iconUrl: '../assets/images/yellow.png',
        city: 'Erode',
        severity: '35.2 µg/m³',
        type: 'Moderate',
      },
    ]
  }
  oClick(data) {
    this.airPurityStatus = data;
    this.oVar = true;
    this.noVar = false;
    this.soVar = false;
    this.severity1 = false;
    this.noiseVar = false;
    this.coVar = false;
    this.markers = [
      {
        lat: 11.0168,
        lng: 76.9558,
        // label: 'R', Coimbatore
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Coimbatore',
        severity: '41 µg/m³',
        type: 'Good',
      },
      {
        lat: 11.6643,
        lng: 78.1460,
        // label: 'R', Salem
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Salem',
        severity: '42 µg/m³',
        type: 'Good',
      },
      {
        lat: 9.9252,
        lng: 78.1198,
        // label: 'R', madurai
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Madurai',
        severity: '41.5 µg/m³',
        type: 'Good',
      },
      {
        lat: 10.7870,
        lng: 79.1378,
        // label: 'R',
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Thanjavur',
        severity: '38.15 µg/m³',
        type: 'Good',
      },
      {
        lat: 10.7905,
        lng: 78.7047,
        // label: 'R',
        draggable: true,
        iconUrl: '../assets/images/red.png',
        city: 'Tiruchirappalli',
        severity: '11 µg/m³',
        type: 'Severe',
      },
      {
        lat: 12.9165,
        lng: 79.1325,
        // label: 'R', vellore
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'vellore',
        severity: '19.99 µg/m³',
        type: 'Good',
      },
      {
        lat: 8.7139,
        lng: 77.7567,
        // label: 'R', Tirunelveli
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Tirunelveli',
        severity: '46.54 µg/m³',
        type: 'Good',
      },
      {
        lat: 11.1085,
        lng: 77.3411,
        // label: 'R', Tiruppur
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Tiruppur',
        severity: '21 µg/m³',
        type: 'Good',
      },
      {
        lat: 8.7642,
        lng: 78.1348,
        // label: 'R', Thoothkudi
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Thoothkudi',
        severity: '46.54 µg/m³',
        type: 'Good',
      },
      {
        lat: 11.3410,
        lng: 77.7172,
        // label: 'R', Erode
        draggable: true,
        iconUrl: '../assets/images/yellow.png',
        city: 'Erode',
        severity: '11 µg/m³',
        type: 'Moderate',
      },
    ]
  }
  noiseClick(data) {
    this.airPurityStatus = data;
    this.noiseVar = true;
    this.oVar = false;
    this.noVar = false;
    this.soVar = false;
    this.severity1 = false;
    this.coVar = false;
    this.markers = [
      {
        lat: 11.0168,
        lng: 76.9558,
        // label: 'R', Coimbatore
        draggable: true,
        iconUrl: '../assets/images/yellow.png',
        city: 'Coimbatore',
        severity: '55.50 dBA',
        type: 'Moderate',
      },
      {
        lat: 11.6643,
        lng: 78.1460,
        // label: 'R', Salem
        draggable: true,
        iconUrl: '../assets/images/red.png',
        city: 'Salem',
        severity: '77.50 dBA',
        type: 'Severe',
      },
      {
        lat: 9.9252,
        lng: 78.1198,
        // label: 'R', madurai
        draggable: true,
        iconUrl: '../assets/images/yellow.png',
        city: 'Madurai',
        severity: '57.50 dBA',
        type: 'Moderate',
      },
      {
        lat: 10.7870,
        lng: 79.1378,
        // label: 'R', Thanjavur
        draggable: true,
        iconUrl: '../assets/images/red.png',
        city: 'Thanjavur',
        severity: '57.50 dBA',
        type: 'Severe',
      },
      {
        lat: 10.7905,
        lng: 78.7047,
        // label: 'R',
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Tiruchirappalli',
        severity: '57.50 dBA',
        type: 'Good',
      },
      {
        lat: 12.9165,
        lng: 79.1325,
        // label: 'R', vellore
        draggable: true,
        iconUrl: '../assets/images/red.png',
        city: 'Vellore',
        severity: '77.367 dBA',
        type: 'Severe',
      },
      {
        lat: 8.7139,
        lng: 77.7567,
        // label: 'R', Tirunelveli
        draggable: true,
        iconUrl: '../assets/images/red.png',
        city: 'Tirunelveli',
        severity: '87.50 dBA',
        type: 'Severe',
      },
      {
        lat: 11.1085,
        lng: 77.3411,
        // label: 'R', Tiruppur
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Tiruppur',
        severity: '57.50 dBA',
        type: 'Good',
      },
      {
        lat: 8.7642,
        lng: 78.1348,
        // label: 'R', Thoothkudi
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Thoothkudi',
        severity: '57.50 dBA',
        type: 'Good',
      },
      {
        lat: 11.3410,
        lng: 77.7172,
        // label: 'R', Erode
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Erode',
        severity: '57.50 dBA',
        type: 'Good',
      },
    ]
  }
  co2Click(data) {
    this.airPurityStatus = data;
    this.coVar = true;
    this.noiseVar = false;
    this.oVar = false;
    this.noVar = false;
    this.soVar = false;
    this.severity1 = false;
    this.markers = [
      {
        lat: 11.0168,
        lng: 76.9558,
        // label: 'R', Coimbatore
        draggable: true,
        iconUrl: '../assets/images/red.png',
        city: 'Coimbatore',
        severity: '760.0 µg/m³',
        type: 'Severe',
      },
      {
        lat: 11.6643,
        lng: 78.1460,
        // label: 'R', Salem
        draggable: true,
        iconUrl: '../assets/images/yellow.png',
        city: 'Salem',
        severity: '660.0 µg/m³',
        type: 'Moderate',
      },
      {
        lat: 9.9252,
        lng: 78.1198,
        // label: 'R', madurai
        draggable: true,
        iconUrl: '../assets/images/red.png',
        city: 'Madurai',
        severity: '760.0 µg/m³',
        type: 'Severe',
      },
      {
        lat: 10.7870,
        lng: 79.1378,
        // label: 'R', Thanjavur
        draggable: true,
        iconUrl: '../assets/images/red.png',
        city: 'Thanjavur',
        severity: '720.0 µg/m³',
        type: 'Severe',
      },
      {
        lat: 10.7905,
        lng: 78.7047,
        // label: 'R',
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Tiruchirappalli',
        severity: '460.0 µg/m³',
        type: 'Good',
      },
      {
        lat: 12.9165,
        lng: 79.1325,
        // label: 'R', vellore
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Vellore',
        severity: '275.0 µg/m³',
        type: 'Good',
      },
      {
        lat: 8.7139,
        lng: 77.7567,
        // label: 'R', Tirunelveli
        draggable: true,
        iconUrl: '../assets/images/red.png',
        city: 'Tirunelveli',
        severity: '880.0 µg/m³',
        type: 'Severe',
      },
      {
        lat: 11.1085,
        lng: 77.3411,
        // label: 'R', Tiruppur
        draggable: true,
        iconUrl: '../assets/images/red.png',
        city: 'Tiruppur',
        severity: '760.0 µg/m³',
        type: 'Severe',
      },
      {
        lat: 8.7642,
        lng: 78.1348,
        // label: 'R', Thoothkudi
        draggable: true,
        iconUrl: '../assets/images/red.png',
        city: 'Thoothkudi',
        severity: '880.0 µg/m³',
        type: 'Severe',
      },
      {
        lat: 11.3410,
        lng: 77.7172,
        // label: 'R', Erode
        draggable: true,
        iconUrl: '../assets/images/yellow.png',
        city: 'Erode',
        severity: '600.0 µg/m³',
        type: 'Moderate',
      },
    ]
  }
  pmClick(data) {
    this.airPurityStatus = data;
    alert(this.airPurityStatus)
    this.coVar = false;
    this.noiseVar = false;
    this.oVar = false;
    this.noVar = false;
    this.soVar = false;
    // this.severity = false;
    this.severity1 = true;
    this.markers = [
      {
        lat: 11.0168,
        lng: 76.9558,
        // label: 'R', Coimbatore
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Coimbatore',
        severity: '24.916666 µg/m³',
        type: 'Good',
      },
      {
        lat: 11.6643,
        lng: 78.1460,
        // label: 'R', Salem
        draggable: true,
        iconUrl: '../assets/images/yellow.png',
        city: 'Salem',
        severity: '31.9 µg/m³',
        type: 'Moderate',
      },
      {
        lat: 9.9252,
        lng: 78.1198,
        // label: 'R', madurai
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Madurai',
        severity: '20.36 µg/m³',
        type: 'Good',
      },
      {
        lat: 10.7870,
        lng: 79.1378,
        // label: 'R',
        draggable: true,
        iconUrl: '../assets/images/yellow.png',
        city: 'Thanjavur',
        severity: '35.12 µg/m³',
        type: 'Moderate',
      },
      {
        lat: 10.7905,
        lng: 78.7047,
        // label: 'R', 
        draggable: true,
        iconUrl: '../assets/images/yellow.png',
        city: 'Tiruchirappalli',
        severity: '31.98 µg/m³',
        type: 'Moderate',
      },
      {
        lat: 12.9165,
        lng: 79.1325,
        // label: 'R', vellore
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Vellore',
        severity: '11.78 µg/m³',
        type: 'Good',
      },
      {
        lat: 8.7139,
        lng: 77.7567,
        // label: 'R', Tirunelveli
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Tirunelveli',
        severity: '20.52 µg/m³',
        type: 'Good',
      },
      {
        lat: 11.1085,
        lng: 77.3411,
        // label: 'R', Tiruppur
        draggable: true,
        iconUrl: '../assets/images/yellow.png',
        city: 'Tiruppur',
        severity: '30.39 µg/m³',
        type: 'Moderate',
      },
      {
        lat: 8.7642,
        lng: 78.1348,
        // label: 'R', Thoothkudi
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Thoothkudi',
        severity: '20.52 µg/m³',
        type: 'Good',
      },
      {
        lat: 11.3410,
        lng: 77.7172,
        // label: 'R', Erode
        draggable: true,
        iconUrl: '../assets/images/red.png',
        city: 'Erode',
        severity: '43.98 µg/m³',
        type: 'Severe',
      },
    ]
  }
  markers: marker[] = [

    {
      lat: 11.0168,
      lng: 76.9558,
      // label: 'R',
      draggable: true,
      iconUrl: '../assets/images/red.png',
      city: '',
      severity: '36',
      type: 'High',
    },
    {
      lat: 11.6643,
      lng: 78.1460,
      // label: 'R',
      draggable: true,
      iconUrl: '../assets/images/yellow.png',
      city: '',
      severity: '29',
      type: 'Moderate',
    },
    {
      lat: 9.9252,
      lng: 78.1198,
      // label: 'R', madurai
      draggable: true,
      iconUrl: '../assets/images/yellow.png',
      city: '',
      severity: '26',
      type: 'Moderate',
    },
    {
      lat: 10.7870,
      lng: 79.1378,
      // label: 'R',
      draggable: true,
      iconUrl: '../assets/images/green.png',
      city: '',
      severity: '24',
      type: 'Low',
    },
    {
      lat: 10.7905,
      lng: 78.7047,
      // label: 'R',
      draggable: true,
      iconUrl: '../assets/images/red.png',
      city: '',
      severity: '40',
      type: 'High',
    },
    {
      lat: 12.9165,
      lng: 79.1325,
      // label: 'R', vellore
      draggable: true,
      iconUrl: '../assets/images/green.png',
      city: '',
      severity: '24',
      type: 'Low',
    },
    {
      lat: 8.7139,
      lng: 77.7567,
      // label: 'R', Tirunelveli
      draggable: true,
      iconUrl: '../assets/images/red.png',
      city: '',
      severity: '40',
      type: 'Very High',
    },
    {
      lat: 11.1085,
      lng: 77.3411,
      // label: 'R', Tiruppur
      draggable: true,
      iconUrl: '../assets/images/red.png',
      city: '',
      severity: '46',
      type: 'Very High',
    },
    {
      lat: 8.7642,
      lng: 78.1348,
      // label: 'R', Thoothkudi
      draggable: true,
      iconUrl: '../assets/images/yellow.png',
      city: '',
      severity: '29',
      type: 'Moderate',
    },
    {
      lat: 11.3410,
      lng: 77.7172,
      // label: 'R', Erode
      draggable: true,
      iconUrl: '../assets/images/red.png',
      city: '',
      severity: '62',
      type: 'Very High',
    },
  ]
  markers1: marker1[] = [

    {
      lat: 11.12254185885016,
      lng: 76.92789541992192,
      label: '1422KL',
      draggable: true,
      iconUrl: '../assets/images/blue.png',
      city: 'Coimbatore',
      severity: '1600KL',
      type: 'Excess Suply',
    },
    {
      lat: 9.929476032244802,
      lng: 78.10891906330528,
       label: '1322KL',
      draggable: true,
      iconUrl: '../assets/images/blue.png',
      city: 'Madurai',
      severity: '1500KL',
      type: 'Excess Suply',
    },
    {
      lat: 11.659632671616086,
      lng: 78.14169532816027,
       label: '559KL',
      draggable: true,
      iconUrl: '../assets/images/purple.png',
      city: 'Salem',
      severity: '238KL',
      type: 'Deficient',
    },
    {
      lat: 10.787,
      lng: 79.1378,
      label: '395KL',
      draggable: true,
      iconUrl: '../assets/images/blue.png',
      city: 'Thanjavur',
      severity: '398KL',
      type: 'Excess Suply',
    },
    {
      lat: 10.7905,
      lng: 78.7047,
      label: '842KL',
      draggable: true,
      iconUrl: '../assets/images/blue.png',
      city: 'Tiruchirapalli',
      severity: '1023KL',
      type: 'Excess Suply',
    },
    {
      lat: 11.1085,
      lng: 77.3411,
      label: '791KL',
      draggable: true,
      iconUrl: '../assets/images/blue.png',
      city: 'Tiruppur',
      severity: '855KL',
      type: 'Excess Suply',
    },
    {
      lat: 12.9165,
      lng: 79.1325,
      label: '544KL',
      draggable: true,
      iconUrl: '../assets/images/blue.png',
      city: 'Vellore',
      severity: '655KL',
      type: 'Excess Suply',
    },
    {
      lat: 8.7139,
      lng: 77.7567,
      label: '1477KL',
      draggable: true,
      iconUrl: '../assets/images/blue.png',
      city: 'Tirunelveli',
      severity: '1497KL',
      type: 'Excess Suply',
    },
    {
      lat: 8.7642,
      lng: 78.1348,
      label: '1359KL',
      draggable: true,
      iconUrl: '../assets/images/blue.png',
      city: 'Thoothkudi',
      severity: '1477KL',
      type: 'Excess Suply',
    },
    {
      lat: 11.3410,
      lng: 77.7172,
      label: '1477KL',
      draggable: true,
      iconUrl: '../assets/images/purple.png',
      city: 'Erode',
      severity: '925KL',
      type: 'Deficient',
    },
	]
  view: any[] = [750, 400];
  view2: any[] = [1100, 450];
  view22:any[] = [1100, 300];
  view1: any[] = [670, 450];
  view11: any[] = [670, 300];
  colorScheme = {
    domain: [ '#FAAC58']
  };

  colorScheme4 = {
    domain: [ '#b87b06','#5ca026']
  };

  colorScheme1 = {
    domain: ['#FA8258']
  };
  colorScheme2 = {
    domain: [ '#b87b06','#5ca026','#D358F7','#FF8000','#FE2E2E','#0174DF','#FF0080','#B404AE','#0404B4','#1C1C1C']
  };

  colorScheme3 = {
    domain: [ '#5ca026','#E5A81B','#E51A1A']
  };
  
  watherDate=[ {
    "name": "CO2",
    "value": 10
  },
  {
    "name": "NO2",
    "value": 25
  },
  {
    "name": "SO2",
    "value": 14
  }];

  madurai=[ {
    "name": "CO2",
    "value": 20
  },
  {
    "name": "NO2",
    "value": 35
  },
  {
    "name": "SO2",
    "value": 14
  },];
  salem=[ {
    "name": "CO2",
    "value": 60
  },
  {
    "name": "NO2",
    "value": 22
  },
  {
    "name": "SO2",
    "value": 11
  }];
  thanjavur=[ {
    "name": "CO2",
    "value": 70
  },
  {
    "name": "NO2",
    "value": 50
  },
  {
    "name": "SO2",
    "value": 17
  }];
  tiruchirapalli=[ {
    "name": "CO2",
    "value": 34
  },
  {
    "name": "NO2",
    "value": 54
  },
  {
    "name": "SO2",
    "value": 11
  }];
  vellore=[ {
    "name": "CO2",
    "value": 76
  },
  {
    "name": "NO2",
    "value": 25
  },
  {
    "name": "SO2",
    "value": 32
  }];
  tirunelveli=[ {
    "name": "CO2",
    "value": 30
  },
  {
    "name": "NO2",
    "value": 45
  },
  {
    "name": "SO2",
    "value": 33
  }];
  tiruppur=[{
    "name": "CO2",
    "value": 40
  },
  {
    "name": "NO2",
    "value": 32
  },
  {
    "name": "SO2",
    "value": 54
  }]
  thoothukudi=[ {
    "name": "CO2",
    "value": 60
  },
  {
    "name": "NO2",
    "value": 65
  },
  {
    "name": "SO2",
    "value": 17
  }];
  erode=[ {
    "name": "CO2",
    "value": 70
  },
  {
    "name": "NO2",
    "value": 65
  },
  {
    "name": "SO2",
    "value": 30
  }];

  chatData=[
    {
      "name": "Germany",
      "series": [
        {
          "name": "40632",
          "value": 40632
        }
      ]
    },
    {
      "name": "United States",
      "series": [
        {
          "name": "201220",
          "value": 49737
        }
      ]
    },
    {
      "name": "France",
      "series": [
        {
          "name": "202210",
          "value": 36745
        }
      ]
    },
    {
      "name": "United ",
      "series": [
        {
          "name": "20210",
          "value": 36240
        }
      ]
    }
  ]
  single:IncidentData[] = [
    {
      "name": "Coimbatore",
      "value": 80
    },
    {
      "name": "Madurai",
      "value": 35
    },
    {
      "name": "Salem",
      "value": 14
    },
    {
      "name": "Thanjavur",
      "value": 67
    },
    {
      "name": "Tiruchirapalli",
      "value": 42
    },
    {
      "name": "Vellore",
      "value": 23
    },
    {
      "name": "Tirunelveli",
      "value": 76
    },
    {
      "name": "Tiruppur",
      "value": 87
    },
    {
      "name": "Thoothukudi",
      "value": 76
    },
    {
      "name": "Erode",
      "value": 100
    }
  ];
  electricityScada=[
    {
      "name": "Coimbatore",
      "value": 80,
      "userElectricity":18000,
      "purchasedElectricity":8000,
      "wastedElectricity":10000,
      "classType":"progress-bar-warning"
    },
    {
      "name": "Madurai",
      "value": 35,
      "userElectricity":20000,
      "purchasedElectricity":16000,
      "wastedElectricity":4000,
      "classType":"progress-bar-info"
    },
    {
      "name": "Salem",
      "value": 14,
      "userElectricity":30000,
      "purchasedElectricity":28000,
      "wastedElectricity":2000,
      "classType":"progress-bar-success"
    },
    {
      "name": "Thanjavur",
      "value": 67,
      "userElectricity":25000,
      "purchasedElectricity":17000,
      "wastedElectricity":8000,
      "classType":"progress-bar-warning"
    },
    {
      "name": "Tiruchirapalli",
      "value": 42,
      "userElectricity":20000,
      "purchasedElectricity":16000,
      "wastedElectricity":4000,
      "classType":"progress-bar-info"
    },
    {
      "name": "Vellore",
      "value": 23,
      "userElectricity":43000,
      "purchasedElectricity":40000,
      "wastedElectricity":3000,
      "classType":"progress-bar-success"
    },
    {
      "name": "Tirunelveli",
      "value": 76,
      "userElectricity":50000,
      "purchasedElectricity":30000,
      "wastedElectricity":20000,
      "classType":"progress-bar-warning"
    },
    {
      "name": "Tiruppur",
      "value": 87,
      "userElectricity":45000,
      "purchasedElectricity":20000,
      "wastedElectricity":25000,
      "classType":"progress-bar-danger"
    },
    {
      "name": "Thoothukudi",
      "value": 76,
      "userElectricity":8000,
      "purchasedElectricity":2000,
      "wastedElectricity":6000,
      "classType":"progress-bar-warning"
    },
    {
      "name": "Erode",
      "value": 50,
      "userElectricity":30000,
      "purchasedElectricity":15000,
      "wastedElectricity":15000,
      "classType":"progress-bar-info"
    }
  ]
  waterScada=[
    {
      "name": "Coimbatore",
      "value": 80,
      "userElectricity":45000,
      "purchasedElectricity":40000,
      "wastedElectricity":4000,
      "uncountedWater":1000,
      "classType":"progress-bar-warning"
    },
    {
      "name": "Madurai",
      "value": 35,
      "userElectricity":30000,
      "purchasedElectricity":25000,
      "wastedElectricity":3000,
      "uncountedWater":2000,
      "classType":"progress-bar-info"
    },
    {
      "name": "Salem",
      "value": 14,
      "userElectricity":60000,
      "purchasedElectricity":40000,
      "wastedElectricity":5000,
      "uncountedWater":1000,
      "classType":"progress-bar-success"
    },
    {
      "name": "Thanjavur",
      "value": 67,
      "userElectricity":30000,
      "purchasedElectricity":20000,
      "wastedElectricity":6000,
      "uncountedWater":4000,
      "classType":"progress-bar-warning"
    },
    {
      "name": "Tiruchirapalli",
      "value": 42,
      "userElectricity":60000,
      "purchasedElectricity":35000,
      "wastedElectricity":20000,
      "uncountedWater":5000,
      "classType":"progress-bar-info"
    },
    {
      "name": "Vellore",
      "value": 23,
      "userElectricity":80000,
      "purchasedElectricity":60000,
      "wastedElectricity":15000,
      "uncountedWater":5000,
      "classType":"progress-bar-success"
    },
    {
      "name": "Tirunelveli",
      "value": 76,
      "userElectricity":5000,
      "purchasedElectricity":2500,
      "wastedElectricity":2000,
      "uncountedWater":500,
      "classType":"progress-bar-warning"
    },
    {
      "name": "Tiruppur",
      "value": 87,
      "userElectricity":70000,
      "purchasedElectricity":40000,
      "wastedElectricity":20000,
      "uncountedWater":10000,
      "classType":"progress-bar-danger"
    },
    {
      "name": "Thoothukudi",
      "value": 76,
      "userElectricity":40000,
      "purchasedElectricity":30000,
      "wastedElectricity":8000,
      "uncountedWater":2000,
      "classType":"progress-bar-warning"
    },
    {
      "name": "Erode",
      "value": 50,
      "userElectricity":60000,
      "purchasedElectricity":50000,
      "wastedElectricity":5000,
      "uncountedWater":5000,
      "classType":"progress-bar-info"
    }
  ]

  getStyle(data){
return data+'%';
  }
  ///////////////////////////////////
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = false;
  showXAxisLabel = true;
  xAxisLabel = '';
  showYAxisLabel = true;
  yAxisLabel = '';

  showDataLabel=true;

  /////////////////////digital assert table data/////////////
sample=[
  {
    "name": "Germany",
    "value": 40632
  },
  {
    "name": "France",
    "value": 36745
  },
  {
    "name": "United Kingdom",
    "value": 36240
  },
  {
    "name": "Spain",
    "value": 33000
  },
  {
    "name": "Vanuatu",
    "value": 10629
  },
  {
    "name": "Comoros",
    "value": 26926
  },
  {
    "name": "Côte D'Ivoire",
    "value": 57274
  }
]
  showdigitalAssert:DigitalAssert[]=[
              {
                cityName:"Coimbatore",
                total:500,
                working:400,
                repair:100,
              },
              {
                cityName:"Madurai",
                total:600,
                working:400,
                repair:200,
              },
              {
                cityName:"Salem",
                total:500,
                working:250,
                repair:250,
              },
              {
                cityName:"Thanjavur",
                total:600,
                working:300,
                repair:300,
              },
              {
                cityName:"Tiruchirapalli",
                total:900,
                working:400,
                repair:500,
              },
              {
                cityName:"Vellore",
                total:230,
                working:200,
                repair:30,
              } ,
              {
                cityName:"Tiruppur",
                total:766,
                working:700,
                repair:66,
              } ,
              {
                cityName:"Thoothukudi",
                total:444,
                working:400,
                repair:44,
              } ,
              {
                cityName:"Erode",
                total:800,
                working:400,
                repair:400,
              }  
  ]
  parkingAvilb:DigitalAssert[]=[
    {
      cityName:"Coimbatore",
      total:30,
      working:10,
      repair:10,
    },
    {
      cityName:"Madurai",
      total:35,
      working:15,
      repair:20,
    },
    {
      cityName:"Salem",
      total:31,
      working:20,
      repair:11,
    },
    {
      cityName:"Thanjavur",
      total:32,
      working:2,
      repair:30,
    },
    {
      cityName:"Tiruchirapalli",
      total:36,
      working:10,
      repair:26,
    },
    {
      cityName:"Vellore",
      total:23,
      working:10,
      repair:13,
    } ,
    {
      cityName:"Tiruppur",
      total:40,
      working:30,
      repair:10,
    } ,
    {
      cityName:"Thoothukudi",
      total:37,
      working:17,
      repair:20,
    } ,
    {
      cityName:"Erode",
      total:30,
      working:20,
      repair:10,
    }  
]
parkingAvilbPieData= [
  {
    "name": "Coimbatore",
    "value": 12
  },
  {
    "name": "Madurai",
    "value": 12
  },
  {
    "name": "Salem",
    "value": 6
  },
  {
    "name": "Thanjavur",
    "value": 11
  },
  {
    "name": "Tiruchirapalli",
    "value": 6
  },
  {
    "name": "Vellore",
    "value": 6
  },
  {
    "name": "Tirunelveli",
    "value": 6
  },
  {
    "name": "Tiruppur",
    "value": 6
  },
  {
    "name": "Thoothukudi",
    "value": 6
  },
  {
    "name": "Erode",
    "value": 6
  }
]
airPurityStatus: any;
  constructor(private serviceAssert:GetDigitalAssetType, private router: Router) { 
    $(document).ready(function(){
			$('[data-toggle="tooltip"]').tooltip();   
		});
  }

  ngOnInit() {
    this.todayDate = new Date();
    this.onSelect({"name":"Coimbatore","value":30});
    this.coVar = false;
    this.noiseVar = false;
    this.oVar = false;
    this.noVar = false;
    this.soVar = false;
    this.severity = true;
    this.airPurityStatus = 'pm';
    // this.severity = false;
    this.severity1 = true;
    this.markers = [
      {
        lat: 11.0168,
        lng: 76.9558,
        // label: 'R', Coimbatore
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Coimbatore',
        severity: '24.916666 µg/m³',
        type: 'Good',
      },
      {
        lat: 11.6643,
        lng: 78.1460,
        // label: 'R', Salem
        draggable: true,
        iconUrl: '../assets/images/yellow.png',
        city: 'Salem',
        severity: '31.9 µg/m³',
        type: 'Moderate',
      },
      {
        lat: 9.9252,
        lng: 78.1198,
        // label: 'R', madurai
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Madurai',
        severity: '20.36 µg/m³',
        type: 'Good',
      },
      {
        lat: 10.7870,
        lng: 79.1378,
        // label: 'R',
        draggable: true,
        iconUrl: '../assets/images/yellow.png',
        city: 'Thanjavur',
        severity: '35.12 µg/m³',
        type: 'Moderate',
      },
      {
        lat: 10.7905,
        lng: 78.7047,
        // label: 'R', 
        draggable: true,
        iconUrl: '../assets/images/yellow.png',
        city: 'Tiruchirappalli',
        severity: '31.98 µg/m³',
        type: 'Moderate',
      },
      {
        lat: 12.9165,
        lng: 79.1325,
        // label: 'R', vellore
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Vellore',
        severity: '11.78 µg/m³',
        type: 'Good',
      },
      {
        lat: 8.7139,
        lng: 77.7567,
        // label: 'R', Tirunelveli
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Tirunelveli',
        severity: '20.52 µg/m³',
        type: 'Good',
      },
      {
        lat: 11.1085,
        lng: 77.3411,
        // label: 'R', Tiruppur
        draggable: true,
        iconUrl: '../assets/images/yellow.png',
        city: 'Tiruppur',
        severity: '30.39 µg/m³',
        type: 'Moderate',
      },
      {
        lat: 8.7642,
        lng: 78.1348,
        // label: 'R', Thoothkudi
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Thoothkudi',
        severity: '20.52 µg/m³',
        type: 'Good',
      },
      {
        lat: 11.3410,
        lng: 77.7172,
        // label: 'R', Erode
        draggable: true,
        iconUrl: '../assets/images/red.png',
        city: 'Erode',
        severity: '43.98 µg/m³',
        type: 'Severe',
      },
    ]
  }

   getColorCode(city){
    let colorcode:string;
    switch (city) {
      case 'Coimbatore':
      colorcode =  '#b87b06';
        break;
      case 'Madurai':
      colorcode = '#5ca026';
        break;
      case 'Salem':
      colorcode = '#D358F7';
        break;
      case 'Thanjavur':
      colorcode = '#FF8000';
        break;

      case 'Tiruchirapalli':
      colorcode = '#FE2E2E';
        break;
      case 'Vellore':
      colorcode = '#0174DF';
        break;
      case 'Tirunelveli':
      colorcode = '#FF0080';
        break;
      case 'Tiruppur':
      colorcode = '#B404AE';
        break;
      case 'Thoothukudi':
      colorcode = '#0404B4';
        break;
      case 'Erode':
      colorcode = '#1C1C1C';
        break;
      default:
      colorcode = "14";
    }

    return colorcode;
   }


  onSelect(event) {
   
    let getcolorCode=this.getColorCode(event.name);
    this.colorScheme1 = {
      domain: [getcolorCode]
    };
console.log(this.colorScheme1)
    this.serviceAssert.getSmartParking(event.name).subscribe(data => {
      console.log("dasssssssssta"+JSON.stringify(data));
      this.selectedCityName = data[0].cityName;
     
      this.smartCityInfo = this.formatData(data);

    }, error => {
        console.log(error);
    });
  }

  formatData(CityData) {
    console.log(JSON.stringify(CityData));
    let result = [];
    for (let i =0; i<CityData.length; i++) {
      let obj :any= {};
      obj.name = CityData[i].info;
      obj.series = [{"name":CityData[i].previousReading+CityData[i].currentReading ,"value":CityData[i].currentReading}];
      result.push(obj);
    }
    return result;
  }

}


interface IncidentData{
  name:string;
  value:number;
}
interface DigitalAssert{
  cityName:string;
  total:number;
  working:number;
  repair:number;
}
interface marker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
  iconUrl: string;
  city: string;
  severity: any;
  type: any;
}
interface marker1 {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
  iconUrl: string;
  city: string;
  severity: any;
  type: any;
}

$(window).scroll(function () {
	var scroll = $(window).scrollTop();

	//>=, not <=
	if (scroll >= 100) {
		//clearHeader, not clearheader - caps H
		$("#incidentMapArea").addClass("darkHeader");
	}
	if (scroll <= 10) {
		//clearHeader, not clearheader - caps H
		$("#incidentMapArea").removeClass("darkHeader");
	}
})