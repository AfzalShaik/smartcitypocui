import { Component, OnInit } from '@angular/core';
// import { Router } from '@angular/router';
import { MatTab, MatTabLink } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
// import { $ } from 'protractor';
declare var $:any;

@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.css']
})
export class ParentComponent implements OnInit {
  textAlert: string;
  cityArray: string[];
  selectedValue: any;
  city: string;
  city1: any = [];
  totalPath : any;
  batteryHealth: any;
  smokeTest: any;
  coState: any;
  
  constructor(private router: Router, private route: ActivatedRoute) {
    $(document).ready(function(){
      $('[data-toggle="tooltip"]').tooltip();   
  });
    $(document).ready(function () {
      $(".nav123.nav").on('click', 'li', function () {
        $(".nav123.nav li.active").removeClass("active");
        // adding classname 'active' to current click li 
        $(this).addClass("active");
      });
    });
   }
  ngOnInit() {
    // this.textAlert = 'Smoke Alert';
    this.selectedValue = 'Coimbatore';
    this.city = 'Coimbatore';
    this.cityArray = ['Coimbatore', 'Madurai', 'Salem', 'Thanjavur', 'Tiruchirapalli', 'Vellore', 'Tirunelveli', 'Tiruppur', 'Thoothukudi', 'Erode']
    let eventSource = window['EventSource'];
    // let source = new eventSource('https://developer-api.nest.com' + '?auth=' + 'c.XMyCgbDrcOlb0Go9kn7TdC7KSdovJ33cak2m0peNlx6q1KXvQoa58u9JAPBjDlHyXBuXqVGSbC2gLj9aqpIN8EZhtqoj7xzuO4GCSRc4Xcp13oefka35WfbnXztFxLqqboT7kkM45VgerYqd');
    let source = new eventSource('https://developer-api.nest.com' + '?auth=' + 'c.cNtVnrcVVRJu8VnGSr7mgShXcPRBgxUJrxQhgVRKJ28fqaqJBgpNZsB5krf5cf2nUpVCyfyZMwDBWj9kXYpj44V6CU3HEbMStqFKKwJ9HcokyCTVI3zXzXLQgUZbiHZJf69qsntLxBBkkHsu');
    source.addEventListener('put', function(e) {
      // console.log('putttttttttttttt ', JSON.parse(e.data).data.devices.smoke_co_alarms['Wct2ztIDRfDOyNlmmprffOxgySsCgM0k']);
      // debugger;
      this.totalPath = JSON.parse(e.data).data.devices.smoke_co_alarms['Wct2ztIDRfDOyNlmmprffOxgySsCgM0k'];
      this.batteryHealth = this.totalPath['battery_health']
      this.smokeTest = this.totalPath['smoke_alarm_state']
      this.coState = this.totalPath['co_alarm_state']
      console.log('battery health ', this.batteryHealth, this.smokeTest, this.coState);
      if (this.smokeTest == 'emergency' || this.coState == 'emergency ') {
        
        // $('#waterlevelPopUp100').modal('show');
      }
      this.totalPath1 = JSON.parse(e.data).data.devices.cameras['AZR_ev4x0z7P8or8XNkSBiMImWHDIZau_gAQr4CBARQa1h2qd6y4ag'].last_event;
      this.hasSound = this.totalPath1.has_sound;
      this.hasPerson = this.totalPath1.has_person;
      this.hasMotion = this.totalPath1.has_motion;
      if (this.hasSound == true && this.hasPerson == true && this.hasMotion == true) {
        $('#waterlevelPopUpCam').modal('show');
      }
    });
    
  }
  pressureChanged(x) {
    this.city = x;
    const getParam = (this.router.url);
    const checkParam = getParam.split('/')
    if(checkParam[2] == 'tabsArea'){
      this.router.navigate(['parent/tabsArea/'+this.city]);
    }else if( checkParam[2] == 'environment'){
      this.router.navigate(['parent/environment/'+this.city]);
    }else if( checkParam[2] == 'digitalAsset'){
      this.router.navigate(['parent/digitalAsset/'+this.city]);
    }
  }
  cancelAlert() {
    $('#waterlevelPopUp100').modal('hide');
  }
  cancelAlert1() {
    $('#waterlevelPopUpCam').modal('hide');
  }
  generate() {
    this.textAlert = '2 Children Dead, One Missing In Annur Camp Fire';
    $('#waterlevelPopUp').modal('show');
  }
  detail() {
    $('#waterlevelPopUp').modal('hide');
    this.router.navigate(['/parent/tabsArea/Ramanathapuram']);
  }
  detail1() {
    $('#waterlevelPopUp100').modal('hide');
    this.router.navigate(['/parent/tabsArea/Bharathiyar']);
  }
  detailCam() {
    $('#waterlevelPopUpCam').modal('hide');
    this.router.navigate(['/parent/tabsArea/Gopalapuram']);
  }
}
$(document).ready(function() {
  $('#parentTabs.nav li:first-child').addClass('active');
  $('#parentTabs.nav li').click(function() {
      $(this).addClass('active').siblings().removeClass('active');
  });
});