import { Component, OnInit } from '@angular/core';
import { MouseEvent } from '@agm/core';
import { GraphsService } from '../../services/graphs/graphs.service';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgxChartsModule } from '@swimlane/ngx-charts';

// import { ZoomControlOptions} from '../services/google-maps-types';
declare var $: any;

@Component({
	selector: 'app-tabs-area',
	templateUrl: './tabs-area.component.html',
	styleUrls: ['./tabs-area.component.css'],
	providers: [GraphsService]
})
export class TabsAreaComponent implements OnInit {
	highlightFiber1: boolean;
	highlightTirunelveli: boolean;
	tirunelveliVal: number;
	counterTirunelveli: number;
	tirunelveliVar: boolean;
	highlightMadurai: boolean;
	maduraiVal: number;
	counterMadurai: number;
	fireMadurai: string;
	fireVarMadurai: boolean;
	velloreVal: number;
	highlightVellore: boolean;
	counterVellore: number;
	highlightTiruppur: boolean;
	tiruppurVal: number;
	counterTiruppur: number;
	tiruppurVar: boolean;
	cameraVar1: boolean;
	highlightTiruchirapalli: boolean;
	tiruchirapalliVal: number;
	counterTiruchirapalli: number;
	highlightTraffic1: boolean;
	highlightFlood: boolean;
	thanjavurVal: number;
	highlightThanjavur: boolean;
	counterThanjavur: number;
	highlightSalem: boolean;
	salemVal: number;
	counterSalem: number;
	fireVarSalem: boolean;
	fireSalem: string;
	bombVar: boolean;
	bombVariable: boolean;
	transObject: any;
	transArray: any[];
	newstrans: any;
	transObj: any;
	allWater: any;
	cameraVar: boolean;
	cam1: string;
	counterCamera: number;
	cameraVal: number;
	highlightCamera: boolean;
	fireVar11: boolean;
	fiberArray: any;
	trafficArray: any;
	bombArray: any;
	floodArray: any;
	floodCounter: any;
	floodObject: any;
	wasteVal: number;
	highlightWaste: boolean;
	counterWaste: number;
	wasteObject: any;
	announceVal: number;
	highlightAnnounce: boolean;
	counterAnnounce: number;
	announceObject: any;
	div: HTMLElement;
	fiberObject: any;
	fiberObj: any;
	trafficObject: any;
	trafficObj: any;
	bombObject: any;
	bombObj: any;
	waterId: any;
	waterObject: any;
	waterObj: any;
	newsFiber: string;
	fiberVal: number;
	highlightFiber: boolean;
	counterFiber: number;
	bombVal: number;
	highlightBomb: boolean;
	counterBomb: number;
	newsBomb: string;
	trafficVal: number;
	highlightTraffic: boolean;
	counterTraffic: number;
	newsTraffic: string;
	counterWater: number;
	newsWater: string;
	markObj: { lat: number; lng: number; draggable: boolean; iconUrl: string; };
	waterVal: number;
	highlightWater: boolean;
	hospital: string;
	area1: string;
	counterSmoke: number;
	objj: { lat: number; lng: number; draggable: boolean; iconUrl: string; };
	arr: { lat: number; lng: number; draggable: boolean; iconUrl: string; }[];
	smokeVal: number = 1;
	highlightSmoke: any;
	iconUrl: string;
	highlight1: boolean;
	counter1: number;
	markers4: any;
	markers2: { lat: number; lng: number; draggable: boolean; iconUrl: string; }[];
	afzal1: number;
	fireVar: boolean = false;
	news10Var: boolean = false;
	news9Var: boolean = false;
	news8Var: boolean = false;
	news7Var: boolean = false;
	news6Var: boolean = false;
	news5Var: boolean = false;
	news4Var: boolean = false;
	news3Var: boolean = false;
	news2Var: boolean = false;
	news1Var: boolean = false;
	news10: any;
	counter: number;
	markers1: { lat: number; lng: number; draggable: boolean; iconUrl: string; }[];
	distance: string;
	area: string;
	myVar: boolean;
	news: any;
	count: number;
	news9: any;
	news8: any;
	news7: any;
	news6: any;
	news5: any;
	news4: any;
	news3: any;
	news2: any;
	news1: any;
	name: any;
	parking1: any;
	parking2: any;
	parking3: any;
	mass1: any;
	mass2: any;
	mass3: any;
	natural3: any;
	natural2: any;
	natural1: any;
	fire1: any;
	fire2: any;
	fire3: any;
	accident1: any;
	accident2: any;
	accident3: any;
	road1: any;
	road2: any;
	road3: any;
	areaName: any;
	paramData: any;
	parkingArray: any;
	massArray: any;
	naturalArray: any;
	fireArray: any;
	accidentArray: any;
	roadArray1: any;
	roadArray: any;
	cityArray: string[];
	currentDate: any;
	afzal: any = 1;
	highlight: any = false;
	tab4Model: any = 'tab-pane fade';
	heroes = ['Windstorm', 'Bombasto', 'Magneta', 'Tornado'];
	// google maps zoom level

	zoom: number = 12;

	// initial center position for the map
	lat: number = 11.0168;
	lng: number = 76.9558;
	zoomControl: boolean = true;
	clickedMarker(label: string, index: number) {
		console.log(`clicked the marker: ${label || index}`)
	}

	mapClicked($event: MouseEvent) {
		console.log('helloooooooooo', $event.coords, $event.coords.lat, $event.coords.lng);
	}

	markerDragEnd(m: marker, $event: MouseEvent) {
		console.log('dragEnd', m, $event);
	}

	markers: marker[] = [
		{
			lat: 11.0168,
			lng: 76.9558,
			// label: 'R',
			draggable: true,
			iconUrl: '',
		}
	]
	constructor(private GS: GraphsService, private route: ActivatedRoute, private router: Router) {
		$(document).ready(function(){
			$('[data-toggle="tooltip"]').tooltip();   
		});
		
		$(document).ready(function () {
			setInterval(function () {
				var iScroll = $(window).scrollTop();
				iScroll = iScroll + 200;
				$('.autoScroll11').animate({
					scrollTop: iScroll
				}, 1000);
			}, 2000);
		});


		// if (this.highlight == true) {
		// alert('in constructor')
		setInterval(() => { this.moveMarker(); }, 1000 * 1 * 2);
		setInterval(() => { this.getNews(1); }, 1000 * 1 * 30);
		setInterval(() => { this.getSmokeNews(1); }, 1000 * 1 * 5);
		setInterval(() => { this.getSalemNews(1); }, 1000 * 1 * 8);
		setInterval(() => { this.getMaduraiNews(1); }, 1000 * 1 * 8);
		setInterval(() => { this.getWaterNews(1); }, 1000 * 1 * 8);
		setInterval(() => { this.getVelloreNews(1); }, 1000 * 1 * 8);
		setInterval(() => { this.getThanjavurNews(1); }, 1000 * 1 * 8);
		setInterval(() => { this.getTrafficNews(1); }, 1000 * 1 * 5);
		setInterval(() => { this.getTiruchirapalliNews(1); }, 1000 * 1 * 5);
		setInterval(() => { this.getBombNews(1); }, 1000 * 1 * 8);
		setInterval(() => { this.getFiberNews(1); }, 1000 * 1 * 8);
		setInterval(() => { this.getAlerts(); }, 1000 * 1 * 5);
		setInterval(() => { this.getTransNews(1); }, 1000 * 1 * 8);
		setInterval(() => { this.getTiruppurNews(1); }, 1000 * 1 * 8);
		// this.interval();getTransNews
		// setInterval(() => { this.moveMarker1(); }, 1000 * 1 * 4);
		// } 

		$(document).ready(function () {
			//Toggle fullscreen
			$("#panel-fullscreen").click(function (e) {

				e.preventDefault();

				var $this = $(this);

				if ($this.children('i').hasClass('glyphicon-resize-full')) {
					$this.children('i').removeClass('glyphicon-resize-full');
					$this.children('i').addClass('glyphicon-resize-small');
					$('#tabsArea .panel-default').css({
						'padding': '20px',
						'margin-top': '95px'
					});

				}
				else if ($this.children('i').hasClass('glyphicon-resize-small')) {
					$this.children('i').removeClass('glyphicon-resize-small');
					$this.children('i').addClass('glyphicon-resize-full');
					$('#tabsArea .panel-default').css({
						'padding': '0px',
						'margin-top': '0px'
					});
				}
				$(this).closest('.panel').toggleClass('panel-fullscreen');
			});
		});
		$(document).ready(function () {
			$("#navMenus").on('click', 'li', function () {
				$("#navMenus li.active").removeClass("active");
				// adding classname 'active' to current click li 
				$(this).addClass("active");
			});
			$("#navMenus1").on('click', 'li', function () {
				$("#navMenus1 li.active").removeClass("active");
				// adding classname 'active' to current click li 
				$(this).addClass("active");
			});
			$("#navMenus2").on('click', 'li', function () {
				$("#navMenus2 li.active").removeClass("active");
				// adding classname 'active' to current click li 
				$(this).addClass("active");
			});
			$("#navMenus3").on('click', 'li', function () {
				$("#navMenus3 li.active").removeClass("active");
				// adding classname 'active' to current click li 
				$(this).addClass("active");
			});
			$("#navMenus4").on('click', 'li', function () {
				$("#navMenus4 li.active").removeClass("active");
				// adding classname 'active' to current click li 
				$(this).addClass("active");
			});
			$("#navMenus5").on('click', 'li', function () {
				$("#navMenus5 li.active").removeClass("active");
				// adding classname 'active' to current click li 
				$(this).addClass("active");
			});





			//Toggle fullscreen
			$("#panel-fullscreen1").click(function (e) {

				e.preventDefault();

				var $this = $(this);

				if ($this.children('i').hasClass('glyphicon-resize-full')) {
					$this.children('i').removeClass('glyphicon-resize-full');
					$this.children('i').addClass('glyphicon-resize-small');
					$('.holder').css({
						'min-height': '600px'
					});
					$('#tabsArea .panel-default').css({
						'padding': '20px',
						'margin-top': '95px'
					});
				}
				else if ($this.children('i').hasClass('glyphicon-resize-small')) {
					$this.children('i').removeClass('glyphicon-resize-small');
					$this.children('i').addClass('glyphicon-resize-full');
					$('.holder').css({
						'min-height': '250px'
					});
					$('#tabsArea .panel-default').css({
						'padding': '0px',
						'margin-top': '0px'
					});
				}
				$(this).closest('.panel').toggleClass('panel-fullscreen1');

			});

		});
	}
	getAlerts() {
		this.GS.getAlerts().subscribe(data => {
			// console.log('dataaaaaaaaaaaaaaaaaaa ', data);
			// debugger;
			if (data.length > 0) {
				for (var i = 0; i < data.length; i++) {
					if (data[i].alertType == 'Flooding' && data[i].read == 'No') {
						this.waterObj = data[i];
						// this.floodCounter.push(this.waterObj);
						// console.log(this.waterObj.description);
						this.newsWater = this.waterObj.description;
						this.waterIncident1(this.waterObj);
						// this.floodObject = {
						// 	"news": this.newsWater,
						// 	"city": "Coimbatore",
						// 	"type": "Flooding"
						// }
						// this.GS.createIncident(this.name).subscribe(data => {
						// 	console.log('data created', data);
						// 	this.waterId = data[0].news;
						// });
					} else if (data[i].alertType == 'Surveillance' && data[i].read == 'No') {
						this.bombObj = data[i];
						this.bombVariable = true;
						console.log(this.bombVariable);
						this.newsBomb = this.bombObj.description;
						this.bombIncident1(this.bombObj);
					} else if (data[i].alertType == 'City Traffic' && data[i].read == 'No') {
						this.trafficObj = data[i];
						console.log(this.trafficObj.description);
						this.newsTraffic = this.trafficObj.description;
						this.trafficIncident1(this.trafficObj);
					} else if (data[i].alertType == 'Smart public events management' && data[i].read == 'No') {
						this.transObj = data[i];
						// alert(this.fiberObj.description);
						console.log(this.transObj.description);
						this.newstrans = this.transObj.description;
						this.transIncident1(this.transObj);
					}
				}
			}
		});
	}
	moveMarker() {
		// console.log('hiiiiiiiiiiii', this.highlightTraffic, this.highlightSmoke);
		// debugger;
		this.afzal = this.afzal + 1;
		if (this.highlight == true) {
			if (this.afzal >= this.crood.length) {
				this.markers = [
					{
						lat: 11.016673,
						lng: 76.9580895,
						// label: 'R',
						draggable: true,
						iconUrl: './assets/images/fires.gif',
					},
					{
						"lat": 11.0170314,
						"lng": 76.9580973,
						"label": "",
						"draggable": true,
						"iconUrl": "../assets/images/ambulanceR.png"
					},
					{
						"lat": 11.016822,
						"lng": 76.957074,
						"label": "",
						"draggable": true,
						"iconUrl": "../assets/images/fireL.png"
					},
				]
			} else {
				this.markers = [];
				this.markers1 = [
					{
						lat: 11.016673,
						lng: 76.9580895,
						// label: 'R',
						draggable: true,
						iconUrl: './assets/images/fires.gif',
					},
				]
				this.markers.push(this.markers1[0], this.crood[this.afzal], this.crood1[this.afzal]);
				// this.getNews(this.afzal);
				// console.log('this.markerssssssssssss ', this.markers);
			}
		} else if (this.highlightSmoke == true) {
			// alert(this.afzal)
			this.smokeVal = this.smokeVal + 1;
			this.markers = [];
			this.objj = {
				lat: 11.033237,
				lng: 76.885838,
				// label: 'R',
				draggable: true,
				iconUrl: './assets/images/fires.gif',
			}
			// console.log('someeeeeeeeeeeeee',this.smokeVal, this.croodSmoke.length)
			if (this.smokeVal == this.croodSmoke.length || this.smokeVal > this.croodSmoke.length) {
				this.markers = [
					{
						lat: 11.033237,
						lng: 76.885838,
						// label: 'R',
						draggable: true,
						iconUrl: './assets/images/fires.gif',
					},
					{
						"lat": 11.034048,
						"lng": 76.885456,
						// label: 'R', 11.036627, 76.881151
						draggable: true,
						iconUrl: '../assets/images/ambulanceL.png',
					},
					{
						"lat": 11.034648,
						"lng": 76.884834,
						// label: 'R', 11.037516, 76.878683
						draggable: true,
						iconUrl: '../assets/images/policeL.png',
					}
				]
			} else {
				// this.markers = [
				// 	{
				// 		lat: 11.033237,
				// 		lng: 76.885838,
				// 		// label: 'R',
				// 		draggable: true,
				// 		iconUrl: './assets/images/fires.gif',
				// 	},
				// 	{
				// 		lat: 11.036627,
				// 		lng: 76.881151,
				// 		// label: 'R', 11.036627, 76.881151
				// 		draggable: true,
				// 		iconUrl: '../assets/images/ambulanceL.png',
				// 	},
				// 	{
				// 		lat: 11.037516,
				// 		lng: 76.878683,
				// 		// label: 'R', 11.037516, 76.878683
				// 		draggable: true,
				// 		iconUrl: '../assets/images/policeL.png',
				// 	}
				// ]
				this.markers.push(this.objj, this.croodSmoke1[this.smokeVal], this.croodSmoke[this.smokeVal]);
			}
		} else if (this.highlightWater == true) {
			// debugger;
			this.waterVal = this.waterVal + 1;
			this.markers = [];
			this.objj = {
				lat: 11.220731503388226,
				lng: 76.77367329597473,
				// label: 'R',
				draggable: true,
				iconUrl: './assets/images/flood.gif',
			}
			this.markObj = {
				lat: 11.2603955,
				lng: 76.8059745,
				// label: 'R',
				draggable: false,
				iconUrl: '',
			}
			if (this.waterVal == this.croodWater.length || this.waterVal > this.croodWater.length) {
				this.markers = [
					{
						lat: 11.2603955,
						lng: 76.8059745,
						// label: 'R',
						draggable: false,
						iconUrl: '',
					},
					{
						lat: 11.220731503388226,
						lng: 76.77367329597473,
						// label: 'R',
						draggable: true,
						iconUrl: './assets/images/flood.gif',
					},
					{
						'lat': 11.2319496054275,
						'lng': 76.80113911628723,
						// label: 'R', 11.036627, 76.881151
						draggable: true,
						iconUrl: '../assets/images/flood1.png',
					}
				]
			} else {

				this.markers.push(this.objj, this.markObj, this.croodWater[this.waterVal]);
			}
		} else if (this.highlightTraffic == true) {
			// debugger;
			this.trafficVal = this.trafficVal + 1;
			this.markers = [];
			this.objj = {
				lat: 10.99330324321405,
				lng: 76.96003317832947,
				// label: 'R',
				draggable: true,
				iconUrl: './assets/images/traffic.gif',
			}

			if (this.trafficVal == this.croodTraffic.length || this.trafficVal > this.croodTraffic.length) {
				this.markers = [
					{
						lat: 10.99330324321405,
						lng: 76.96003317832947,
						// label: 'R',
						draggable: false,
						iconUrl: './assets/images/traffic.gif',
					},
					{
						lat: 10.993298964606769,
						lng: 76.95960151031613,
						// label: 'R',
						draggable: true,
						iconUrl: './assets/images/pr.png',
					},
					{
						'lat': 10.993524907899008,
						'lng': 76.95971416309476,
						// label: 'R', 11.036627, 76.881151
						draggable: true,
						iconUrl: '../assets/images/pd.png',
					}
				]
			} else {

				this.markers.push(this.objj, this.croodTraffic[this.trafficVal], this.croodTraffic1[this.trafficVal]);
			}
		} else if (this.bombVar == true) {

			this.bombVal = this.bombVal + 1;
			this.markers = [];
			this.objj = {
				lat: 10.970811393517605,
				lng: 76.8916368484497,
				// label: 'R',
				draggable: true,
				iconUrl: './assets/images/suicase1.png',
			}

			if (this.bombVal == this.croodTraffic.length || this.bombVal > this.croodTraffic.length) {
				this.markers = [
					{
						lat: 10.970811393517605,
						lng: 76.8916368484497,
						// label: 'R',
						draggable: false,
						iconUrl: './assets/images/suicase1.png',
					},
					{
						lat: 10.97086998198181,
						lng: 76.8895997107029,
						// label: 'R',
						draggable: true,
						iconUrl: './assets/images/bomb.png',
					},
					{
						lat: 10.97095621891363,
						lng: 76.89371958374977,
						// label: 'R',
						draggable: true,
						iconUrl: './assets/images/pl.png',
					}
				]
			} else {

				this.markers.push(this.objj, this.croodBomb[this.bombVal], this.croodBomb1[this.bombVal]);
			}
		}
		else if (this.highlightFiber == true) {

			this.fiberVal = this.fiberVal + 1;
			this.markers = [];
			this.objj = {
				lat: 11.115685415326556,
				lng: 76.93560495972633,
				// label: 'R',
				draggable: true,
				iconUrl: './assets/images/fiber.png',
			}

			if (this.fiberVal == this.croodFiber.length || this.fiberVal > this.croodFiber.length) {
				this.markers = [
					{
						lat: 11.115685415326556,
						lng: 76.93560495972633,
						// label: 'R',
						draggable: false,
						iconUrl: './assets/images/fiber.png',
					},
					{
						lat: 11.117685645295275,
						lng: 76.93571090698242,
						// label: 'R',
						draggable: true,
						iconUrl: './assets/images/pd.png',
					},
					{
						lat: 11.113785842125118,
						lng: 76.93569079041481,
						// label: 'R',
						draggable: true,
						iconUrl: './assets/images/road.png',
					}
				]
			} else {

				this.markers.push(this.objj, this.croodFiber[this.fiberVal], this.croodFiber1[this.fiberVal]);
			}
		}
		else if (this.highlightWaste == true) {

			this.wasteVal = this.wasteVal + 1;
			this.markers = [];
			this.objj = {
				lat: 11.024517650120645,
				lng: 76.95141289383173,
				// label: 'R',
				draggable: true,
				iconUrl: './assets/images/waste1.png',
			}

			if (this.wasteVal == this.croodWaste.length || this.wasteVal > this.croodWaste.length) {
				this.markers = [
					{
						lat: 11.024517650120645,
						lng: 76.95141289383173,
						// label: 'R',
						draggable: false,
						iconUrl: './assets/images/waste1.png',
					},
					{
						lat: 11.025334447275576,
						lng: 76.95140719413757,
						// label: 'R',
						draggable: true,
						iconUrl: './assets/images/pd.png',
					},
					{
						lat: 11.0240022966792,
						lng: 76.95153091102839,
						// label: 'R',
						draggable: true,
						iconUrl: './assets/images/pu.png',
					}
				]
			} else {

				this.markers.push(this.objj, this.croodWaste[this.wasteVal], this.croodWaste1[this.wasteVal]);
			}
		}
		else if (this.highlightCamera == true) {

			this.cameraVal = this.cameraVal + 1;
			this.markers = [];
			this.objj = {
				lat: 11.003798150909669,
				lng: 76.97230763733387,
				// label: 'R',
				draggable: true,
				iconUrl: './assets/images/rally.gif',
			}

			if (this.cameraVal == this.croodCamera.length || this.cameraVal > this.croodCamera.length) {
				this.markers = [
					{
						lat: 11.003798150909669,
						lng: 76.97230763733387,
						// label: 'R',
						draggable: true,
						iconUrl: './assets/images/rally.gif',
					},
					{
						lat: 11.004747966375659,
						lng: 76.97398133575916,
						// label: 'R',
						draggable: true,
						iconUrl: './assets/images/policeR.png',
					}
				]
			} else {

				this.markers.push(this.objj, this.croodCamera[this.cameraVal]);
			}
		}
		else if (this.highlightSalem == true) {

			this.salemVal = this.salemVal + 1;
			this.markers = [];
			this.objj = {
				lat: 11.664109886411596,
				lng: 78.14588069915771,
				// label: 'R', 11.702058, 78.102855
				draggable: true,
				iconUrl: './assets/images/fires.gif',
			}

			if (this.salemVal == this.croodSalem.length || this.salemVal > this.croodSalem.length) {
				this.markers = [
					{
						lat: 11.664109886411596,
						lng: 78.14588069915771,
						// label: 'R', 11.702058, 78.102855
						draggable: true,
						iconUrl: './assets/images/fires.gif',
					},
					{
						lat: 11.664908438678186,
						lng: 78.14549446105957,
						// label: 'R', 11.724581, 78.067750
						draggable: true,
						iconUrl: '../assets/images/policeL.png',
					},
					{
						lat: 11.663482403968386,
						lng: 78.14683590084314,
						// label: 'R', 11.692560, 78.112639
						draggable: true,
						iconUrl: '../assets/images/cc1.png',
					}
				]
			} else {

				this.markers.push(this.objj, this.croodSalem[this.salemVal], this.croodSalem1[this.salemVal]);
			}
		} else if (this.highlightThanjavur == true) {

			this.thanjavurVal = this.thanjavurVal + 1;
			this.markers = [];
			this.objj = {
				lat: 10.833875015038698,
				lng: 78.8170337677002,
				// label: 'R', 10.8321013!4d78.8176763
				draggable: true,
				iconUrl: './assets/images/flood.gif',
			}
				

			if (this.thanjavurVal == this.croodThanjavur.length || this.thanjavurVal > this.croodThanjavur.length) {
				this.markers = [
					{
						lat: 10.833875015038698,
						lng: 78.8170337677002,
						// label: 'R', 10.8321013!4d78.8176763
						draggable: true,
						iconUrl: './assets/images/flood.gif',
					},
					{
						lat: 10.834563910397534,
						lng: 78.81519947201014,
						// label: 'R', 11.724581, 78.067750
						draggable: true,
						iconUrl: '../assets/images/flood3.png',
					}
				]
			} else {

				this.markers.push(this.objj, this.croodThanjavur[this.thanjavurVal]);
			}
		} else if (this.highlightTiruchirapalli == true) {

			this.tiruchirapalliVal = this.tiruchirapalliVal + 1;
			this.markers = [];
			this.objj ={
				lat: 10.790372611414844,
				lng: 78.70502471923828,
				// label: 'R', 10.8321013!4d78.8176763
				draggable: true,
				iconUrl: './assets/images/traffic.gif',
			}
				

			if (this.tiruchirapalliVal == this.croodTiruchirapalli.length || this.tiruchirapalliVal > this.croodTiruchirapalli.length) {
				this.markers = [
					{
						lat: 10.790372611414844,
						lng: 78.70502471923828,
						// label: 'R', 10.8321013!4d78.8176763
						draggable: true,
						iconUrl: './assets/images/traffic.gif',
					},
					{
						lat: 10.790119672031679,
						lng:78.70296478271484,
						// label: 'R', 11.724581, 78.067750
						draggable: true,
						iconUrl: '../assets/images/pr.png',
					}
				]
			} else {

				this.markers.push(this.objj, this.croodTiruchirapalli[this.tiruchirapalliVal]);
			}
		} else if (this.highlightTiruppur == true) {

			this.tiruppurVal = this.tiruppurVal + 1;
			this.markers = [];
			this.objj ={
				lat: 11.10806336066603,
				lng: 77.34113216400146,
				// label: 'R', 10.8321013!4d78.8176763 11.121628, 77.382435
				draggable: true,
				iconUrl: './assets/images/rally.gif',
			}
				

			if (this.tiruppurVal == this.croodTiruppur.length || this.tiruppurVal > this.croodTiruppur.length) {
				this.markers = [
					{
						lat: 11.10806336066603,
						lng: 77.34113216400146,
						// label: 'R', 10.8321013!4d78.8176763 11.121628, 77.382435
						draggable: true,
						iconUrl: './assets/images/rally.gif',
					},
					{
						lat: 11.10808852877605,
						lng: 77.34200673177838,
						// label: 'R', 11.724581, 78.067750 
						draggable: true,
						iconUrl: '../assets/images/policeR.png',
					}
				]
			} else {

				this.markers.push(this.objj, this.croodTiruppur[this.tiruppurVal]);
			}
		} else if (this.highlightVellore == true) {

			this.velloreVal = this.velloreVal + 1;
			this.markers = [];
			this.objj ={
				lat:12.921494733141406,
				lng:79.13131389766932,
				// label: 'R', 10.8321013!4d78.8176763
				draggable: true,
				iconUrl: './assets/images/flood.gif',
			}
				

			if (this.velloreVal == this.croodVellore.length || this.velloreVal > this.croodVellore.length) {
				this.markers = [
					{
						lat:12.921494733141406,
						lng:79.13131389766932,
						// label: 'R', 10.8321013!4d78.8176763
						draggable: true,
						iconUrl: './assets/images/flood.gif',
					},
					{
						lat: 12.921039846539445, 
						lng: 79.13201093673706,
						// label: 'R', 11.724581, 78.067750 
						draggable: true,
						iconUrl: '../assets/images/flood4.png',
					}
				]
			} else {

				this.markers.push(this.objj, this.croodVellore[this.velloreVal]);
			}
		}
		else if (this.highlightMadurai == true) {

			this.maduraiVal = this.maduraiVal + 1;
			this.markers = [];
			this.objj ={
				lat: 9.905295377423082,
				lng: 78.12081813812256,
				// label: 'R', 11.702058, 78.102855
				draggable: true,
				iconUrl: './assets/images/fires.gif',
			}
				

			if (this.maduraiVal == this.croodMadurai.length || this.maduraiVal > this.croodMadurai.length) {
				this.markers = [
					{
						lat: 9.905295377423082,
						lng: 78.12081813812256,
						// label: 'R', 11.702058, 78.102855
						draggable: true,
						iconUrl: './assets/images/fires.gif',
					},
					{
						lat:9.905217636961112,
						lng: 78.11995553708846,
						// label: 'R', 11.692560, 78.112639
						draggable: true,
						iconUrl: '../assets/images/fireL.png',
					},
					{
						lat: 9.906619996002602, 
						lng: 78.12102842069442,
						// label: 'R', 11.692560, 78.112639
						draggable: true,
						iconUrl: '../assets/images/pd.png',
					}
				]
			} else {

				this.markers.push(this.objj, this.croodMadurai[this.maduraiVal], this.croodMadurai1[this.maduraiVal]);
			}
		}
		
	}
	moveMarker1() {
		// console.log('hiiiiiiiiiiii', this.crood.length, this.crood1.length);
		this.afzal1 = this.afzal1 + 1;
		if (this.highlight == true) {
			if (this.afzal1 >= this.crood2.length) {
				this.markers4 = [
					{
						lat: 11.233144,
						lng: 77.104926,
						// label: 'R',
						draggable: false,
						iconUrl: './assets/images/fires.gif',
					},
					{
						"lat": 11.232908,
						"lng": 77.107788,
						"label": "",
						"draggable": true,
						"iconUrl": "../assets/images/ambulanceR.png"
					},
					{
						"lat": 11.233062,
						"lng": 77.099130,
						"label": "",
						"draggable": true,
						"iconUrl": "../assets/images/policeL.png"
					},
				]
			} else {
				this.markers4 = [];
				this.markers2 = [
					{
						lat: 11.233144,
						lng: 77.104926,
						// label: 'R',
						draggable: false,
						iconUrl: './assets/images/fires.gif',
					},
				]
				this.markers4.push(this.markers2[0], this.crood2[this.afzal1], this.crood3[this.afzal1]);
				// this.getNews(this.afzal);
				// console.log('this.markerssssssssssss ', this.markers);
			}
		} else {

		}
	}

	interval() {
		// alert('inside method' + this.highlight1)
		if (this.highlight1 == true) {
			// alert(this.highlight1);
			// setInterval(() => { this.moveMarker1(); }, 1000 * 1 * 4);
		}
	}
	ngOnInit() {
		this.currentDate = new Date();
		this.fire1 = 'Moving Car Gutted In Fire In Coimbatore, No Casualties';
		// this.fire2 = '2 got injured in fire accident';
		this.paramData = this.route.params.subscribe(params => {
			this.areaName = params.id;
			//   alert(params.id == 'Ramanathapuram')
			if (params.id == 'Ramanathapuram') {

				this.secondFire('');
			} else if (params.id == 'Bharathiyar') {
				this.smoke('');
			} else if (params.id == 'Gopalapuram') {
				this.camera('');
			}
			// alert('.. '+ params.id);
			this.areaName = (params.id) ? params.id : 'Coimbatore';
			if (this.areaName == 'Ramanathapuram') {
				this.areaName = 'Coimbatore';
			} else if (this.areaName == 'Bharathiyar') {
				this.areaName = 'Coimbatore';
			} else if (this.areaName == 'Salem') {
				this.salemFireIncident('');
			} else if (this.areaName == 'Thanjavur') {
				this.thanjavurFloodIncident('');
			} else if (this.areaName == 'Tiruchirapalli') {
				this.tiruchirapalliTrafficIncident('');
			} else if (this.areaName == 'Tiruppur') {
				this.tiruppurEventIncident('');
			} else if (this.areaName == 'Vellore') {
				this.velloreFloodIncident('');
			} else if (this.areaName == 'Madurai') {
				this.maduraiFireIncident('');
			} else if (this.areaName == 'Tirunelveli') {
				this.tirunelveliTrafficIncident('');
			} else if (this.areaName == 'CoimbatoreBomb') {
				this.bombIncident('');
			} else if (this.areaName == 'CoimbatoreTraffic') {
				this.trafficIncident('');
			} else if (this.areaName == 'CoimbatoreEvent') {
				this.camera('');
			} else if (this.areaName == 'CoimbatoreFlood') {
				this.waterIncident('');
			} else if (this.areaName == 'CoimbatoreFiber') {
				this.fiberIncident('');
			}
			this.pressureChanged(this.areaName);
		});
		this.getFloodIncidents();
		this.getBombIncidents();
		this.getCityIncidents();
		this.getTransIncidents();
		this.getTwitter();
		// this.name = this.areaName;
		this.cityArray = ['Coimbatore', 'Madurai', 'Salem', 'Thanjavur', 'Tiruchirapalli', 'Vellore', 'Tirunelveli', 'Tiruppur', 'Thoothukudi'];
		this.count = 1;
		this.count = this.count + 1;
	}
	getFloodIncidents() {
		this.GS.getIncidents('Flooding').subscribe(data => {
			if (data.length > 0) {
				this.floodArray = [];
				for (var i = 0; i < data.length; i++) {
					this.floodArray.push(data[i])
				}
			}
		});
	}
	getBombIncidents() {
		this.GS.getIncidents('Surveillance').subscribe(data => {
			if (data.length > 0) {
				this.bombArray = [];
				for (var i = 0; i < data.length; i++) {
					this.bombArray.push(data[i])
				}
			}
		});
	}
	getCityIncidents() {
		this.GS.getIncidents('City Traffic').subscribe(data => {
			if (data.length > 0) {
				this.trafficArray = [];
				for (var i = 0; i < data.length; i++) {
					this.trafficArray.push(data[i])
				}
			}
		});
	}
	getFiberIncidents() {
		this.GS.getIncidents('Fiber').subscribe(data => {
			if (data.length > 0) {
				this.fiberArray = [];
				for (var i = 0; i < data.length; i++) {
					this.fiberArray.push(data[i])
				}
			}
		});
	}
	getTransIncidents() {
		this.GS.getIncidents('Smart public events management').subscribe(data => {
			if (data.length > 0) {
				this.transArray = [];
				for (var i = 0; i < data.length; i++) {
					this.transArray.push(data[i])
				}
			}
		});
	}
	getTwitter() {
		// debugger;
		this.GS.getTwitterData('FireDemo').subscribe(data => {
			// debugger;
			let statuses = data.data.statuses;
			console.log('statusssssss ', statuses);
			if (statuses.length > 0) {
				for (let i = 0; i < statuses.length; i++) {
					let all = statuses[i].text;

					// fire
					let expr = 'Fire';
					let expr1 = 'fire';
					let expr3 = 'fireDemo';
					let expr4 = 'FireDemo';
					// let fireVar1 = all.search(expr);
					// let fireVar111 = all.search(expr1);
					let fireVar119 = all.search(expr3);
					let fireVar118 = all.search(expr4);
					if (fireVar119 >=0 || fireVar118 >= 0) {
						$('#waterlevelPopUp100').modal('show');
					}

				}

			}
		});
		this.GS.getTwitterData('FloodDemo1').subscribe(data => {
			let statuses = data.data.statuses;
			console.log('statusssssdddss ', JSON.stringify(statuses));
			if (statuses.length > 0) {
				for (let i = 0; i < statuses.length; i++) {
					let all = statuses[i].text;
					this.newsWater = all;
					this.allWater = statuses[i].text;
					// flood
					let exprWater = 'Water';
					let exprWater1 = 'water';
					let exprWater2 = 'Flood';
					let exprWater3 = 'flood';
					let waterVar = all.search(exprWater);
					let waterVar1 = all.search(exprWater1);
					let waterVar2 = all.search(exprWater2);
					let waterVar3 = all.search(exprWater3);
					console.log('flodddd ', all, waterVar, waterVar1, waterVar2, waterVar3);

					let waterPostObj = {
						"citizenId": "5af03877aa9cae2e42c1e62b",
						"citizenName": "sairam",
						"citizenMobileNumber": "7799849440",
						"areaName": "R S Puram",
						"department": "Revenue",
						"description": this.allWater,
						"alertType": "Flooding",
						"read": "No",
						"count": 1
					}
					this.GS.postAlert(waterPostObj).subscribe(dataPost => {
						if (waterVar >= 0 || waterVar1 >= 0 || waterVar2 >= 0 || waterVar3 >= 0 && dataPost[i].read == 'No') {
							$('#waterlevelPopUpWater').modal('show');
						}
					});
				}
				this.getFloodIncidents();
			}
		});
		this.GS.getTwitterData('SuitcaseDemo').subscribe(data => {
			let statuses = data.data.statuses;
			console.log('statusssssdd111dss ', JSON.stringify(statuses));
			if (statuses.length > 0) {
				for (let i = 0; i < statuses.length; i++) {
					let all = statuses[i].text;
					this.newsBomb = all;

					// flood
					let exprBomb = 'Suitcase';
					let exprBomb1 = 'suitcase';
					let bombVar = all.search(exprBomb);
					let bombVar1 = all.search(exprBomb1);
					console.log('flodddd ', all, bombVar, bombVar1);
					let bombPostObj = {
						"citizenId": "5af03877aa9cae2e42c1e62b",
						"citizenName": "sairam",
						"citizenMobileNumber": "7799849440",
						"areaName": "R S Puram",
						"department": "Revenue",
						"description": all,
						"alertType": "Surveillance",
						"read": "No",
						"count": 1
					}
					this.GS.postAlert(bombPostObj).subscribe(dataPost => {
						if (bombVar >= 0 || bombVar1 >= 0 && dataPost[i].read == 'No') {
							$('#waterlevelPopUpBomb').modal('show');
						}
					});
				}
				this.getBombIncidents();
			}
		});
		this.GS.getTwitterData('TrafficDemo').subscribe(data => {
			let statuses = data.data.statuses;
			console.log('statuTrafficDemosssssss ', statuses);
			if (statuses.length > 0) {
				for (let i = 0; i < statuses.length; i++) {
					let all = statuses[i].text;
					this.newsTraffic = all;
					// fire
					let expr = 'Traffic';
					let expr1 = 'traffic';
					let trafficVar1 = all.search(expr);
					let trafficVar111 = all.search(expr1);
					let trafficPostObj = {
						"citizenId": "5af03877aa9cae2e42c1e62b",
						"citizenName": "sairam",
						"citizenMobileNumber": "7799849440",
						"areaName": "R S Puram",
						"department": "Revenue",
						"description": all,
						"alertType": "City Traffic",
						"read": "No",
						"count": 1
					}
					this.GS.postAlert(trafficPostObj).subscribe(dataPost => {
						if (trafficVar1 >= 0 || trafficVar111 >= 0 && dataPost[i].read == 'No') {
							// $('#waterlevelPopUpTraffic').modal('show');
						}
					});
				}
				this.getCityIncidents();
			}
		});
	}
	cancelAlert(x) {
		console.log('cancel', x);
		$('#waterlevelPopUpWater').modal('hide');
		this.waterObject = x;
		this.waterObject.read = 'Yes';
		this.GS.updateAlert(this.waterObject).subscribe(data => {
		});
	}
	cancelBombAlert(x) {
		console.log('cancel', x);
		$('#waterlevelPopUpBomb').modal('hide');
		this.bombObject = x;
		this.bombObject.read = 'Yes';
		this.GS.updateAlert(this.bombObject).subscribe(data => {
		});
	}
	cancelTrafficAlert(x) {
		console.log('cancel', x);
		$('#waterlevelPopUpTraffic').modal('hide');
		this.trafficObject = x;
		this.trafficObject.read = 'Yes';
		this.GS.updateAlert(this.trafficObject).subscribe(data => {
		});
	}
	cancelFiberbAlert(x) {
		console.log('cancel', x);
		$('#waterlevelPopUpFiber').modal('hide');
		this.fiberObject = x;
		this.fiberObject.read = 'Yes';
		this.GS.updateAlert(this.fiberObject).subscribe(data => {
		});
	}
	canceltransAlert(x) {
		console.log('cancel', x);
		$('#waterlevelPopUpCam1').modal('hide');
		this.transObject = x;
		this.transObject.read = 'Yes';
		this.GS.updateAlert(this.transObject).subscribe(data => {
		});
	}
	waterIncident1(x) {
		this.getFloodIncidents();
		this.getBombIncidents();
		this.getCityIncidents();
		this.getFiberIncidents();
		// debugger;
		if (x) {
			// alert('..')
			this.newsWater = x.description;
			console.log(this.newsWater, '00');
			if (this.newsWater) {
				// setInterval(() => { $('#waterlevelPopUpWater').modal('show'); }, 1000 * 1 * 5);
				// $('#waterlevelPopUpWater').modal('show');
			}
			// this.waterObject = x;
			// this.waterObject.count = 3;
			// this.GS.updateAlert(this.waterObject).subscribe(data => {
			// });
			// this.waterObject = x;
			// this.waterObject.read = 'Yes';
			// this.GS.updateAlert(this.waterObject).subscribe(data => {
			// });
		}
	}
	trafficIncident1(x) {
		this.getFloodIncidents();
		this.getBombIncidents();
		this.getCityIncidents();
		this.getFiberIncidents();
		if (x) {
			this.newsTraffic = x.description;
			console.log(this.newsTraffic, '001');

			if (this.newsTraffic) {
				// $('#waterlevelPopUpTraffic').modal('show');
			}
			// this.trafficObject = x;
			// this.trafficObject.read = 'Yes';
			// this.GS.updateAlert(this.trafficObject).subscribe(data => {
			// });
		}
	}
	bombIncident1(x) {
		this.getFloodIncidents();
		this.getBombIncidents();
		this.getCityIncidents();
		this.getFiberIncidents();
		if (x) {
			this.newsBomb = x.description;
			console.log(this.newsBomb, '002');

			if (this.newsBomb) {
				// $('#waterlevelPopUpBomb').modal('show');
			}
			// this.bombObject = x;
			// this.bombObject.read = 'Yes';
			// this.GS.updateAlert(this.bombObject).subscribe(data => {
			// });
		}
	}
	transIncident1(x) {
		this.getFloodIncidents();
		this.getBombIncidents();
		this.getCityIncidents();
		this.getTransIncidents();
		if (x) {
			this.newstrans = x.description;
			console.log(this.newstrans, '0032');
			if (this.newstrans) {
				// $('#waterlevelPopUpCam1').modal('show');
			}
			// this.transObject = x;
			// this.transObject.read = 'Yes';
			// this.GS.updateAlert(this.transObject).subscribe(data => {
			// });
		}
	}
	fiberIncident1(x) {
		this.getFloodIncidents();
		this.getBombIncidents();
		this.getCityIncidents();
		this.getFiberIncidents();
		if (x) {
			this.newsFiber = x.description;
			console.log(this.newsFiber, '003');
			if (this.newsFiber) {
				// $('#waterlevelPopUpFiber').modal('show');
			}
			// this.fiberObject = x;
			// this.fiberObject.read = 'Yes';
			// this.GS.updateAlert(this.fiberObject).subscribe(data => {
			// });
		}
		// $('#waterlevelPopUpFiber').modal('show');
		// this.newsFiber = 'Fiber Cut at Venkatachalapathy Nagar Road No 9';
	}
	wasteIncident1(x) {
		this.getFloodIncidents();
		this.getBombIncidents();
		this.getCityIncidents();
		this.getFiberIncidents();
		if (x) {
			$('#waterlevelPopUpWaste').modal('show');
			// this.wasteObject = x;
			// this.wasteObject.count = 3;
			// this.GS.updateAlert(this.wasteObject).subscribe(data => {
			// });
		}
	}
	wasteIncident(x) {
		this.getFloodIncidents();
		this.getBombIncidents();
		this.getCityIncidents();
		this.getFiberIncidents();
		if (x) {
			// this.wasteObject = x;
			// this.wasteObject.read = 'Yes';
			// this.GS.updateAlert(this.wasteObject).subscribe(data => {
			// });
		}
		$('#waterlevelPopUpWaste').modal('hide');
		this.counterWaste = 1;
		this.myVar = true;
		this.area = 'Fiber Cut at Venkatachalapathy Nagar';
		this.area1 = 'Fiber Cut at Venkatachalapathy Nagar';
		this.distance = 'Police and Road Departmnet vehicles are near to Venkatachalapathy Nagar with a distance of 90m';
		this.hospital = 'Lalitha Govt Hospital';

		this.highlightWaste = true
		this.wasteVal = 1;
		this.lat = 11.024517650120645;
		this.lng = 76.95141289383173;
		this.zoom = 17;
		this.zoomControl = true;
		this.iconUrl = './assets/images/waste1.png';
		this.markers = [
			{
				lat: 11.024517650120645,
				lng: 76.95141289383173,
				// label: 'R', 11.0242557!4d76.9511819
				draggable: false,
				iconUrl: './assets/images/waste1.png',
			},
			{
				lat: 11.026819287863892,
				lng: 76.95127844810486,
				// label: 'R', 11.0242557!4d76.9511819
				draggable: false,
				iconUrl: './assets/images/pd.png',
			},
			{
				lat: 11.02282283815051,
				lng: 76.95170257240534,
				// label: 'R', 11.0242557!4d76.9511819
				draggable: false,
				iconUrl: './assets/images/pu.png',
			}
		]
	}
	fiberIncident(x) {
		this.getFloodIncidents();
		this.getBombIncidents();
		this.getCityIncidents();
		this.getFiberIncidents();
		// if (x) {
		// 	this.fiberObject = x;
		// 	this.fiberObject.read = 'Yes';
		// 	this.GS.updateAlert(this.fiberObject).subscribe(data => {
		// 	});
		// }
		// $('#tabUL a[href="#tab3"]').trigger('click');
		$('#tabUL a[href="#tab6"]').trigger('click');
		$('#waterlevelPopUpFiber').modal('hide');
		this.counterFiber = 1;
		this.highlightFiber1 = true;
		this.myVar = true;
		this.area = 'Fiber Cut at Venkatachalapathy Nagar';
		this.area1 = 'Fiber Cut at Venkatachalapathy Nagar';
		this.distance = 'Police and Road Departmnet vehicles are near to Venkatachalapathy Nagar with a distance of 90m';
		this.hospital = 'Lalitha Govt Hospital';

		this.highlightFiber = true
		this.fiberVal = 1;
		this.lat = 11.115685415326556;
		this.lng = 76.93560495972633;
		this.zoom = 15;
		this.zoomControl = true;
		this.iconUrl = './assets/images/fiber.png';
		this.markers = [
			{
				lat: 11.115685415326556,
				lng: 76.93560495972633,
				// label: 'R', 11.1133404!4d76.9359201
				draggable: false,
				iconUrl: './assets/images/fiber.png',
			},
			{
				lat: 11.12278090594676,
				lng: 76.93536758422852,
				// label: 'R', 11.1133404!4d76.9359201
				draggable: false,
				iconUrl: './assets/images/pd.png',
			},
			{
				lat: 11.110486727157733,
				lng: 76.93663492798805,
				// label: 'R', 11.1133404!4d76.9359201
				draggable: false,
				iconUrl: './assets/images/road.png',
			}
		]
	}

	bombIncident(x) {
		this.bombVariable = true;
		this.getFloodIncidents();
		this.getBombIncidents();
		this.getCityIncidents();
		this.getFiberIncidents();
		// if (x) {
		// 	this.bombObject = x;
		// 	this.bombObject.read = 'Yes';
		// 	this.GS.updateAlert(this.bombObject).subscribe(data => {
		// 	});
		// }
		$('#tabUL a[href="#tab2"]').trigger('click');
		$('#waterlevelPopUpBomb').modal('hide');
		this.counterBomb = 1;
		this.myVar = true;
		this.area = 'Vivek Complex';
		this.area1 = 'City Hospital';
		this.distance = 'Bomb Squad Officers are near to Bank of India with a distance of 100m';
		this.hospital = 'Lalitha Govt Hospital';

		this.highlightBomb = true
		this.bombVal = 1;
		this.lat = 10.970811393517605;
		this.lng = 76.8916368484497;
		this.zoom = 15;
		this.zoomControl = true;
		this.iconUrl = './assets/images/suicase1.png';
		this.markers = [
			{
				lat: 10.970811393517605,
				lng: 76.8916368484497,
				// label: 'R', 10.970811393517605!4d76.8916368484497
				draggable: false,
				iconUrl: './assets/images/suicase1.png',
			},
			{
				lat: 10.972454499300357,
				lng: 76.87906265258789,
				// label: 'R', 10.970811393517605!4d76.8916368484497
				draggable: false,
				iconUrl: './assets/images/bomb.png',
			},
			{
				lat: 10.967560703003675,
				lng: 76.90234556794167,
				// label: 'R', 10.970811393517605!4d76.8916368484497
				draggable: false,
				iconUrl: './assets/images/pl.png',
			}
		]
	}
	trafficIncident(x) {
		// alert('trafc')
		// debugger;
		this.getFloodIncidents();
		this.getBombIncidents();
		this.getCityIncidents();
		this.getFiberIncidents();
		// if (x) {
		// 	this.trafficObject = x;
		// 	this.trafficObject.read = 'Yes';
		// 	this.GS.updateAlert(this.trafficObject).subscribe(data => {
		// 	});
		// }
		$('#tabUL a[href="#tab5"]').trigger('click');
		$('#waterlevelPopUpTraffic').modal('hide');
		this.counterTraffic = 1;
		this.myVar = true;
		this.area = 'Town Hall';
		this.area1 = 'Near Jamath Complex';
		this.distance = 'Traffic Police Vehicle is near to Bank of India with a distance of 100m';
		this.hospital = 'Lalitha Govt Hospital';

		this.highlightTraffic = true
		this.highlightTraffic1 = true
		this.trafficVal = 1;
		this.lat = 10.993316655001879;
		this.lng = 76.95975171402097;
		this.zoom = 18;
		this.zoomControl = true;
		this.iconUrl = './assets/images/traffic.gif';
		this.markers = [
			{
				lat: 10.99330324321405,
				lng: 76.96003317832947,
				// label: 'R',
				draggable: false,
				iconUrl: './assets/images/traffic.gif',
			},
			{
				lat: 10.994535479529782,
				lng: 76.9594806432724,
				// label: 'R',
				draggable: false,
				iconUrl: './assets/images/pd.png',
			},
			{
				lat: 10.99319792363201,
				lng: 76.9586706161499,
				// label: 'R',
				draggable: true,
				iconUrl: './assets/images/pr.png',
			}
		]
	}
	waterIncident(x) {
		this.getFloodIncidents();
		this.getBombIncidents();
		this.getCityIncidents();
		this.getFiberIncidents();

		// x.read = 'Yes';
		// this.GS.updateAlert(x).subscribe(data => {
		// 	alert(JSON.stringify(x));
		// });
		// debugger
		$('#tabUL a[href="#tab4"]').trigger('click');
		$('#waterlevelPopUpWater').modal('hide');
		this.counterWater = 1;
		this.myVar = true;
		this.area = 'Pilluru Dam';
		this.area1 = 'Near Poochamarathur Eco Tourism Resort';
		this.distance = 'Disaster Vehicle is near to Nellithurai with a distance of 190m';
		this.hospital = 'Pilluru Govt Hospital';

		this.highlightWater = true;
		this.highlightFlood = true;
		this.waterVal = 1;
		this.lat = 11.2603955;
		this.lng = 76.8059745;
		this.zoom = 12;
		this.zoomControl = true;
		this.markers = [
			{
				lat: 11.2603955,
				lng: 76.8059745,
				// label: 'R',
				draggable: false,
				iconUrl: '',
			},
			{
				lat: 11.220731503388226,
				lng: 76.77367329597473,
				// label: 'R',
				draggable: true,
				iconUrl: './assets/images/flood.gif',
			},
			{
				lat: 11.267305870377978,
				lng: 76.86259388923645,
				// label: 'R', 11.245739, 76.856897
				draggable: true,
				iconUrl: './assets/images/flood1.png',
			}
		]
	}
	secondFire(x) {
		// debugger;
		this.fireVar = true;
		this.fire2 = '2 Children Dead, One Missing In Annur Camp Fire';
		this.zoom = 12;

		// initial center position for the map
		this.lat = 11.232866;
		this.lng = 77.108450;
		this.zoomControl = true;

		this.counter1 = 1;
		this.highlight1 = true;
		this.myVar = true;
		this.area = 'Ramnathpuram';
		this.distance = '150m';
		this.iconUrl = './assets/images/fires.gif'
		// this.interval();
		this.markers4 = [
			{
				lat: 11.233144,
				lng: 77.104926,
				// label: 'R',
				draggable: false,
				iconUrl: './assets/images/fires.gif',
			},
			{
				"lat": 11.230256,
				"lng": 77.117701,
				// "label": "", 
				"draggable": true,
				"iconUrl": "../assets/images/ambulanceR.png"
			},
			{
				"lat": 11.232704,
				"lng": 77.097124,
				"label": "",
				"draggable": true,
				"iconUrl": "../assets/images/policeL.png"
			}
		]
	}
	getNews(x) {
		// alert(x)
		this.counter = this.counter + 1;
		// this.GS.getLatestNews('Coimbatore', 'Ramnathpuram').subscribe(data => {
		// this.news = data;
		this.news = [{
			"city": "Coimbatore",
			"town": "Ramnathpuram",
			"news1": "Fires brakeout in ramnathpuram at road no 5",
			"news2": "Nearest Hospitals found",
			"news3": "Nearest Fire Station found",
			"news4": "Nearest Police Station found",
			"news5": "Notification sent to nearest hospitals, police stations and fire stations",
			"news6": "Force has started from fire station",
			"news7": "Ambulance near to ramnathpuram road no 5 is started",
			"news8": "2 Ambulances arrived to spot to save the victims",
			"news9": "Preliminary inquiry suggests that the deaths happened due to intense smoke, police sources said.",
			"news10": "12 people were rescued using hydraulic lifts.",
			"news11": "A major market in the heart of Ramnathpuram remains blocked off as a fire raged for the second day in a row at a large shop which sells textiles.",
			"news12": "The fire was nearly extinguished, but the area remained covered in thick smoke",
			"news13": "Govt officials inspected the Incident area"
		}]
		// alert(this.news)
		if (this.counter == 2 && this.highlight == true) {
			var objDiv = document.getElementById("ticker01");
			//alert(objDiv+""+this.myVar)
			objDiv.scrollTop = objDiv.scrollHeight;
			//alert("else2")
			this.news1 = this.news[0].news1;
			this.news1Var = true;
		} else if (this.counter == 3 && this.highlight == true) {
			var objDiv = document.getElementById("ticker01");
			//alert(objDiv+""+this.myVar)
			objDiv.scrollTop = objDiv.scrollHeight;
			//alert("else2")
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news1Var = true;
			this.news2Var = true;
		} else if (this.counter == 4 && this.highlight == true) {
			var objDiv = document.getElementById("ticker01");
			//alert(objDiv+""+this.myVar)
			objDiv.scrollTop = objDiv.scrollHeight;
			//alert("else2")
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
		} else if (this.counter == 5 && this.highlight == true) {
			var objDiv = document.getElementById("ticker01");
			//alert(objDiv+""+this.myVar)
			objDiv.scrollTop = objDiv.scrollHeight;
			//alert("else2")
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
		} else if (this.counter == 6 && this.highlight == true) {
			var objDiv = document.getElementById("ticker01");
			//alert(objDiv+""+this.myVar)
			objDiv.scrollTop = objDiv.scrollHeight;
			//alert("else2")
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
		} else if (this.counter == 7 && this.highlight == true) {
			var objDiv = document.getElementById("ticker01");
			//alert(objDiv+""+this.myVar)
			objDiv.scrollTop = objDiv.scrollHeight;
			//alert("else2")
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news6 = this.news[0].news6;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
			this.news6Var = true;
		} else if (this.counter == 8 && this.highlight == true) {
			var objDiv = document.getElementById("ticker01");
			//alert(objDiv+""+this.myVar)
			objDiv.scrollTop = objDiv.scrollHeight;
			//alert("else2")
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news6 = this.news[0].news6;
			this.news7 = this.news[0].news7;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
			this.news6Var = true;
			this.news7Var = true;
		} else if (this.counter == 9 && this.highlight == true) {
			var objDiv = document.getElementById("ticker01");
			//alert(objDiv+""+this.myVar)
			objDiv.scrollTop = objDiv.scrollHeight;
			//alert("else2")
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news6 = this.news[0].news6;
			this.news7 = this.news[0].news7;
			this.news8 = this.news[0].news8;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
			this.news6Var = true;
			this.news7Var = true;
			this.news8Var = true;
		} else if (this.counter == 10 && this.highlight == true) {
			var objDiv = document.getElementById("ticker01");
			//alert(objDiv+""+this.myVar)
			objDiv.scrollTop = objDiv.scrollHeight;
			//alert("else2")
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news6 = this.news[0].news6;
			this.news7 = this.news[0].news7;
			this.news8 = this.news[0].news8;
			this.news9 = this.news[0].news9;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
			this.news6Var = true;
			this.news7Var = true;
			this.news8Var = true;
			this.news9Var = true;
		} else if (this.counter == 11 && this.highlight == true) {
			var objDiv = document.getElementById("ticker01");
			//alert(objDiv+""+this.myVar)
			objDiv.scrollTop = objDiv.scrollHeight;
			//alert("else2")
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news6 = this.news[0].news6;
			this.news7 = this.news[0].news7;
			this.news8 = this.news[0].news8;
			this.news9 = this.news[0].news9;
			this.news10 = this.news[0].news10;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
			this.news6Var = true;
			this.news7Var = true;
			this.news8Var = true;
			this.news9Var = true;
			this.news10Var = true;
		}
		// this.news1 = this.news[0].news1;
		// this.news2 = this.news[0].news2;
		// this.news3 = this.news[0].news3;
		// this.news4 = this.news[0].news4;
		// this.news5 = this.news[0].news5;
		// this.news6 = this.news[0].news6;
		// this.news7 = this.news[0].news7;
		// this.news8 = this.news[0].news8;
		// this.news9 = this.news[0].news9;

		// });
	}
	getSalemNews(x) {
		this.counterSalem = this.counterSalem + 1;
		this.news = [{
			"city": "Salem",
			"town": "Arisipalayam Main Road",
			"news1": "Fires brakeout in Arisipalayam Main Road",
			"news2": "Nearest Hospitals found",
			"news3": "Nearest Fire Station found",
			"news4": "Nearest Police Station found",
			"news5": "Notification sent to nearest hospitals, police stations and fire stations",
			"news6": "Force has started from fire station",
			"news7": "Ambulance near to Arisipalayam Main Road is started",
			"news8": "2 Ambulances arrived to spot to save the victims",
			"news9": "Preliminary inquiry suggests that the deaths happened due to intense smoke, police sources said.",
			"news10": "12 people were rescued using hydraulic lifts.",
			"news11": "A major market in the heart of Arisipalayam Main Road remains blocked off as a fire raged for the second day in a row at a large shop which sells textiles.",
			"news12": "The fire was nearly extinguished, but the area remained covered in thick smoke",
			"news13": "Govt officials inspected the Incident area"
		}]
		if (this.highlightSalem == true && this.counterSalem == 2) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news1Var = true;
		} else if (this.highlightSalem == true && this.counterSalem == 3) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news1Var = true;
			this.news2Var = true;
		} else if (this.counterSalem == 4 && this.highlightSalem == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
		} else if (this.counterSalem == 5 && this.highlightSalem == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
		} else if (this.counterSalem == 6 && this.highlightSalem == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
		} else if (this.counterSalem == 7 && this.highlightSalem == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news6 = this.news[0].news6;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
			this.news6Var = true;
		} else if (this.counterSalem == 8 && this.highlightSalem == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news6 = this.news[0].news6;
			this.news7 = this.news[0].news7;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
			this.news6Var = true;
			this.news7Var = true;
		} else if (this.counterSalem == 9 && this.highlightSalem == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news6 = this.news[0].news6;
			this.news7 = this.news[0].news7;
			this.news8 = this.news[0].news8;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
			this.news6Var = true;
			this.news7Var = true;
			this.news8Var = true;
		} else if (this.counterSalem == 10 && this.highlightSalem == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news6 = this.news[0].news6;
			this.news7 = this.news[0].news7;
			this.news8 = this.news[0].news8;
			this.news9 = this.news[0].news9;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
			this.news6Var = true;
			this.news7Var = true;
			this.news8Var = true;
			this.news9Var = true;
		} else if (this.counterSalem == 11 && this.highlightSalem == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news6 = this.news[0].news6;
			this.news7 = this.news[0].news7;
			this.news8 = this.news[0].news8;
			this.news9 = this.news[0].news9;
			this.news10 = this.news[0].news10;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
			this.news6Var = true;
			this.news7Var = true;
			this.news8Var = true;
			this.news9Var = true;
			this.news10Var = true;
		}
	}
	getMaduraiNews(x) {
		this.counterMadurai = this.counterMadurai + 1;
		this.news = [{
			"city": "Madurai",
			"town": "Aruppukottai Road",
			"news1": "Fires brakeout in Aruppukottai Main Road",
			"news2": "Nearest Hospitals found",
			"news3": "Nearest Fire Station found",
			"news4": "Nearest Police Station found",
			"news5": "Notification sent to nearest hospitals, police stations and fire stations",
			"news6": "Force has started from fire station",
			"news7": "Ambulance near to Aruppukottai Main Road is started",
			"news8": "2 Ambulances arrived to spot to save the victims",
			"news9": "Preliminary inquiry suggests that the deaths happened due to intense smoke, police sources said.",
			"news10": "12 people were rescued using hydraulic lifts.",
			"news11": "A major market in the heart of Aruppukottai Main Road remains blocked off as a fire raged for the second day in a row at a large shop which sells textiles.",
			"news12": "The fire was nearly extinguished, but the area remained covered in thick smoke",
			"news13": "Govt officials inspected the Incident area"
		}]
		if (this.highlightMadurai == true && this.counterMadurai == 2) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news1Var = true;
		} else if (this.highlightMadurai == true && this.counterMadurai == 3) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news1Var = true;
			this.news2Var = true;
		} else if (this.counterMadurai == 4 && this.highlightMadurai == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
		} else if (this.counterMadurai == 5 && this.highlightMadurai == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
		} else if (this.counterMadurai == 6 && this.highlightMadurai == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
		} else if (this.counterMadurai == 7 && this.highlightMadurai == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news6 = this.news[0].news6;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
			this.news6Var = true;
		} else if (this.counterMadurai == 8 && this.highlightMadurai == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news6 = this.news[0].news6;
			this.news7 = this.news[0].news7;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
			this.news6Var = true;
			this.news7Var = true;
		} else if (this.counterMadurai == 9 && this.highlightMadurai == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news6 = this.news[0].news6;
			this.news7 = this.news[0].news7;
			this.news8 = this.news[0].news8;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
			this.news6Var = true;
			this.news7Var = true;
			this.news8Var = true;
		} else if (this.counterMadurai == 10 && this.highlightMadurai == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news6 = this.news[0].news6;
			this.news7 = this.news[0].news7;
			this.news8 = this.news[0].news8;
			this.news9 = this.news[0].news9;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
			this.news6Var = true;
			this.news7Var = true;
			this.news8Var = true;
			this.news9Var = true;
		} else if (this.counterMadurai == 11 && this.highlightMadurai == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news6 = this.news[0].news6;
			this.news7 = this.news[0].news7;
			this.news8 = this.news[0].news8;
			this.news9 = this.news[0].news9;
			this.news10 = this.news[0].news10;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
			this.news6Var = true;
			this.news7Var = true;
			this.news8Var = true;
			this.news9Var = true;
			this.news10Var = true;
		}
	}
	getSmokeNews(x) {

		this.counterSmoke = this.counterSmoke + 1;
		this.news = [{
			"city": "Coimbatore",
			"town": "Kalveerampalayam near Sowdambika gardens Building no 9",
			"news1": "Fires brakeout in Kalveerampalayam at road no 8",
			"news2": "Nearest Hospitals found",
			"news3": "Nearest Fire Station found",
			"news4": "Nearest Police Station found",
			"news5": "Notification sent to nearest hospitals, police stations and fire stations",
			"news6": "Force has started from fire station",
			"news7": "Ambulance near to Sowdambika gardens road no 5 is started",
			"news8": "2 Ambulances arrived to spot to save the victims",
			"news9": "Preliminary inquiry suggests that the deaths happened due to intense smoke, police sources said.",
			"news10": "12 people were rescued using hydraulic lifts.",
			"news11": "A major market in the heart of Sowdambika gardens remains blocked off as a fire raged for the second day in a row at a large shop which sells textiles.",
			"news12": "The fire was nearly extinguished, but the area remained covered in thick smoke",
			"news13": "Govt officials inspected the Incident area"
		}]
		if (this.highlightSmoke == true && this.counterSmoke == 2) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news1Var = true;
		} else if (this.highlightSmoke == true && this.counterSmoke == 3) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news1Var = true;
			this.news2Var = true;
		} else if (this.counterSmoke == 4 && this.highlightSmoke == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
		} else if (this.counterSmoke == 5 && this.highlightSmoke == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
		} else if (this.counterSmoke == 6 && this.highlightSmoke == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
		} else if (this.counterSmoke == 7 && this.highlightSmoke == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news6 = this.news[0].news6;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
			this.news6Var = true;
		} else if (this.counterSmoke == 8 && this.highlightSmoke == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news6 = this.news[0].news6;
			this.news7 = this.news[0].news7;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
			this.news6Var = true;
			this.news7Var = true;
		} else if (this.counterSmoke == 9 && this.highlightSmoke == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news6 = this.news[0].news6;
			this.news7 = this.news[0].news7;
			this.news8 = this.news[0].news8;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
			this.news6Var = true;
			this.news7Var = true;
			this.news8Var = true;
		} else if (this.counterSmoke == 10 && this.highlightSmoke == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news6 = this.news[0].news6;
			this.news7 = this.news[0].news7;
			this.news8 = this.news[0].news8;
			this.news9 = this.news[0].news9;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
			this.news6Var = true;
			this.news7Var = true;
			this.news8Var = true;
			this.news9Var = true;
		} else if (this.counterSmoke == 11 && this.highlightSmoke == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news6 = this.news[0].news6;
			this.news7 = this.news[0].news7;
			this.news8 = this.news[0].news8;
			this.news9 = this.news[0].news9;
			this.news10 = this.news[0].news10;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
			this.news6Var = true;
			this.news7Var = true;
			this.news8Var = true;
			this.news9Var = true;
			this.news10Var = true;
		}
	}
	getThanjavurNews(x) {
		this.counterThanjavur = this.counterThanjavur + 1;

		this.news = [{
			"city": "Thanjavur",
			"town": "Near Anjaneya Swamy Temple",
			"news1": "Water Level Rises more than 97ft in Kaveri Dam",
			"news2": "Social Media Channels notified",
			"news3": "SMS/Email sent to all residents",
			"news4": "Request Sent to Drainage Pumping Station",
			"news5": "No drainage pumping vehicles currently available",
			"news6": "1 Drainage vehicle available",
			"news7": "Drainage Pumping vehicle has been dispatched to the location",
			"news8": "Drainage pumping vehicle has reached the point",
			"news9": "Excess water has been pumped out",
			"news10": "Notify Social Media that appropriate action has been taken by authority",
		}]
		if (this.highlightThanjavur == true && this.counterThanjavur == 2) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news1Var = true;
		} else if (this.highlightThanjavur == true && this.counterThanjavur == 3) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news1Var = true;
			this.news2Var = true;
		} else if (this.counterThanjavur == 4 && this.highlightThanjavur == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
		} else if (this.counterThanjavur == 5 && this.highlightThanjavur == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
		} else if (this.counterThanjavur == 6 && this.highlightThanjavur == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
		} else if (this.counterThanjavur == 7 && this.highlightThanjavur == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news6 = this.news[0].news6;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
			this.news6Var = true;
		} else if (this.counterThanjavur == 8 && this.highlightThanjavur == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news6 = this.news[0].news6;
			this.news7 = this.news[0].news7;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
			this.news6Var = true;
			this.news7Var = true;
		} else if (this.counterThanjavur == 9 && this.highlightThanjavur == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news6 = this.news[0].news6;
			this.news7 = this.news[0].news7;
			this.news8 = this.news[0].news8;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
			this.news6Var = true;
			this.news7Var = true;
			this.news8Var = true;
		} else if (this.counterThanjavur == 10 && this.highlightThanjavur == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news6 = this.news[0].news6;
			this.news7 = this.news[0].news7;
			this.news8 = this.news[0].news8;
			this.news9 = this.news[0].news9;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
			this.news6Var = true;
			this.news7Var = true;
			this.news8Var = true;
			this.news9Var = true;
		} else if (this.counterThanjavur == 11 && this.highlightThanjavur == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news6 = this.news[0].news6;
			this.news7 = this.news[0].news7;
			this.news8 = this.news[0].news8;
			this.news9 = this.news[0].news9;
			this.news10 = this.news[0].news10;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
			this.news6Var = true;
			this.news7Var = true;
			this.news8Var = true;
			this.news9Var = true;
			this.news10Var = true;
		}
	}
	getWaterNews(x) {

		this.counterWater = this.counterWater + 1;

		this.news = [{
			"city": "Coimbatore",
			"town": "Near Poochamarathur Eco Tourism Resort",
			"news1": "Water Level Rises more than 97ft in Pilluru Dam",
			"news2": "Social Media Channels notified",
			"news3": "SMS/Email sent to all residents",
			"news4": "Request Sent to Drainage Pumping Station",
			"news5": "No drainage pumping vehicles currently available",
			"news6": "1 Drainage vehicle available",
			"news7": "Drainage Pumping vehicle has been dispatched to the location",
			"news8": "Drainage pumping vehicle has reached the point",
			"news9": "Excess water has been pumped out",
			"news10": "Notify Social Media that appropriate action has been taken by authority",
		}]
		if (this.highlightWater == true && this.counterWater == 2) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news1Var = true;
		} else if (this.highlightWater == true && this.counterWater == 3) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news1Var = true;
			this.news2Var = true;
		} else if (this.counterWater == 4 && this.highlightWater == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
		} else if (this.counterWater == 5 && this.highlightWater == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
		} else if (this.counterWater == 6 && this.highlightWater == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
		} else if (this.counterWater == 7 && this.highlightWater == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news6 = this.news[0].news6;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
			this.news6Var = true;
		} else if (this.counterWater == 8 && this.highlightWater == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news6 = this.news[0].news6;
			this.news7 = this.news[0].news7;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
			this.news6Var = true;
			this.news7Var = true;
		} else if (this.counterWater == 9 && this.highlightWater == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news6 = this.news[0].news6;
			this.news7 = this.news[0].news7;
			this.news8 = this.news[0].news8;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
			this.news6Var = true;
			this.news7Var = true;
			this.news8Var = true;
		} else if (this.counterWater == 10 && this.highlightWater == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news6 = this.news[0].news6;
			this.news7 = this.news[0].news7;
			this.news8 = this.news[0].news8;
			this.news9 = this.news[0].news9;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
			this.news6Var = true;
			this.news7Var = true;
			this.news8Var = true;
			this.news9Var = true;
		} else if (this.counterWater == 11 && this.highlightWater == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news6 = this.news[0].news6;
			this.news7 = this.news[0].news7;
			this.news8 = this.news[0].news8;
			this.news9 = this.news[0].news9;
			this.news10 = this.news[0].news10;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
			this.news6Var = true;
			this.news7Var = true;
			this.news8Var = true;
			this.news9Var = true;
			this.news10Var = true;
		}
	}
	getVelloreNews(x) {

		this.counterVellore = this.counterVellore + 1;

		this.news = [{
			"city": "Vellore",
			"town": "Near Vellore Fort",
			"news1": "Water Level Rises more than 97ft in Pallar River",
			"news2": "Social Media Channels notified",
			"news3": "SMS/Email sent to all residents",
			"news4": "Request Sent to Drainage Pumping Station",
			"news5": "No drainage pumping vehicles currently available",
			"news6": "1 Drainage vehicle available",
			"news7": "Drainage Pumping vehicle has been dispatched to the location",
			"news8": "Drainage pumping vehicle has reached the point",
			"news9": "Excess water has been pumped out",
			"news10": "Notify Social Media that appropriate action has been taken by authority",
		}]
		if (this.highlightVellore == true && this.counterVellore == 2) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news1Var = true;
		} else if (this.highlightVellore == true && this.counterVellore == 3) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news1Var = true;
			this.news2Var = true;
		} else if (this.counterVellore == 4 && this.highlightVellore == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
		} else if (this.counterVellore == 5 && this.highlightVellore == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
		} else if (this.counterVellore == 6 && this.highlightVellore == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
		} else if (this.counterVellore == 7 && this.highlightVellore == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news6 = this.news[0].news6;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
			this.news6Var = true;
		} else if (this.counterVellore == 8 && this.highlightVellore == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news6 = this.news[0].news6;
			this.news7 = this.news[0].news7;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
			this.news6Var = true;
			this.news7Var = true;
		} else if (this.counterVellore == 9 && this.highlightVellore == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news6 = this.news[0].news6;
			this.news7 = this.news[0].news7;
			this.news8 = this.news[0].news8;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
			this.news6Var = true;
			this.news7Var = true;
			this.news8Var = true;
		} else if (this.counterVellore == 10 && this.highlightVellore == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news6 = this.news[0].news6;
			this.news7 = this.news[0].news7;
			this.news8 = this.news[0].news8;
			this.news9 = this.news[0].news9;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
			this.news6Var = true;
			this.news7Var = true;
			this.news8Var = true;
			this.news9Var = true;
		} else if (this.counterVellore == 11 && this.highlightVellore == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news6 = this.news[0].news6;
			this.news7 = this.news[0].news7;
			this.news8 = this.news[0].news8;
			this.news9 = this.news[0].news9;
			this.news10 = this.news[0].news10;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
			this.news6Var = true;
			this.news7Var = true;
			this.news8Var = true;
			this.news9Var = true;
			this.news10Var = true;
		}
	}
	getTrafficNews(x) {

		this.counterTraffic = this.counterTraffic + 1;

		this.news = [{
			"city": "Coimbatore",
			"town": "Near Near Jamath Complex",
			"news1": "Heavy Traffic at Jamath Complex",
			"news2": "Request sent to GIS to check if reported heavy traffic is true",
			"news3": "Heavy traffic shown as per GIS Data",
			"news4": "Nearest traffic police station found.",
			"news5": "Request send to traffic police station to attend to the traffic problem",
			"news6": "Traffic beat cop dispatched to the location",
			"news7": "Traffic cop reached the location and is controlling the situation",
			"news8": "Traffic situation eased",
			"news9": "GIS Data confirms that traffic has been updated to amber from red",
			"news10": "Traffic got cleared and people is happy that it was cleared early.",

		}]
		if (this.highlightTraffic == true && this.counterTraffic == 2) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news1Var = true;
		} else if (this.highlightTraffic == true && this.counterTraffic == 3) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news1Var = true;
			this.news2Var = true;
		} else if (this.counterTraffic == 4 && this.highlightTraffic == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
		} else if (this.counterTraffic == 5 && this.highlightTraffic == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
		} else if (this.counterTraffic == 6 && this.highlightTraffic == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
		} else if (this.counterTraffic == 7 && this.highlightTraffic == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news6 = this.news[0].news6;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
			this.news6Var = true;
		} else if (this.counterTraffic == 8 && this.highlightTraffic == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news6 = this.news[0].news6;
			this.news7 = this.news[0].news7;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
			this.news6Var = true;
			this.news7Var = true;
		} else if (this.counterTraffic == 9 && this.highlightTraffic == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news6 = this.news[0].news6;
			this.news7 = this.news[0].news7;
			this.news8 = this.news[0].news8;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
			this.news6Var = true;
			this.news7Var = true;
			this.news8Var = true;
		} else if (this.counterTraffic == 10 && this.highlightTraffic == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news6 = this.news[0].news6;
			this.news7 = this.news[0].news7;
			this.news8 = this.news[0].news8;
			this.news9 = this.news[0].news9;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
			this.news6Var = true;
			this.news7Var = true;
			this.news8Var = true;
			this.news9Var = true;
		} else if (this.counterTraffic == 11 && this.highlightTraffic == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news6 = this.news[0].news6;
			this.news7 = this.news[0].news7;
			this.news8 = this.news[0].news8;
			this.news9 = this.news[0].news9;
			this.news10 = this.news[0].news10;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
			this.news6Var = true;
			this.news7Var = true;
			this.news8Var = true;
			this.news9Var = true;
			this.news10Var = true;
		}
	}
	getTiruchirapalliNews(x) {

		this.counterTiruchirapalli = this.counterTiruchirapalli + 1;

		this.news = [{
			"city": "Tiruchirapalli",
			"town": "AriyaMangalam Zone",
			"news1": "Heavy Traffic at AriyaMangalam Zone",
			"news2": "Request sent to GIS to check if reported heavy traffic is true",
			"news3": "Heavy traffic shown as per GIS Data",
			"news4": "Nearest traffic police station found.",
			"news5": "Request send to traffic police station to attend to the traffic problem",
			"news6": "Traffic beat cop dispatched to the location",
			"news7": "Traffic cop reached the location and is controlling the situation",
			"news8": "Traffic situation eased",
			"news9": "GIS Data confirms that traffic has been updated to amber from red",
			"news10": "Traffic got cleared and people is happy that it was cleared early.",

		}]
		if (this.highlightTiruchirapalli == true && this.counterTiruchirapalli == 2) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news1Var = true;
		} else if (this.highlightTiruchirapalli == true && this.counterTiruchirapalli == 3) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news1Var = true;
			this.news2Var = true;
		} else if (this.counterTiruchirapalli == 4 && this.highlightTiruchirapalli == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
		} else if (this.counterTiruchirapalli == 5 && this.highlightTiruchirapalli == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
		} else if (this.counterTiruchirapalli == 6 && this.highlightTiruchirapalli == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
		} else if (this.counterTiruchirapalli == 7 && this.highlightTiruchirapalli == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news6 = this.news[0].news6;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
			this.news6Var = true;
		} else if (this.counterTiruchirapalli == 8 && this.highlightTiruchirapalli == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news6 = this.news[0].news6;
			this.news7 = this.news[0].news7;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
			this.news6Var = true;
			this.news7Var = true;
		} else if (this.counterTiruchirapalli == 9 && this.highlightTiruchirapalli == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news6 = this.news[0].news6;
			this.news7 = this.news[0].news7;
			this.news8 = this.news[0].news8;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
			this.news6Var = true;
			this.news7Var = true;
			this.news8Var = true;
		} else if (this.counterTiruchirapalli == 10 && this.highlightTiruchirapalli == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news6 = this.news[0].news6;
			this.news7 = this.news[0].news7;
			this.news8 = this.news[0].news8;
			this.news9 = this.news[0].news9;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
			this.news6Var = true;
			this.news7Var = true;
			this.news8Var = true;
			this.news9Var = true;
		} else if (this.counterTiruchirapalli == 11 && this.highlightTiruchirapalli == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news6 = this.news[0].news6;
			this.news7 = this.news[0].news7;
			this.news8 = this.news[0].news8;
			this.news9 = this.news[0].news9;
			this.news10 = this.news[0].news10;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
			this.news6Var = true;
			this.news7Var = true;
			this.news8Var = true;
			this.news9Var = true;
			this.news10Var = true;
		}
	}
	getBombNews(x) {
		this.counterBomb = this.counterBomb + 1;

		this.news = [{
			"city": "Coimbatore",
			"town": "City Hospital Vivek Complex",
			"news1": "Abandoned suitcase found near City Hospital Vivek Complex.",
			"news2": "Notification sent to police stations",
			"news3": "Force has been dispatched from Police Station",
			"news4": "Police force has arrived at the reported location",
			"news5": "Police has engaged in crowd control in the surrounding area",
			"news6": "Bomb squad has been notified",
			"news7": "Bomb squad has arrived at the location",
			"news8": "Bomb squad has analyzed the problem and bomb was detected and defuse is in progress",
			"news9": "People have been evacuated from the location",
			"news10": "Bomb has been defused",
		}]
		if (this.highlightBomb == true && this.counterBomb == 2) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;

			this.news1 = this.news[0].news1;
			this.news1Var = true;
		} else if (this.highlightBomb == true && this.counterBomb == 3) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;

			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news1Var = true;
			this.news2Var = true;
		} else if (this.counterBomb == 4 && this.highlightBomb == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;

			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
		} else if (this.counterBomb == 5 && this.highlightBomb == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;

			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
		} else if (this.counterBomb == 6 && this.highlightBomb == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;

			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
		} else if (this.counterBomb == 7 && this.highlightBomb == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;

			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news6 = this.news[0].news6;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
			this.news6Var = true;
		} else if (this.counterBomb == 8 && this.highlightBomb == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;

			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news6 = this.news[0].news6;
			this.news7 = this.news[0].news7;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
			this.news6Var = true;
			this.news7Var = true;
		} else if (this.counterBomb == 9 && this.highlightBomb == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;

			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news6 = this.news[0].news6;
			this.news7 = this.news[0].news7;
			this.news8 = this.news[0].news8;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
			this.news6Var = true;
			this.news7Var = true;
			this.news8Var = true;
		} else if (this.counterBomb == 10 && this.highlightBomb == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;

			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news6 = this.news[0].news6;
			this.news7 = this.news[0].news7;
			this.news8 = this.news[0].news8;
			this.news9 = this.news[0].news9;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
			this.news6Var = true;
			this.news7Var = true;
			this.news8Var = true;
			this.news9Var = true;
		} else if (this.counterBomb == 11 && this.highlightBomb == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;

			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news6 = this.news[0].news6;
			this.news7 = this.news[0].news7;
			this.news8 = this.news[0].news8;
			this.news9 = this.news[0].news9;
			this.news10 = this.news[0].news10;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
			this.news6Var = true;
			this.news7Var = true;
			this.news8Var = true;
			this.news9Var = true;
			this.news10Var = true;
		}
	}
	getTransNews(x) {
		this.counterCamera = this.counterCamera + 1;

		this.news = [{

			"city": "Coimbatore",
			"town": "Gopalapuram",
			"news1": "Smart Surveillance picks up high higher concentration of people at event",
			"news2": "ICCC Notified of possible stampede situation",
			"news3": "Incident recorded by ICCC and forwarded for incident management",
			"news4": "Nearest Traffic police stattion found",
			"news5": "Respective traffic police station notified of the potential stampede problem",
			"news6": "Traffic police force dispatched to the location",
			"news7": "Traffic police arrived",
			"news8": "Traffic police assessing the situation",
			"news9": "Authorities have been notified to penalize the event co-ordinator",
			"news10": "Appreciated reported citizens."
		}]
		if (this.highlightCamera == true && this.counterCamera == 2) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;

			this.news1 = this.news[0].news1;
			this.news1Var = true;
		} else if (this.highlightCamera == true && this.counterCamera == 3) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;

			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news1Var = true;
			this.news2Var = true;
		} else if (this.counterCamera == 4 && this.highlightCamera == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;

			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
		} else if (this.counterCamera == 5 && this.highlightCamera == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;
			
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
		} else if (this.counterCamera == 6 && this.highlightCamera == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;

			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
		} else if (this.counterCamera == 7 && this.highlightCamera == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;

			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news6 = this.news[0].news6;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
			this.news6Var = true;
		} else if (this.counterCamera == 8 && this.highlightCamera == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;

			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news6 = this.news[0].news6;
			this.news7 = this.news[0].news7;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
			this.news6Var = true;
			this.news7Var = true;
		} else if (this.counterCamera == 9 && this.highlightCamera == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;

			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news6 = this.news[0].news6;
			this.news7 = this.news[0].news7;
			this.news8 = this.news[0].news8;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
			this.news6Var = true;
			this.news7Var = true;
			this.news8Var = true;
		} else if (this.counterCamera == 10 && this.highlightCamera == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;

			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news6 = this.news[0].news6;
			this.news7 = this.news[0].news7;
			this.news8 = this.news[0].news8;
			this.news9 = this.news[0].news9;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
			this.news6Var = true;
			this.news7Var = true;
			this.news8Var = true;
			this.news9Var = true;
		} else if (this.counterCamera == 11 && this.highlightCamera == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;

			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news6 = this.news[0].news6;
			this.news7 = this.news[0].news7;
			this.news8 = this.news[0].news8;
			this.news9 = this.news[0].news9;
			this.news10 = this.news[0].news10;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
			this.news6Var = true;
			this.news7Var = true;
			this.news8Var = true;
			this.news9Var = true;
			this.news10Var = true;
		}
	}
	getTiruppurNews(x) {
		this.counterTiruppur = this.counterTiruppur + 1;

		this.news = [{

			"city": "Tiruppur",
			"town": "Avinashi",
			"news1": "Smart Surveillance picks up high higher concentration of people at event Avinashi",
			"news2": "ICCC Notified of possible stampede situation",
			"news3": "Incident recorded by ICCC and forwarded for incident management",
			"news4": "Nearest Traffic police stattion found",
			"news5": "Respective traffic police station notified of the potential stampede problem",
			"news6": "Traffic police force dispatched to the location",
			"news7": "Traffic police arrived",
			"news8": "Traffic police assessing the situation",
			"news9": "Authorities have been notified to penalize the event co-ordinator",
			"news10": "Appreciated reported citizens."
		}]
		if (this.highlightTiruppur == true && this.counterTiruppur == 2) {
			this.news1 = this.news[0].news1;
			this.news1Var = true;
		} else if (this.highlightTiruppur == true && this.counterTiruppur == 3) {
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news1Var = true;
			this.news2Var = true;
		} else if (this.counterTiruppur == 4 && this.highlightTiruppur == true) {
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
		} else if (this.counterTiruppur == 5 && this.highlightTiruppur == true) {
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
		} else if (this.counterTiruppur == 6 && this.highlightTiruppur == true) {
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
		} else if (this.counterTiruppur == 7 && this.highlightTiruppur == true) {
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news6 = this.news[0].news6;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
			this.news6Var = true;
		} else if (this.counterTiruppur == 8 && this.highlightTiruppur == true) {
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news6 = this.news[0].news6;
			this.news7 = this.news[0].news7;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
			this.news6Var = true;
			this.news7Var = true;
		} else if (this.counterTiruppur == 9 && this.highlightTiruppur == true) {
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news6 = this.news[0].news6;
			this.news7 = this.news[0].news7;
			this.news8 = this.news[0].news8;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
			this.news6Var = true;
			this.news7Var = true;
			this.news8Var = true;
		} else if (this.counterTiruppur == 10 && this.highlightTiruppur == true) {
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news6 = this.news[0].news6;
			this.news7 = this.news[0].news7;
			this.news8 = this.news[0].news8;
			this.news9 = this.news[0].news9;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
			this.news6Var = true;
			this.news7Var = true;
			this.news8Var = true;
			this.news9Var = true;
		} else if (this.counterTiruppur == 11 && this.highlightTiruppur == true) {
			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news6 = this.news[0].news6;
			this.news7 = this.news[0].news7;
			this.news8 = this.news[0].news8;
			this.news9 = this.news[0].news9;
			this.news10 = this.news[0].news10;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
			this.news6Var = true;
			this.news7Var = true;
			this.news8Var = true;
			this.news9Var = true;
			this.news10Var = true;
		}
	}
	getFiberNews(x) {
		this.counterFiber = this.counterFiber + 1;

		this.news = [{
			"city": "Coimbatore",
			"town": "Venkata Chalapathy Nagar",
			"news1": "ArcGIS reports cut in Fiber in Venkata Chalapathy Nagar",
			"news2": "Nearest PWD department has been located",
			"news3": "Located PWD department notified of the problem",
			"news4": "Nearest traffic police station has been located",
			"news5": "Traffic Beat Cop has been dispatched to divert traffic",
			"news6": "Fiber Infra company has been notified to fix the broken cable",
			"news7": "Fiber Technicians assigned to the scene",
			"news8": "Traffic diversion installed",
			"news9": "The broken fibre has been fixed and service is restored",
			"news10": "Road work has been completed and Traffic condition restored to previous",
		}]
		if (this.highlightFiber == true && this.counterFiber == 2) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;

			this.news1 = this.news[0].news1;
			this.news1Var = true;
		} else if (this.highlightFiber == true && this.counterFiber == 3) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;

			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news1Var = true;
			this.news2Var = true;
		} else if (this.counterFiber == 4 && this.highlightFiber == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;

			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
		} else if (this.counterFiber == 5 && this.highlightFiber == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;

			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
		} else if (this.counterFiber == 6 && this.highlightFiber == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;

			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
		} else if (this.counterFiber == 7 && this.highlightFiber == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;

			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news6 = this.news[0].news6;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
			this.news6Var = true;
		} else if (this.counterFiber == 8 && this.highlightFiber == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;

			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news6 = this.news[0].news6;
			this.news7 = this.news[0].news7;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
			this.news6Var = true;
			this.news7Var = true;
		} else if (this.counterFiber == 9 && this.highlightFiber == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;

			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news6 = this.news[0].news6;
			this.news7 = this.news[0].news7;
			this.news8 = this.news[0].news8;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
			this.news6Var = true;
			this.news7Var = true;
			this.news8Var = true;
		} else if (this.counterFiber == 10 && this.highlightFiber == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;

			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news6 = this.news[0].news6;
			this.news7 = this.news[0].news7;
			this.news8 = this.news[0].news8;
			this.news9 = this.news[0].news9;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
			this.news6Var = true;
			this.news7Var = true;
			this.news8Var = true;
			this.news9Var = true;
		} else if (this.counterFiber == 11 && this.highlightFiber == true) {
			var objDiv = document.getElementById("ticker01");
			objDiv.scrollTop = objDiv.scrollHeight;

			this.news1 = this.news[0].news1;
			this.news2 = this.news[0].news2;
			this.news3 = this.news[0].news3;
			this.news4 = this.news[0].news4;
			this.news5 = this.news[0].news5;
			this.news6 = this.news[0].news6;
			this.news7 = this.news[0].news7;
			this.news8 = this.news[0].news8;
			this.news9 = this.news[0].news9;
			this.news10 = this.news[0].news10;
			this.news1Var = true;
			this.news2Var = true;
			this.news3Var = true;
			this.news4Var = true;
			this.news5Var = true;
			this.news6Var = true;
			this.news7Var = true;
			this.news8Var = true;
			this.news9Var = true;
			this.news10Var = true;
		}
	}
	camera(x) {
		$('#tabUL a[href="#tab3"]').trigger('click');
		$('#waterlevelPopUpCam1').modal('hide');
		this.cameraVar = true;
		this.cameraVar1 = true;
		this.cam1 = 'More than 200 people participated in the rally';
		this.counterCamera = 1;
		this.cameraVal = 1;
		this.highlightCamera = true;
		this.myVar = true;
		this.area = 'Gopalapuram';
		this.area1 = 'Gopalapuram road no 6';
		this.distance = '100m from Kotak Mahindra Bank';
		this.hospital = 'Gopalapuram Hospital'
		this.zoom = 16;
		this.lat = 11.003798150909669;
		this.lng = 76.97230763733387;
		this.zoomControl = true;
		this.markers = [];
		this.markers = [
			{
				lat: 11.003798150909669,
				lng: 76.97230763733387,
				// label: 'R',
				draggable: true,
				iconUrl: './assets/images/rally.gif',
			},
			{
				lat: 11.007757998902179,
				lng: 76.97843313217163,
				// label: 'R',
				draggable: true,
				iconUrl: './assets/images/policeR.png',
			}

		]
	}
	tirunelveliTrafficIncident(x) {
		$('#tabUL a[href="#tab3"]').trigger('click');
		this.getFloodIncidents();
		this.getBombIncidents();
		this.getCityIncidents();
		this.getFiberIncidents();
		let cameraPostObj = {
			"citizenId": "5af03877aa9cae2e42c1e62b",
			"citizenName": "sairam",
			"citizenMobileNumber": "7799849440",
			"areaName": "R S Puram",
			"department": "Revenue",
			"description": 'More than 200 people participated in the rally in tirunelveli.',
			"alertType": "Smart public events management",
			"city": "Tirunelveli",
			"read": "No",
			"count": 1
		}
		this.GS.postAlert(cameraPostObj).subscribe(dataPost => {

		});
		this.tirunelveliVar = true;
		this.cameraVar1 = true;
		// this.cam1 = 'More than 200 people participated in the rally';
		this.counterTirunelveli = 1;
		this.tirunelveliVal = 1;
		this.highlightTirunelveli = true;
		this.myVar = true;
		this.area = 'KTC Nagar';
		this.area1 = 'KTC Nagar';
		this.distance = '100m from TMF';
		this.hospital = 'Avinashi  Hospital'
		this.zoom = 15;
		this.lat = 8.7205945,
		this.lng = 77.77428;
		this.zoomControl = true;
		this.markers = [];
		this.markers = [
			{
				lat: 8.713746,
				lng: 77.772015,
				// label: 'R', 10.8321013!4d78.8176763 11.121628, 77.382435
				draggable: true,
				iconUrl: './assets/images/rally.gif',
			},
			{
				lat: 8.713236681388207,
				lng: 77.78383972820518,
				// label: 'R', 11.724581, 78.067750 
				draggable: true,
				iconUrl: '../assets/images/policeR.png',
			}
			
		]
	}
	tiruppurEventIncident(x) {
		$('#tabUL a[href="#tab3"]').trigger('click');
		this.getFloodIncidents();
		this.getBombIncidents();
		this.getCityIncidents();
		this.getFiberIncidents();
		let cameraPostObj = {
			"citizenId": "5af03877aa9cae2e42c1e62b",
			"citizenName": "sairam",
			"citizenMobileNumber": "7799849440",
			"areaName": "R S Puram",
			"department": "Revenue",
			"description": 'More than 200 people participated in the rally.',
			"alertType": "Smart public events management",
			"city": "Tiruppur",
			"read": "No",
			"count": 1
		}
		this.GS.postAlert(cameraPostObj).subscribe(dataPost => {

		});
		this.tiruppurVar = true;
		this.cameraVar1 = true;
		// this.cam1 = 'More than 200 people participated in the rally';
		this.counterTiruppur = 1;
		this.tiruppurVal = 1;
		this.highlightTiruppur = true;
		this.myVar = true;
		this.area = 'Avinashi';
		this.area1 = 'Avinashi  road no 6';
		this.distance = '100m from TMF';
		this.hospital = 'Avinashi  Hospital'
		this.zoom = 17;
		this.lat = 11.1085242;
		this.lng = 77.3410656;
		this.zoomControl = true;
		this.markers = [];
		this.markers = [
			{
				lat: 11.10806336066603,
				lng: 77.34113216400146,
				// label: 'R', 10.8321013!4d78.8176763 11.121628, 77.382435
				draggable: true,
				iconUrl: './assets/images/rally.gif',
			},
			{
				lat: 11.109126670298771,
				lng: 77.34428644180298,
				// label: 'R', 11.724581, 78.067750 
				draggable: true,
				iconUrl: '../assets/images/policeR.png',
			}
			
		]
	}
	tiruchirapalliTrafficIncident(x) {
		this.getFloodIncidents();
		this.getBombIncidents();
		this.getCityIncidents();
		this.getFiberIncidents();
		let trafficPostObj = {
			"citizenId": "5af03877aa9cae2e42c1e62b",
			"citizenName": "sairam",
			"citizenMobileNumber": "7799849440",
			"areaName": "R S Puram",
			"department": "Revenue",
			"description": 'Heavy Traffic at AriyaMangalam Zone',
			"alertType": "City Traffic",
			"city": "Tiruchirapalli",
			"read": "No",
			"count": 1
		}
		this.GS.postAlert(trafficPostObj).subscribe(dataPost => {

		});
		$('#tabUL a[href="#tab5"]').trigger('click');
		this.counterTiruchirapalli = 1;
		this.myVar = true;
		this.area = 'Sangillyandapuram';
		this.area1 = 'AriyaMangalam Zone';
		this.distance = 'Traffic Police Vehicle is near to Pentecostal Mission Church with a distance of 100m';
		this.hospital = 'Pentecostal Govt Hospital';

		this.highlightTiruchirapalli = true
		this.highlightTraffic1 = true;
		this.tiruchirapalliVal = 1;

		this.zoom = 16;
		this.lat = 10.7904833;
		this.lng = 78.7046725;
		this.zoomControl = true;
		this.markers = [];
		this.markers = [
			{
				lat: 10.790372611414844,
				lng: 78.70502471923828,
				// label: 'R', 10.8321013!4d78.8176763
				draggable: true,
				iconUrl: './assets/images/traffic.gif',
			},
			{
				lat: 10.789276539217528,
				lng: 78.69802951812744,
				// label: 'R', 11.724581, 78.067750
				draggable: true,
				iconUrl: '../assets/images/pr.png',
			}
			
		]
	}
	velloreFloodIncident(x) {
		let waterPostObj = {
			"citizenId": "5af03877aa9cae2e42c1e62b",
			"citizenName": "sairam",
			"citizenMobileNumber": "7799849440",
			"areaName": "R S Puram",
			"department": "Revenue",
			"description": 'Flood Increases at Palar River',
			"alertType": "Flooding",
			"city": "Vellore",
			"read": "No",
			"count": 1
		}
		this.GS.postAlert(waterPostObj).subscribe(dataPost => {

		});
		$('#tabUL a[href="#tab4"]').trigger('click');
		// $('#waterlevelPopUpWater').modal('hide');
		this.counterVellore = 1;
		this.myVar = true;
		this.area = 'Palar River';
		this.area1 = 'Near Vellore Fort';
		this.distance = 'Disaster Vehicle is near to Kotak Mahindra Bank with a distance of 190m';
		this.hospital = 'Vellore Fort Govt Hospital';

		this.highlightVellore = true;
		this.highlightFlood = true;
		this.velloreVal = 1;
		this.zoom = 16;
		this.lat = 12.9165167;
		this.lng = 79.1324986;
		this.zoomControl = true;
		this.markers = [];
		this.markers = [
			{
				lat:12.921494733141406,
				lng:79.13131389766932,
				// label: 'R', 10.8321013!4d78.8176763
				draggable: true,
				iconUrl: './assets/images/flood.gif',
			},
			{
				lat:12.916104012216014,
				lng:79.13259029388428,
				// label: 'R', 11.724581, 78.067750
				draggable: true,
				iconUrl: '../assets/images/flood4.png',
			}
			
		]
	}
	thanjavurFloodIncident(x) {
		let waterPostObj = {
			"citizenId": "5af03877aa9cae2e42c1e62b",
			"citizenName": "sairam",
			"citizenMobileNumber": "7799849440",
			"areaName": "R S Puram",
			"department": "Revenue",
			"description": 'Flood Increases at Anjaneya Temple',
			"alertType": "Flooding",
			"city": "Thanjavur",
			"read": "No",
			"count": 1
		}
		this.GS.postAlert(waterPostObj).subscribe(dataPost => {

		});
		$('#tabUL a[href="#tab4"]').trigger('click');
		$('#waterlevelPopUpWater').modal('hide');
		this.counterThanjavur = 1;
		this.myVar = true;
		this.area = 'Kaveri River';
		this.area1 = 'Near Anjaneya Swamy Temple';
		this.distance = 'Disaster Vehicle is near to Rameez Fertilizer with a distance of 190m';
		this.hospital = 'Anjaneya Govt Hospital';

		this.highlightThanjavur = true;
		this.highlightFlood = true;
		this.thanjavurVal = 1;
		this.zoom = 16;
		this.lat = 10.8321013;
		this.lng = 78.8176763;
		this.zoomControl = true;
		this.markers = [];
		this.markers = [
			{
				lat: 10.833875015038698,
				lng: 78.8170337677002,
				// label: 'R', 10.8321013!4d78.8176763
				draggable: true,
				iconUrl: './assets/images/flood.gif',
			},
			{
				lat: 10.836432349962717,
				lng: 78.81140146404505,
				// label: 'R', 11.724581, 78.067750
				draggable: true,
				iconUrl: '../assets/images/flood3.png',
			}
			
		]
	}
	
	maduraiFireIncident(x) {
		
		this.fireVar11 = true;
		this.fireVarMadurai = true;
		this.fireMadurai = 'Fires Break out At Madurai Corporation Childrens Park';
		this.counterMadurai = 1;
		this.maduraiVal = 1;
		this.highlightMadurai = true;
		this.myVar = true;
		this.area = 'Aruppukottai Rd';
		this.area1 = 'Aruppukottai Rd';
		this.distance = 'Fire Engine is in 1000 from South Indian Bank';
		this.hospital = 'Aruppukottai Rural';
		this.zoom = 16;
		this.lat = 9.9059459;
		this.lng = 78.1191668;
		this.zoomControl = true;
		this.markers = [];
		this.markers = [
			{
				lat: 9.905295377423082,
				lng: 78.12081813812256,
				// label: 'R', 11.702058, 78.102855
				draggable: true,
				iconUrl: './assets/images/fires.gif',
			},
			{
				lat: 9.905908373394674,
				lng: 78.11457395553589,
				// label: 'R', 11.692560, 78.112639
				draggable: true,
				iconUrl: '../assets/images/fireL.png',
			},
			{
				lat: 9.910368413046792,
				lng: 78.11980962753296,
				// label: 'R', 11.724581, 78.067750
				draggable: true,
				iconUrl: '../assets/images/pd.png',
			}
			
		]
	}
	salemFireIncident(x) {
		this.fireVar11 = true;
		this.fireVarSalem = true;
		this.fireSalem = 'Fires Break out At St. Marys Hospital';
		this.counterSalem = 1;
		this.salemVal = 1;
		this.highlightSalem = true;
		this.myVar = true;
		this.area = 'Arisipalayam Main Road';
		this.area1 = 'Arisipalayam Main Road';
		this.distance = 'Fire Engine is in 1000 from South Indian Bank';
		this.hospital = 'Arisipalayam Rural';
		this.zoom = 16;
		this.lat = 11.664325;
		this.lng = 78.1460142;
		this.zoomControl = true;
		this.markers = [];
		this.markers = [
			{
				lat: 11.664109886411596,
				lng: 78.14588069915771,
				// label: 'R', 11.702058, 78.102855
				draggable: true,
				iconUrl: './assets/images/fires.gif',
			},
			{
				lat: 11.660495357944953,
				lng: 78.15000057220459,
				// label: 'R', 11.692560, 78.112639
				draggable: true,
				iconUrl: '../assets/images/cc1.png',
			},
			{
				lat: 11.66932144923812,
				lng: 78.14049482345581,
				// label: 'R', 11.724581, 78.067750
				draggable: true,
				iconUrl: '../assets/images/policeL.png',
			}

		]

	}
	smoke(x) {
		this.fireVar11 = true;
		this.fire2 = 'Fires Break out in Kalveerampalayam Near Sowdambika gardens Building no 9';
		this.counterSmoke = 1;
		this.smokeVal = 1;
		this.highlightSmoke = true;
		this.myVar = true;
		this.area = 'Kalveerampalayam';
		this.area1 = 'Kalveerampalayam Near Sowdambika gardens Building no 9';
		this.distance = '150m from State Bank of India ATM';
		this.hospital = 'Kalveeram Palayam Rural'
		this.zoom = 16;
		this.lat = 11.036027;
		this.lng = 76.883247;
		this.zoomControl = true;
		this.markers = [];
		this.markers = [
			{
				lat: 11.033237,
				lng: 76.885838,
				// label: 'R',
				draggable: true,
				iconUrl: './assets/images/fires.gif',
			},
			{
				lat: 11.036627,
				lng: 76.881151,
				// label: 'R', 11.036627, 76.881151
				draggable: true,
				iconUrl: '../assets/images/ambulanceL.png',
			},
			{
				lat: 11.037516,
				lng: 76.878683,
				// label: 'R', 11.037516, 76.878683
				draggable: true,
				iconUrl: '../assets/images/policeL.png',
			}
		]
	}
	abcd(x) {
		//   alert('helooo');
		this.counter = 1;
		this.afzal = 1;
		this.highlight = true;
		this.myVar = true;

		this.area = 'Ramnathpuram';
		this.area1 = 'Ramnathpuram Near BSNL Exchange Office'
		this.distance = '150m from Kongunad Hospital';
		this.hospital = 'Kongunad'
		this.zoom = 16;
		this.lat = 11.0168;
		this.lng = 76.9558;

		this.zoomControl = true;
		this.markers = [
			{
				lat: 11.016673,
				lng: 76.9580895,
				// label: 'R',
				draggable: true,
				iconUrl: './assets/images/fires.gif',
			},
			{
				lat: 11.0181,
				lng: 76.9606,
				// label: 'R',
				draggable: true,
				iconUrl: '../assets/images/ambulanceR.png',
			},
			{
				lat: 11.0174284,
				lng: 76.9618081,
				// label: 'R',
				draggable: true,
				iconUrl: '../assets/images/ambulanceR.png',
			},
			{
				lat: 11.016605,
				lng: 76.954505,
				// label: 'R',
				draggable: true,
				iconUrl: '../assets/images/fireL.png',
			},
			{
				lat: 11.3530,
				lng: 76.7959,
				// label: 'R',
				draggable: true,
				iconUrl: '../assets/images/hospital.png',
			}
		]
		// this.getNews();

	}

	pressureChanged(x) {
		this.name = x;
		// this.GS.getIncidentRoad(this.name).subscribe(data => {
		// 	this.roadArray = data;
		// 	// alert(JSON.stringify(this.roadArray));
		// 	this.road1 = this.roadArray[0].news;
		// 	this.road2 = this.roadArray[1].news;
		// 	this.road3 = this.roadArray[2].news;
		// });
		// this.GS.getIncidentAccident(this.name).subscribe(data => {
		// 	this.accidentArray = data;
		// 	this.accident1 = this.accidentArray[0].news;
		// 	this.accident2 = this.accidentArray[1].news;
		// 	this.accident3 = this.accidentArray[2].news;
		// });
		// this.GS.getIncidentFire(this.name).subscribe(data => {
		// 	this.fireArray = data;
		this.fire1 = 'Moving Car Gutted In Fire In Coimbatore, No Casualties';
		// 	//   this.fire2 = this.fireArray[1].news;
		// 	// this.fire3 = this.fireArray[2].news;
		// });
		// this.GS.getIncidentNatural(this.name).subscribe(data => {
		// 	this.naturalArray = data;
		// 	this.natural1 = this.naturalArray[0].news;
		// 	this.natural2 = this.naturalArray[1].news;
		// 	this.natural3 = this.naturalArray[2].news;
		// });
		// this.GS.getIncidentMass(this.name).subscribe(data => {
		// 	this.massArray = data;
		// 	this.mass1 = this.massArray[0].news;
		// 	this.mass2 = this.massArray[1].news;
		// 	this.mass3 = this.massArray[2].news;
		// });
		// this.GS.getIncidentParking(this.name).subscribe(data => {
		// 	this.parkingArray = data;
		// 	this.parking1 = this.parkingArray[0].news;
		// 	this.parking2 = this.parkingArray[1].news;
		// 	this.parking3 = this.parkingArray[2].news;
		// });
	}
	bridge1(x) {
		console.log('hellooooo ', x);
	}
	waterPolice() {
		// debugger;
		// debugger;
		if (this.bombVariable == true) {
			this.bombVar = true;
			// this.bombIncident(this.bombObj);
		} 
		// else {
		// 	$('#waterlevelPopUp1').modal('show');
		// }

		// this.bombIncident(this.bombObj);
	}
	waterFire() {
		$('#waterlevelPopUp1').modal('show');
	}
	waterHospital() {
		$('#waterlevelPopUp1').modal('show');
	}
	crood: marker[] = [
		{
			"lat": 11.017958,
			"lng": 76.960493,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceR.png"
		},
		{
			"lat": 11.017854,
			"lng": 76.960278,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceR.png"
		},
		{
			"lat": 11.017664,
			"lng": 76.960235,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceR.png"
		},
		{
			"lat": 11.017527,
			"lng": 76.960214,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceR.png"
		},
		{
			"lat": 11.017411,
			"lng": 76.960203,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceR.png"
		},
		{
			"lat": 11.017285,
			"lng": 76.960214,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceR.png"
		},
		{
			"lat": 11.017127,
			"lng": 76.960224,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceR.png"
		},
		{
			"lat": 11.016969,
			"lng": 76.960203,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceR.png"
		},
		{
			"lat": 11.016916,
			"lng": 76.960074,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceR.png"
		},
		{
			"lat": 11.016895,
			"lng": 76.959892,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceR.png"
		},
		{
			"lat": 11.016864,
			"lng": 76.959688,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceR.png"
		},
		{
			"lat": 11.016853,
			"lng": 76.959505,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceR.png"
		},
		{
			"lat": 11.016843,
			"lng": 76.959334,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceR.png"
		},
		{
			"lat": 11.016843,
			"lng": 76.959184,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceR.png"
		},
		{
			"lat": 11.016811,
			"lng": 76.958990,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceR.png"
		},
		{
			"lat": 11.0170314,
			"lng": 76.9580973,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceR.png"
		}
	]
	crood1: marker[] = [
		{
			"lat": 11.016605,
			"lng": 76.954505,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/fireL.png"
		},
		{
			"lat": 11.016613,
			"lng": 76.954580,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/fireL.png"
		},
		{
			"lat": 11.016624,
			"lng": 76.954664,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/fireL.png"
		},

		{
			"lat": 11.016634,
			"lng": 76.954787,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/fireL.png"
		},
		{
			"lat": 11.016650,
			"lng": 76.954889,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/fireL.png"
		},

		{
			"lat": 11.016660,
			"lng": 76.955045,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/fireL.png"
		},
		{
			"lat": 11.016663,
			"lng": 76.955168,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/fireL.png"
		},
		{
			"lat": 11.016679,
			"lng": 76.955340,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/fireL.png"
		},
		{
			"lat": 11.016695,
			"lng": 76.955490,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/fireL.png"
		},
		{
			"lat": 11.016697,
			"lng": 76.955627,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/fireL.png"
		},
		{
			"lat": 11.016695,
			"lng": 76.955777,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/fireL.png"
		},

		{
			"lat": 11.016700,
			"lng": 76.955876,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/fireL.png"
		},
		{
			"lat": 11.016703,
			"lng": 76.955991,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/fireL.png"
		},
		{
			"lat": 11.016708,
			"lng": 76.956158,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/fireL.png"
		},
		{
			"lat": 11.016710,
			"lng": 76.956396,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/fireL.png"
		},
		{
			"lat": 11.016732, //11.016732, 76.956927
			"lng": 76.956927,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/fireL.png"
		},
	]
	crood2: marker[] = [
		{
			"lat": 11.230256,
			"lng": 77.117701,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceR.png"
		},
		{
			"lat": 11.230593,
			"lng": 77.117100,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceR.png"
		},
		{
			"lat": 11.230761,
			"lng": 77.116671,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceR.png"
		},
		{
			"lat": 11.230930,
			"lng": 77.116242,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceR.png"
		},
		{
			"lat": 11.231014,
			"lng": 77.115727,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceR.png"
		},
		{
			"lat": 11.231645,
			"lng": 77.114096,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceR.png"
		},
		{
			"lat": 11.231603,
			"lng": 77.113538,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceR.png"
		},
		{
			"lat": 11.231814,
			"lng": 77.112852,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceR.png"
		},
		{
			"lat": 11.232024,
			"lng": 77.112251,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceR.png"
		},
		{
			"lat": 11.232108,
			"lng": 77.111607,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceR.png"
		},
		{
			"lat": 11.232361,
			"lng": 77.110877,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceR.png"
		},
		{
			"lat": 11.232361,
			"lng": 77.110320,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceR.png"
		},
		{
			"lat": 11.232403,
			"lng": 77.109805,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceR.png"
		},
		{
			"lat": 11.232908,
			"lng": 77.107788,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceR.png"
		},
		{
			"lat": 11.232908,
			"lng": 77.107788,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceR.png"
		},
		{
			"lat": 11.232908,
			"lng": 77.107788,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceR.png"
		},
	]
	crood3: marker[] = [
		{
			"lat": 11.232704,
			"lng": 77.097124,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.232756,
			"lng": 77.097242,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.232788,
			"lng": 77.097349,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.232788,
			"lng": 77.097467,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.232830,
			"lng": 77.097596,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.232862,
			"lng": 77.097703,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.232883,
			"lng": 77.097832,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.232904,
			"lng": 77.097950,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.232914,
			"lng": 77.098111,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.232935,
			"lng": 77.098261,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.232946,
			"lng": 77.098379,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.232946,
			"lng": 77.098508,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.232977,
			"lng": 77.098636,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.233030,
			"lng": 77.098787,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.233030,
			"lng": 77.098926,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.233062,
			"lng": 77.099130,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
	]
	croodSmoke: marker[] = [
		{
			"lat": 11.036623,
			"lng": 76.881168,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.036607,
			"lng": 76.881219,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.036612,
			"lng": 76.881238,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.036602,
			"lng": 76.881273,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.036589,
			"lng": 76.881297,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.036594,
			"lng": 76.881324,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.036578,
			"lng": 76.881356,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.036562,
			"lng": 76.881388,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.036557,
			"lng": 76.881415,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.036544,
			"lng": 76.881455,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.036541,
			"lng": 76.881512,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.036536,
			"lng": 76.881549,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.036533,
			"lng": 76.881584,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.036528,
			"lng": 76.88163,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.036475,
			"lng": 76.881649,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		}
		,
		{
			"lat": 11.036473,
			"lng": 76.881683,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		}
		,
		{
			"lat": 11.036458,
			"lng": 76.881709,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.036437,
			"lng": 76.881755,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.036421,
			"lng": 76.881814,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.036421,
			"lng": 76.881849,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.036413,
			"lng": 76.881889,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.036381,
			"lng": 76.881927,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.036376,
			"lng": 76.881975,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.036339,
			"lng": 76.882023,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.036331,
			"lng": 76.882063,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.036302,
			"lng": 76.88216,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.036268,
			"lng": 76.882224,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.036239,
			"lng": 76.882286,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.036223,
			"lng": 76.882321,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.036189,
			"lng": 76.882385,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.036179,
			"lng": 76.882439,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.036152,
			"lng": 76.882508,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.036112,
			"lng": 76.882548,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.036089,
			"lng": 76.882621,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.036078,
			"lng": 76.882666,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.036044,
			"lng": 76.882693,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.036015,
			"lng": 76.882759,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.036014,
			"lng": 76.882794,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.035995,
			"lng": 76.882824,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.035966,
			"lng": 76.882851,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.035950,
			"lng": 76.882885,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.035937,
			"lng": 76.882915,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.035906,
			"lng": 76.88295,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.035874,
			"lng": 76.88299,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.035848,
			"lng": 76.883011,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.035853,
			"lng": 76.88307,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.035840,
			"lng": 76.883116,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.035832,
			"lng": 76.883191,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.035782,
			"lng": 76.883264,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.035766,
			"lng": 76.883315,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.035724,
			"lng": 76.8834,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.035711,
			"lng": 76.883457,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.035687,
			"lng": 76.883521,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.035635,
			"lng": 76.883561,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.035616,
			"lng": 76.883628,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.035558,
			"lng": 76.88369,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.035532,
			"lng": 76.883744,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.035521,
			"lng": 76.883805,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.035508,
			"lng": 76.883848,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.035492,
			"lng": 76.883915,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.035453,
			"lng": 76.883974,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.035421,
			"lng": 76.884052,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.035363,
			"lng": 76.8841,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.035324,
			"lng": 76.884181,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.035263,
			"lng": 76.884277,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.035192,
			"lng": 76.884409,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.035017,
			"lng": 76.884522,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.034880,
			"lng": 76.884608,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.034743,
			"lng": 76.884683,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.034648,
			"lng": 76.884834,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.034501,
			"lng": 76.884995,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.034395,
			"lng": 76.885145,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.034259,
			"lng": 76.885316,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.034048,
			"lng": 76.885456,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.034048,
			"lng": 76.885456,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.034048,
			"lng": 76.885456,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.034048,
			"lng": 76.885456,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.034048,
			"lng": 76.885456,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.034048,
			"lng": 76.885456,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.034048,
			"lng": 76.885456,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.034048,
			"lng": 76.885456,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.034048,
			"lng": 76.885456,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.034048,
			"lng": 76.885456,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		},
		{
			"lat": 11.034048,
			"lng": 76.885456,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/ambulanceL.png"
		}
	]
	croodSmoke1: marker[] = [
		{
			lat: 11.037516,
			lng: 76.878683,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			lat: 11.037440,
			lng: 76.878844,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			lat: 11.037377,
			lng: 76.879101,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			lat: 11.037188,
			lng: 76.879337,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			lat: 11.037188,
			lng: 76.879337,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			lat: 11.037188,
			lng: 76.879337,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.036623,
			"lng": 76.881168,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.036623,
			"lng": 76.881168,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.036623,
			"lng": 76.881168,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},

		{
			"lat": 11.036623,
			"lng": 76.881168,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.036607,
			"lng": 76.881219,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.036612,
			"lng": 76.881238,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.036602,
			"lng": 76.881273,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.036589,
			"lng": 76.881297,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.036594,
			"lng": 76.881324,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.036578,
			"lng": 76.881356,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.036562,
			"lng": 76.881388,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.036557,
			"lng": 76.881415,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.036544,
			"lng": 76.881455,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.036541,
			"lng": 76.881512,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.036536,
			"lng": 76.881549,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.036533,
			"lng": 76.881584,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.036528,
			"lng": 76.88163,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.036475,
			"lng": 76.881649,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		}
		,
		{
			"lat": 11.036473,
			"lng": 76.881683,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		}
		,
		{
			"lat": 11.036458,
			"lng": 76.881709,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.036437,
			"lng": 76.881755,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.036421,
			"lng": 76.881814,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.036421,
			"lng": 76.881849,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.036413,
			"lng": 76.881889,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.036381,
			"lng": 76.881927,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.036376,
			"lng": 76.881975,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.036339,
			"lng": 76.882023,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.036331,
			"lng": 76.882063,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.036302,
			"lng": 76.88216,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.036268,
			"lng": 76.882224,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.036239,
			"lng": 76.882286,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.036223,
			"lng": 76.882321,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.036189,
			"lng": 76.882385,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.036179,
			"lng": 76.882439,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.036152,
			"lng": 76.882508,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.036112,
			"lng": 76.882548,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.036089,
			"lng": 76.882621,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.036078,
			"lng": 76.882666,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.036044,
			"lng": 76.882693,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.036015,
			"lng": 76.882759,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.036014,
			"lng": 76.882794,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.035995,
			"lng": 76.882824,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.035966,
			"lng": 76.882851,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.035950,
			"lng": 76.882885,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.035937,
			"lng": 76.882915,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.035906,
			"lng": 76.88295,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.035874,
			"lng": 76.88299,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.035848,
			"lng": 76.883011,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.035853,
			"lng": 76.88307,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.035840,
			"lng": 76.883116,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.035832,
			"lng": 76.883191,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.035782,
			"lng": 76.883264,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.035766,
			"lng": 76.883315,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.035724,
			"lng": 76.8834,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.035711,
			"lng": 76.883457,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.035687,
			"lng": 76.883521,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.035635,
			"lng": 76.883561,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.035616,
			"lng": 76.883628,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.035558,
			"lng": 76.88369,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.035532,
			"lng": 76.883744,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.035521,
			"lng": 76.883805,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.035508,
			"lng": 76.883848,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.035492,
			"lng": 76.883915,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.035453,
			"lng": 76.883974,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.035421,
			"lng": 76.884052,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.035363,
			"lng": 76.8841,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.035324,
			"lng": 76.884181,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.035263,
			"lng": 76.884277,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.035192,
			"lng": 76.884409,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.035017,
			"lng": 76.884522,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.034880,
			"lng": 76.884608,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.034743,
			"lng": 76.884683,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.034648,
			"lng": 76.884834,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.034648,
			"lng": 76.884834,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.034648,
			"lng": 76.884834,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.034648,
			"lng": 76.884834,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.034648,
			"lng": 76.884834,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		},
		{
			"lat": 11.034648,
			"lng": 76.884834,
			"label": "",
			"draggable": true,
			"iconUrl": "../assets/images/policeL.png"
		}
	]
	croodWater: marker[] = [
		{
			'lat': 11.267305870377978,
			'lng': 76.86259388923645,
			// label:'R', 1.245823, 76.855609 draggable:false,
			"label": "",
			"draggable": true,
			'iconUrl': './assets/images/flood1.png',
		},
		{
			'lat': 11.265959044594519,
			'lng': 76.8601906299591,
			// label:'R',
			"label": "",
			"draggable": true,
			'iconUrl': './assets/images/flood1.png',
		},
		{
			'lat': 11.264275503496597,
			'lng': 76.85744404792786,
			// label:'R',
			"label": "",
			"draggable": true,
			'iconUrl': './assets/images/flood1.png',
		},
		{
			'lat': 11.262591952545991,
			'lng': 76.8546974658966,
			// label:'R',
			"label": "",
			"draggable": true,
			'iconUrl': './assets/images/flood1.png',
		},
		{
			'lat': 11.261245104692524,
			'lng': 76.85263752937317,
			// label:'R',
			"label": "",
			"draggable": true,
			'iconUrl': './assets/images/flood1.png',
		},
		{
			'lat': 11.259224821092337,
			'lng': 76.8488609790802,
			// label:'R',
			"label": "",
			"draggable": true,
			'iconUrl': './assets/images/flood1.png',
		},
		{
			'lat': 11.257877957479945,
			'lng': 76.84645771980286,
			// label:'R',
			"label": "",
			"draggable": true,
			'iconUrl': './assets/images/flood1.png',
		},
		{
			'lat': 11.255184211348904,
			'lng': 76.8433678150177,
			// label:'R',
			"label": "",
			"draggable": true,
			'iconUrl': './assets/images/flood1.png',
		},
		{
			'lat': 11.254174050051667,
			'lng': 76.83959126472473,
			// label:'R',
			"label": "",
			"draggable": true,
			'iconUrl': './assets/images/flood1.png',
		},
		{
			'lat': 11.251143544897465,
			'lng': 76.83650135993958,
			// label:'R',
			"label": "",
			"draggable": true,
			'iconUrl': './assets/images/flood1.png',
		},
		{
			'lat': 11.249123190412284,
			'lng': 76.83375477790833,
			// label:'R',
			"label": "",
			"draggable": true,
			'iconUrl': './assets/images/flood1.png',
		},
		{
			'lat': 11.247776279549916,
			'lng': 76.82997822761536,
			// label:'R',
			"label": "",
			"draggable": true,
			'iconUrl': './assets/images/flood1.png',
		},
		{
			'lat': 11.244745707087416,
			'lng': 76.8268883228302,
			// label:'R',
			"label": "",
			"draggable": true,
			'iconUrl': './assets/images/flood1.png',
		},
		{
			'lat': 11.243398775762554,
			'lng': 76.82242512702942,
			// label:'R',
			"label": "",
			"draggable": true,
			'iconUrl': './assets/images/flood1.png',
		},
		{
			'lat': 11.242051838143057,
			'lng': 76.81864857673645,
			// label:'R',
			"label": "",
			"draggable": true,
			'iconUrl': './assets/images/flood1.png',
		},
		{
			'lat': 11.240031419912917,
			'lng': 76.81487202644348,
			// label:'R',
			"label": "",
			"draggable": true,
			'iconUrl': './assets/images/flood1.png',
		},
		{
			'lat': 11.238010987523891,
			'lng': 76.8107521533966,
			// label:'R',
			"label": "",
			"draggable": true,
			'iconUrl': './assets/images/flood1.png',
		},
		{
			'lat': 11.235990540978367,
			'lng': 76.80766224861145,
			// label:'R',
			"label": "",
			"draggable": true,
			'iconUrl': './assets/images/flood1.png',
		},
		{
			'lat': 11.233970080278764,
			'lng': 76.8049156665802,
			// label:'R',
			"label": "",
			"draggable": true,
			'iconUrl': './assets/images/flood1.png',
		},
		{
			'lat': 11.2319496054275,
			'lng': 76.80113911628723,
			// label:'R',
			"label": "",
			"draggable": true,
			'iconUrl': './assets/images/flood1.png',
		}
	]
	croodTraffic: marker[] = [
		{
			lat: 10.99319792363201,
			lng: 76.9586706161499,
			// label: 'R',
			draggable: true,
			iconUrl: './assets/images/pr.png',
		},
		{
			lat: 10.99321898755142,
			lng: 76.95884764194489,
			// label: 'R',
			draggable: true,
			iconUrl: './assets/images/pr.png',
		},
		{
			lat: 10.99326210275679,
			lng: 76.95895241573453,
			// label: 'R',
			draggable: true,
			iconUrl: './assets/images/pr.png',
		},
		{
			lat: 10.99326210275679,
			lng: 76.95901142433286,
			// label: 'R',
			draggable: true,
			iconUrl: './assets/images/pr.png',
		},
		{
			lat: 10.993251570798813,
			lng: 76.95905970409513,
			// label: 'R',
			draggable: true,
			iconUrl: './assets/images/pr.png',
		},
		{
			lat: 10.99324630481968,
			lng: 76.95912944152951,
			// label: 'R',
			draggable: true,
			iconUrl: './assets/images/pr.png',
		},
		{
			lat: 10.99324630481968,
			lng: 76.95918308570981,
			// label: 'R',
			draggable: true,
			iconUrl: './assets/images/pr.png',
		},
		{
			lat: 10.993251570798813,
			lng: 76.95924745872617,
			// label: 'R',
			draggable: true,
			iconUrl: './assets/images/pr.png',
		},
		{
			lat: 10.993267368735642,
			lng: 76.95931719616055,
			// label: 'R',
			draggable: true,
			iconUrl: './assets/images/pr.png',
		},
		{
			lat: 10.99328316667162,
			lng: 76.95937084034085,
			// label: 'R',
			draggable: true,
			iconUrl: './assets/images/pr.png',
		},
		{
			lat: 10.993288432650093,
			lng: 76.95942984893918,
			// label: 'R',
			draggable: true,
			iconUrl: './assets/images/pr.png',
		},
		{
			lat: 10.993288432650093,
			lng: 76.9594513066113,
			// label: 'R',
			draggable: true,
			iconUrl: './assets/images/pr.png',
		},
		{
			lat: 10.993298964606769,
			lng: 76.9595049507916,
			// label: 'R',
			draggable: true,
			iconUrl: './assets/images/pr.png',
		},
		{
			lat: 10.993298964606769,
			lng: 76.95952104404569,
			// label: 'R',
			draggable: true,
			iconUrl: './assets/images/pr.png',
		},
		{
			lat: 10.993298964606769,
			lng: 76.95954786613584,
			// label: 'R',
			draggable: true,
			iconUrl: './assets/images/pr.png',
		},
		{
			lat: 10.993298964606769,
			lng: 76.95960151031613,
			// label: 'R',
			draggable: true,
			iconUrl: './assets/images/pr.png',
		}
	]
	croodTraffic1: marker[] = [
		{
			lat: 10.994462249817825,
			lng: 76.95951031520963,
			// label: 'R',
			draggable: true,
			iconUrl: './assets/images/pd.png',
		},
		{
			lat: 10.994425388113248,
			lng: 76.95951031520963,
			// label: 'R',
			draggable: true,
			iconUrl: './assets/images/pd.png',
		},
		{
			lat: 10.994356930649678,
			lng: 76.95955323055387,
			// label: 'R',
			draggable: true,
			iconUrl: './assets/images/pd.png',
		},
		{
			lat: 10.994262143366159,
			lng: 76.95955323055387,
			// label: 'R',
			draggable: true,
			iconUrl: './assets/images/pd.png',
		},
		{
			lat: 10.994198951826915,
			lng: 76.95955323055387,
			// label: 'R',
			draggable: true,
			iconUrl: './assets/images/pd.png',
		},
		{
			lat: 10.994077340987985,
			lng: 76.95955574512482,
			// label: 'R',
			draggable: true,
			iconUrl: './assets/images/pd.png',
		},
		{
			lat: 10.994077340987985,
			lng: 76.95955574512482,
			// label: 'R',
			draggable: true,
			iconUrl: './assets/images/pd.png',
		},
		{
			lat: 10.9940093771279,
			lng: 76.95962296798825,
			// label: 'R',
			draggable: true,
			iconUrl: './assets/images/pd.png',
		},
		{
			lat: 10.993956717467652,
			lng: 76.95963906124234,
			// label: 'R',
			draggable: true,
			iconUrl: './assets/images/pd.png',
		},
		{
			lat: 10.993904057798002,
			lng: 76.95963906124234,
			// label: 'R',
			draggable: true,
			iconUrl: './assets/images/pd.png',
		},
		{
			lat: 10.993751344702869,
			lng: 76.9596497900784,
			// label: 'R',
			draggable: true,
			iconUrl: './assets/images/pd.png',
		},
		{
			lat: 10.993709216938562,
			lng: 76.95965515449643,
			// label: 'R',
			draggable: true,
			iconUrl: './assets/images/pd.png',
		},
		{
			lat: 10.993656557224728,
			lng: 76.95967124775052,
			// label: 'R',
			draggable: true,
			iconUrl: './assets/images/pd.png',
		},
		{
			lat: 10.993624961391907,
			lng: 76.95967124775052,
			// label: 'R',
			draggable: true,
			iconUrl: './assets/images/pd.png',
		},
		{
			lat: 10.993567035689626,
			lng: 76.95967661216855,
			// label: 'R',
			draggable: true,
			iconUrl: './assets/images/pd.png',
		},
		{
			lat: 10.993524907899008,
			lng: 76.95971416309476,
			// label: 'R',
			draggable: true,
			iconUrl: './assets/images/pd.png',
		}
	]
	croodBomb: marker[] = [
		{
			lat: 10.972260302578505,
			lng: 76.8804158270359,
			// label: 'R', 1
			draggable: true,
			iconUrl: './assets/images/bomb.png',
		},
		{
			lat: 10.972260302578505,
			lng: 76.88114538788795,
			// label: 'R', 1
			draggable: true,
			iconUrl: './assets/images/bomb.png',
		},
		{
			lat: 10.971923255761736,
			lng: 76.88187494874,
			// label: 'R', 1
			draggable: true,
			iconUrl: './assets/images/bomb.png',
		},
		{
			lat: 10.971838993997478,
			lng: 76.88217535614967,
			// label: 'R', 1
			draggable: true,
			iconUrl: './assets/images/bomb.png',
		},
		{
			lat: 10.971796863106341,
			lng: 76.88256159424782,
			// label: 'R', 1
			draggable: true,
			iconUrl: './assets/images/bomb.png',
		},
		{
			lat: 10.971796863106341,
			lng: 76.88290491700172,
			// label: 'R', 1
			draggable: true,
			iconUrl: './assets/images/bomb.png',
		},
		{
			lat: 10.97167047039688,
			lng: 76.88329115509987,
			// label: 'R', 1
			draggable: true,
			iconUrl: './assets/images/bomb.png',
		},
		{
			lat: 10.971544077633386,
			lng: 76.88363447785378,
			// label: 'R', 1
			draggable: true,
			iconUrl: './assets/images/bomb.png',
		},
		{
			lat: 10.971459815761015,
			lng: 76.88406363129616,
			// label: 'R', 1
			draggable: true,
			iconUrl: './assets/images/bomb.png',
		},
		{
			lat: 10.971333422907424,
			lng: 76.88436403870583,
			// label: 'R', 1
			draggable: true,
			iconUrl: './assets/images/bomb.png',
		},
		{
			lat: 10.971207029999785,
			lng: 76.8846644461155,
			// label: 'R', 1
			draggable: true,
			iconUrl: './assets/images/bomb.png',
		},
		{
			lat: 10.97116489901854,
			lng: 76.88505068421364,
			// label: 'R', 1
			draggable: true,
			iconUrl: './assets/images/bomb.png',
		},
		{
			lat: 10.971038506038845,
			lng: 76.88530817627907,
			// label: 'R', 1
			draggable: true,
			iconUrl: './assets/images/bomb.png',
		},
		{
			lat: 10.971038506038845,
			lng: 76.88586607575417,
			// label: 'R', 1
			draggable: true,
			iconUrl: './assets/images/bomb.png',
		},
		{
			lat: 10.971038506038845,
			lng: 76.88655272126198,
			// label: 'R', 1
			draggable: true,
			iconUrl: './assets/images/bomb.png',
		},
		{
			lat: 10.971122768031325,
			lng: 76.88693895936012,
			// label: 'R', 1
			draggable: true,
			iconUrl: './assets/images/bomb.png',
		},
		{
			lat: 10.97116489901854,
			lng: 76.88741102814674,
			// label: 'R', 1
			draggable: true,
			iconUrl: './assets/images/bomb.png',
		},
		{
			lat: 10.970996375033591,
			lng: 76.8884839117527,
			// label: 'R', 1
			draggable: true,
			iconUrl: './assets/images/bomb.png',
		},
		{
			lat: 10.97086998198181,
			lng: 76.8895997107029,
			// label: 'R',
			draggable: true,
			iconUrl: './assets/images/bomb.png',
		}
	]
	croodBomb1: marker[] = [
		{
			lat: 10.967964901785514,
			lng: 76.90110102295876,
			// label: 'R', 1
			draggable: true,
			iconUrl: './assets/images/pl.png',
		},
		{
			lat: 10.967964901785514,
			lng: 76.90075770020485,
			// label: 'R', 1
			draggable: true,
			iconUrl: './assets/images/pl.png',
		},
		{
			lat: 10.968091296079464,
			lng: 76.90045729279518,
			// label: 'R', 1
			draggable: true,
			iconUrl: './assets/images/pl.png',
		},
		{
			lat: 10.968259821720656,
			lng: 76.90007105469704,
			// label: 'R', 1
			draggable: true,
			iconUrl: './assets/images/pl.png',
		},
		{
			lat: 10.968386215888504,
			lng: 76.8998135626316,
			// label: 'R', 1
			draggable: true,
			iconUrl: './assets/images/pl.png',
		},
		{
			lat: 10.968470478637053,
			lng: 76.89968481659889,
			// label: 'R', 1
			draggable: true,
			iconUrl: './assets/images/pl.png',
		},
		{
			lat: 10.96851261000231,
			lng: 76.89916983246803,
			// label: 'R', 1
			draggable: true,
			iconUrl: './assets/images/pl.png',
		},
		{
			lat: 10.968681135403342,
			lng: 76.89886942505836,
			// label: 'R', 1
			draggable: true,
			iconUrl: './assets/images/pl.png',
		},
		{
			lat: 10.969102448485566,
			lng: 76.89844027161598,
			// label: 'R', 1
			draggable: true,
			iconUrl: './assets/images/pl.png',
		},
		{
			lat: 10.969228842293159,
			lng: 76.89813986420631,
			// label: 'R', 1
			draggable: true,
			iconUrl: './assets/images/pl.png',
		},
		{
			lat: 10.969397367285861,
			lng: 76.89779654145241,
			// label: 'R', 1
			draggable: true,
			iconUrl: './assets/images/pl.png',
		},
		{
			lat: 10.969650154594786,
			lng: 76.89732447266579,
			// label: 'R', 1
			draggable: true,
			iconUrl: './assets/images/pl.png',
		},
		{
			lat: 10.969902941687534,
			lng: 76.89685240387917,
			// label: 'R', 1
			draggable: true,
			iconUrl: './assets/images/pl.png',
		},
		{
			lat: 10.970239990808247,
			lng: 76.89646616578102,
			// label: 'R', 1
			draggable: true,
			iconUrl: './assets/images/pl.png',
		},
		{
			lat: 10.970450646313509,
			lng: 76.89625158905983,
			// label: 'R', 1
			draggable: true,
			iconUrl: './assets/images/pl.png',
		},
		{
			lat: 10.970703432721676,
			lng: 76.89539328217506,
			// label: 'R', 1
			draggable: true,
			iconUrl: './assets/images/pl.png',
		},
		{
			lat: 10.970703432721676,
			lng: 76.89474955201149,
			// label: 'R', 1
			draggable: true,
			iconUrl: './assets/images/pl.png',
		},
		{
			lat: 10.970871956873667,
			lng: 76.89406290650368,
			// label: 'R', 1
			draggable: true,
			iconUrl: './assets/images/pl.png',
		},
		{
			lat: 10.97095621891363,
			lng: 76.89371958374977,
			// label: 'R', 1
			draggable: true,
			iconUrl: './assets/images/pl.png',
		}
	]
	croodFiber: marker[] = [
		{
			lat: 11.122528250492643,
			lng: 76.93541049957275,
			// label: 'R', 1
			draggable: true,
			iconUrl: './assets/images/pd.png',
		},
		{
			lat: 11.122233485519313,
			lng: 76.9352388381958,
			// label: 'R', 1
			draggable: true,
			iconUrl: './assets/images/pd.png',
		},
		{
			lat: 11.121854501544147,
			lng: 76.93528175354004,
			// label: 'R', 1
			draggable: true,
			iconUrl: './assets/images/pd.png',
		},
		{
			lat: 11.121391298238551,
			lng: 76.93541049957275,
			// label: 'R', 1
			draggable: true,
			iconUrl: './assets/images/pd.png',
		},
		{
			lat: 11.120843875201063,
			lng: 76.93541049957275,
			// label: 'R', 1
			draggable: true,
			iconUrl: './assets/images/pd.png',
		},
		{
			lat: 11.120380670289403,
			lng: 76.93549633026123,
			// label: 'R', 1
			draggable: true,
			iconUrl: './assets/images/pd.png',
		},
		{
			lat: 11.120001683905079,
			lng: 76.93553924560547,
			// label: 'R', 1
			draggable: true,
			iconUrl: './assets/images/pd.png',
		},
		{
			lat: 11.119538477655105,
			lng: 76.93562507629395,
			// label: 'R', 1
			draggable: true,
			iconUrl: './assets/images/pd.png',
		},
		{
			lat: 11.119117380425525,
			lng: 76.93566799163818,
			// label: 'R', 1
			draggable: true,
			iconUrl: './assets/images/pd.png',
		},
		{
			lat: 11.11840151373928,
			lng: 76.93566799163818,
			// label: 'R', 1
			draggable: true,
			iconUrl: './assets/images/pd.png',
		},
		{
			lat: 11.117685645295275,
			lng: 76.93571090698242,
			// label: 'R', 1
			draggable: true,
			iconUrl: './assets/images/pd.png',
		}
	]
	croodFiber1: marker[] = [
		{
			lat: 11.110486727157733,
			lng: 76.93663492798805,
			// label: 'R', 1
			draggable: true,
			iconUrl: './assets/images/road.png',
		},
		{
			lat: 11.110739393045712,
			lng: 76.93654909729958,
			// label: 'R', 1
			draggable: true,
			iconUrl: './assets/images/road.png',
		},
		{
			lat: 11.110992058714883,
			lng: 76.93620577454567,
			// label: 'R', 1
			draggable: true,
			iconUrl: './assets/images/road.png',
		},
		{
			lat: 11.111413167677226,
			lng: 76.93624868988991,
			// label: 'R', 1
			draggable: true,
			iconUrl: './assets/images/road.png',
		},
		{
			lat: 11.11166583276284,
			lng: 76.9361199438572,
			// label: 'R', 1
			draggable: true,
			iconUrl: './assets/images/road.png',
		},
		{
			lat: 11.11200271920327,
			lng: 76.93603411316872,
			// label: 'R', 1
			draggable: true,
			iconUrl: './assets/images/road.png',
		},
		{
			lat: 11.112381715983705,
			lng: 76.93594828248024,
			// label: 'R', 1
			draggable: true,
			iconUrl: './assets/images/road.png',
		},
		{
			lat: 11.113013376190196,
			lng: 76.93581953644753,
			// label: 'R', 1
			draggable: true,
			iconUrl: './assets/images/road.png',
		},
		{
			lat: 11.113687145569402,
			lng: 76.93573370575905,
			// label: 'R', 1
			draggable: true,
			iconUrl: './assets/images/road.png',
		},
		{
			lat: 11.113785842125118,
			lng: 76.93569079041481,
			// label: 'R', 1
			draggable: true,
			iconUrl: './assets/images/road.png',
		},
		{
			lat: 11.113785842125118,
			lng: 76.93569079041481,
			// label: 'R', 1
			draggable: true,
			iconUrl: './assets/images/road.png',
		}
	]
	croodWaste: marker[] = [
		{
			lat: 11.026713980267784,
			lng: 76.95125699043274,
			// label: 'R', 11.0242557!4d76.9511819
			draggable: false,
			iconUrl: './assets/images/pd.png',
		},
		{
			lat: 11.026619203399033,
			lng: 76.95125699043274,
			// label: 'R', 11.0242557!4d76.9511819
			draggable: false,
			iconUrl: './assets/images/pd.png',
		},
		{
			lat: 11.026513895731272,
			lng: 76.9512677192688,
			// label: 'R', 11.0242557!4d76.9511819
			draggable: false,
			iconUrl: './assets/images/pd.png',
		},
		{
			lat: 11.026029479973753,
			lng: 76.9513213634491,
			// label: 'R', 11.0242557!4d76.9511819
			draggable: false,
			iconUrl: './assets/images/pd.png',
		},
		{
			lat: 11.026029479973753,
			lng: 76.9513213634491,
			// label: 'R', 11.0242557!4d76.9511819
			draggable: false,
			iconUrl: './assets/images/pd.png',
		},
		{
			lat: 11.025892579723738,
			lng: 76.95134282112122,
			// label: 'R', 11.0242557!4d76.9511819
			draggable: false,
			iconUrl: './assets/images/pd.png',
		},
		{
			lat: 11.02576621020561,
			lng: 76.95135354995728,
			// label: 'R', 11.0242557!4d76.9511819
			draggable: false,
			iconUrl: './assets/images/pd.png',
		},
		{
			lat: 11.025681963830008,
			lng: 76.95135354995728,
			// label: 'R', 11.0242557!4d76.9511819
			draggable: false,
			iconUrl: './assets/images/pd.png',
		},
		{
			lat: 11.025576655826585,
			lng: 76.95134282112122,
			// label: 'R', 11.0242557!4d76.9511819
			draggable: false,
			iconUrl: './assets/images/pd.png',
		},
		{
			lat: 11.025439755365753,
			lng: 76.95136427879333,
			// label: 'R', 11.0242557!4d76.9511819
			draggable: false,
			iconUrl: './assets/images/pd.png',
		},
		{
			lat: 11.025334447275576,
			lng: 76.95140719413757,
			// label: 'R', 11.0242557!4d76.9511819
			draggable: false,
			iconUrl: './assets/images/pd.png',
		}
	]
	croodWaste1: marker[] = [
		{
			lat: 11.02298080162062,
			lng: 76.95168111473322,
			// label: 'R', 11.0242557!4d76.9511819
			draggable: false,
			iconUrl: './assets/images/pu.png',
		},
		{
			lat: 11.02310717233563,
			lng: 76.9516596570611,
			// label: 'R', 11.0242557!4d76.9511819
			draggable: false,
			iconUrl: './assets/images/pu.png',
		},
		{
			lat: 11.023212481223323,
			lng: 76.95164892822504,
			// label: 'R', 11.0242557!4d76.9511819
			draggable: false,
			iconUrl: './assets/images/pu.png',
		},
		{
			lat: 11.023307259190016,
			lng: 76.95162747055292,
			// label: 'R', 11.0242557!4d76.9511819
			draggable: false,
			iconUrl: './assets/images/pu.png',
		},
		{
			lat: 11.023402037126163,
			lng: 76.95158455520868,
			// label: 'R', 11.0242557!4d76.9511819
			draggable: false,
			iconUrl: './assets/images/pu.png',
		},
		{
			lat: 11.023538938535578,
			lng: 76.9516060128808,
			// label: 'R', 11.0242557!4d76.9511819
			draggable: false,
			iconUrl: './assets/images/pu.png',
		},
		{
			lat: 11.023654778139912,
			lng: 76.95161674171686,
			// label: 'R', 11.0242557!4d76.9511819
			draggable: false,
			iconUrl: './assets/images/pu.png',
		},
		{
			lat: 11.023760086831544,
			lng: 76.9516060128808,
			// label: 'R', 11.0242557!4d76.9511819
			draggable: false,
			iconUrl: './assets/images/pu.png',
		},
		{
			lat: 11.023886457211718,
			lng: 76.95158455520868,
			// label: 'R', 11.0242557!4d76.9511819
			draggable: false,
			iconUrl: './assets/images/pu.png',
		},
		{
			lat: 11.0240022966792,
			lng: 76.95153091102839,
			// label: 'R', 11.0242557!4d76.9511819
			draggable: false,
			iconUrl: './assets/images/pu.png',
		},
		{
			lat: 11.0240022966792,
			lng: 76.95153091102839,
			// label: 'R', 11.0242557!4d76.9511819
			draggable: false,
			iconUrl: './assets/images/pu.png',
		}
	]
	croodCamera: marker[] = [
		{
			lat: 11.007168237680968,
			lng: 76.97778940200806,
			// label: 'R',
			draggable: true,
			iconUrl: './assets/images/policeR.png',
		},
		{
			lat: 11.006620601203988,
			lng: 76.97746753692627,
			// label: 'R',
			draggable: true,
			iconUrl: './assets/images/policeR.png',
		},
		{
			lat: 11.006346782583705,
			lng: 76.97633028030396,
			// label: 'R',
			draggable: true,
			iconUrl: './assets/images/policeR.png',
		},
		{
			lat: 11.006222379328396,
			lng: 76.97617001831532,
			// label: 'R',
			draggable: true,
			iconUrl: './assets/images/policeR.png',
		},
		{
			lat: 11.00607493836518,
			lng: 76.97589106857777,
			// label: 'R',
			draggable: true,
			iconUrl: './assets/images/policeR.png',
		},
		{
			lat: 11.005885371304025,
			lng: 76.9755906611681,
			// label: 'R',
			draggable: true,
			iconUrl: './assets/images/policeR.png',
		},
		{
			lat: 11.005420009980426,
			lng: 76.97472095489502,
			// label: 'R',
			draggable: true,
			iconUrl: './assets/images/policeR.png',
		},
		{
			lat: 11.005084975701106,
			lng: 76.97445340454578,
			// label: 'R',
			draggable: true,
			iconUrl: './assets/images/policeR.png',
		},
		{
			lat: 11.004747966375659,
			lng: 76.97398133575916,
			// label: 'R',
			draggable: true,
			iconUrl: './assets/images/policeR.png',
		}
	]
	croodSalem: marker[] = [
		{
			lat: 11.669011490073968,
			lng: 78.14089212566614,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/policeL.png',
		},
		{
			lat: 11.668843376484157,
			lng: 78.14102087169886,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/policeL.png',
		},
		{
			lat: 11.66878033386173,
			lng: 78.14108524471521,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/policeL.png',
		},
		{
			lat: 11.668633234353651,
			lng: 78.14125690609217,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/policeL.png',
		},
		{
			lat: 11.668513387598548,
			lng: 78.14142856746912,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/policeL.png',
		},
		{
			lat: 11.668429330665473,
			lng: 78.14157877117395,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/policeL.png',
		},
		{
			lat: 11.668324259463304,
			lng: 78.14179334789515,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/policeL.png',
		},
		{
			lat: 11.667220024391508,
			lng: 78.14296245574951,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/policeL.png',
		},
		{
			lat: 11.667220024391508,
			lng: 78.14296245574951,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/policeL.png',
		},
		{
			lat: 11.667220024391508,
			lng: 78.14296245574951,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/policeL.png',
		},
		{
			lat: 11.667220024391508,
			lng: 78.14296245574951,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/policeL.png',
		},
		{
			lat: 11.667220024391508,
			lng: 78.14296245574951,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/policeL.png',
		},
		{
			lat: 11.66707916268807,
			lng: 78.14267311245203,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/policeL.png',
		},
		{
			lat: 11.66676394742831,
			lng: 78.14301643520594,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/policeL.png',
		},
		{
			lat: 11.666553803722856,
			lng: 78.14316663891077,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/policeL.png',
		},
		{
			lat: 11.666421478776677,
			lng: 78.14345598220825,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/policeL.png',
		},
		{
			lat: 11.66627437801802,
			lng: 78.14360618591309,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/policeL.png',
		},
		{
			lat: 11.666148291591393,
			lng: 78.14375638961792,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/policeL.png',
		},
		{
			lat: 11.666001190687918,
			lng: 78.14390659332275,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/policeL.png',
		},
		{
			lat: 11.665833075274199,
			lng: 78.14412117004395,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/policeL.png',
		},
		{
			lat: 11.665706988647058,
			lng: 78.14420700073242,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/policeL.png',
		},
		{
			lat: 11.665517858598939,
			lng: 78.14435720443726,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/policeL.png',
		},
		{
			lat: 11.665307713949852,
			lng: 78.14452886581421,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/policeL.png',
		},
		{
			lat: 11.665286699476184,
			lng: 78.14482927322388,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/policeL.png',
		},
		{
			lat: 11.664908438678186,
			lng: 78.14549446105957,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/policeL.png',
		},
	]
	croodSalem1: marker[] = [
		{
			lat: 11.661170787125156,
			lng: 78.14887437969446,
			// label: 'R', 11.692560, 78.112639
			draggable: true,
			iconUrl: '../assets/images/cc1.png',
		},
		{
			lat: 11.661633112033774,
			lng: 78.14850959926844,
			// label: 'R', 11.692560, 78.112639
			draggable: true,
			iconUrl: '../assets/images/cc1.png',
		},
		{
			lat: 11.66213746560139,
			lng: 78.14818773418665,
			// label: 'R', 11.692560, 78.112639
			draggable: true,
			iconUrl: '../assets/images/cc1.png',
		},
		{
			lat: 11.66236862734698,
			lng: 78.1479087844491,
			// label: 'R', 11.692560, 78.112639
			draggable: true,
			iconUrl: '../assets/images/cc1.png',
		},
		{
			lat: 11.662599788900058,
			lng: 78.14760837703943,
			// label: 'R', 11.692560, 78.112639
			draggable: true,
			iconUrl: '../assets/images/cc1.png',
		},
		{
			lat: 11.6627679062723,
			lng: 78.14732942730188,
			// label: 'R', 11.692560, 78.112639
			draggable: true,
			iconUrl: '../assets/images/cc1.png',
		},
		{
			lat: 11.662999067492885,
			lng: 78.14717922359705,
			// label: 'R', 11.692560, 78.112639
			draggable: true,
			iconUrl: '../assets/images/cc1.png',
		},
		{
			lat: 11.663041096785044,
			lng: 78.14696464687586,
			// label: 'R', 11.692560, 78.112639
			draggable: true,
			iconUrl: '../assets/images/cc1.png',
		},
		{
			lat: 11.663482403968386,
			lng: 78.14683590084314,
			// label: 'R', 11.692560, 78.112639
			draggable: true,
			iconUrl: '../assets/images/cc1.png',
		},
		{
			lat: 11.663482403968386,
			lng: 78.14683590084314,
			// label: 'R', 11.692560, 78.112639
			draggable: true,
			iconUrl: '../assets/images/cc1.png',
		},
		{
			lat: 11.663482403968386,
			lng: 78.14683590084314,
			// label: 'R', 11.692560, 78.112639
			draggable: true,
			iconUrl: '../assets/images/cc1.png',
		},
		{
			lat: 11.663482403968386,
			lng: 78.14683590084314,
			// label: 'R', 11.692560, 78.112639
			draggable: true,
			iconUrl: '../assets/images/cc1.png',
		},
		{
			lat: 11.663482403968386,
			lng: 78.14683590084314,
			// label: 'R', 11.692560, 78.112639
			draggable: true,
			iconUrl: '../assets/images/cc1.png',
		},
		{
			lat: 11.663482403968386,
			lng: 78.14683590084314,
			// label: 'R', 11.692560, 78.112639
			draggable: true,
			iconUrl: '../assets/images/cc1.png',
		},
		{
			lat: 11.663482403968386,
			lng: 78.14683590084314,
			// label: 'R', 11.692560, 78.112639
			draggable: true,
			iconUrl: '../assets/images/cc1.png',
		},
		{
			lat: 11.663482403968386,
			lng: 78.14683590084314,
			// label: 'R', 11.692560, 78.112639
			draggable: true,
			iconUrl: '../assets/images/cc1.png',
		},
		{
			lat: 11.663482403968386,
			lng: 78.14683590084314,
			// label: 'R', 11.692560, 78.112639
			draggable: true,
			iconUrl: '../assets/images/cc1.png',
		},
		{
			lat: 11.663482403968386,
			lng: 78.14683590084314,
			// label: 'R', 11.692560, 78.112639
			draggable: true,
			iconUrl: '../assets/images/cc1.png',
		},
		{
			lat: 11.663482403968386,
			lng: 78.14683590084314,
			// label: 'R', 11.692560, 78.112639
			draggable: true,
			iconUrl: '../assets/images/cc1.png',
		},
		{
			lat: 11.663482403968386,
			lng: 78.14683590084314,
			// label: 'R', 11.692560, 78.112639
			draggable: true,
			iconUrl: '../assets/images/cc1.png',
		},
		{
			lat: 11.663482403968386,
			lng: 78.14683590084314,
			// label: 'R', 11.692560, 78.112639
			draggable: true,
			iconUrl: '../assets/images/cc1.png',
		},
		{
			lat: 11.663482403968386,
			lng: 78.14683590084314,
			// label: 'R', 11.692560, 78.112639
			draggable: true,
			iconUrl: '../assets/images/cc1.png',
		},
		{
			lat: 11.663482403968386,
			lng: 78.14683590084314,
			// label: 'R', 11.692560, 78.112639
			draggable: true,
			iconUrl: '../assets/images/cc1.png',
		},
		{
			lat: 11.663482403968386,
			lng: 78.14683590084314,
			// label: 'R', 11.692560, 78.112639
			draggable: true,
			iconUrl: '../assets/images/cc1.png',
		},
		{
			lat: 11.663482403968386,
			lng: 78.14683590084314,
			// label: 'R', 11.692560, 78.112639
			draggable: true,
			iconUrl: '../assets/images/cc1.png',
		}

	]
	croodThanjavur: marker[] = [
		{
			lat: 10.83640403038065,
			lng: 78.81181955337524,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/flood3.png',
		},
		{
			lat: 10.836319730213472,
			lng: 78.81222724914551,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/flood3.png',
		},
		{
			lat: 10.836172204863804,
			lng: 78.81246328353882,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/flood3.png',
		},
		{
			lat: 10.836066829569527,
			lng: 78.81265640258789,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/flood3.png',
		},
		{
			lat: 10.836191962727352,
			lng: 78.8132468238473,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/flood3.png',
		},
		{
			lat: 10.836128737559406,
			lng: 78.81333265453577,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/flood3.png',
		},
		{
			lat: 10.836065512378124,
			lng: 78.81341848522425,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/flood3.png',
		},
		{
			lat: 10.836044437314722,
			lng: 78.81350431591272,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/flood3.png',
		},
		{
			lat: 10.835960137046289,
			lng: 78.81365451961756,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/flood3.png',
		},
		{
			lat: 10.835939061975472,
			lng: 78.81371889263391,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/flood3.png',
		},
		{
			lat: 10.835854761677345,
			lng: 78.81378326565027,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/flood3.png',
		},
		{
			lat: 10.83579153643819,
			lng: 78.81389055401087,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/flood3.png',
		},
		{
			lat: 10.835723042414015,
			lng: 78.81419096142054,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/flood3.png',
		},
		{
			lat: 10.83559659186669,
			lng: 78.8142553344369,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/flood3.png',
		},
		{
			lat: 10.835533366573006,
			lng: 78.81431970745325,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/flood3.png',
		},
		{
			lat: 10.835427991053827,
			lng: 78.81438408046961,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/flood3.png',
		},
		{
			lat: 10.835343690611797,
			lng: 78.81451282650232,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/flood3.png',
		},
		{
			lat: 10.835259390146016,
			lng: 78.8145986571908,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/flood3.png',
		},
		{
			lat: 10.83513293940283,
			lng: 78.81466303020716,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/flood3.png',
		},
		{
			lat: 10.834964338328831,
			lng: 78.81474886089563,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/flood3.png',
		},
		{
			lat: 10.834837887461017,
			lng: 78.81498489528894,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/flood3.png',
		},
		{
			lat: 10.834563910397534,
			lng: 78.81519947201014,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/flood3.png',
		}
	]
	croodTiruchirapalli: marker[] = [
		{
			lat: 10.789613792626922,
			lng: 78.69895219802856,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/pr.png',
		},
		{
			lat: 10.789592714299918,
			lng: 78.69931697845459,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/pr.png',
		},
		{
			lat: 10.789634870952451,
			lng: 78.69974613189697,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/pr.png',
		},
		{
			lat: 10.789634870952451,
			lng: 78.70008945465088,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/pr.png',
		},
		{
			lat: 10.789655949276492,
			lng: 78.70023965835571,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/pr.png',
		},
		{
			lat: 10.789782419189748,
			lng: 78.70099067687988,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/pr.png',
		},
		{
			lat: 10.78982457581568,
			lng: 78.70129108428955,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/pr.png',
		},
		{
			lat: 10.789887810743503,
			lng: 78.70159149169922,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/pr.png',
		},
		{
			lat: 10.789951045658007,
			lng: 78.70180606842041,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/pr.png',
		},
		{
			lat: 10.79005643715263,
			lng: 78.7022352218628,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/pr.png',
		},
		{
			lat: 10.790098593740144,
			lng:78.70266437530518,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/pr.png',
		},
		{
			lat: 10.790119672031679,
			lng:78.70296478271484,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/pr.png',
		}
	]
	croodTiruppur: marker[] = [
		{
			lat: 11.109029781632838,
			lng: 77.34413104131818,
			// label: 'R', 11.724581, 78.067750 
			draggable: true,
			iconUrl: '../assets/images/policeR.png',
		},
		{
			lat: 11.108977142615787,
			lng: 77.3440452106297,
			// label: 'R', 11.724581, 78.067750 
			draggable: true,
			iconUrl: '../assets/images/policeR.png',
		},
		{
			lat: 11.108924503589236,
			lng: 77.34394865110517,
			// label: 'R', 11.724581, 78.067750 
			draggable: true,
			iconUrl: '../assets/images/policeR.png',
		},
		{
			lat: 11.10886133674486,
			lng: 77.34385209158063,
			// label: 'R', 11.724581, 78.067750 
			draggable: true,
			iconUrl: '../assets/images/policeR.png',
		},
		{
			lat: 11.108855579349509,
			lng: 77.34371261671185,
			// label: 'R', 11.724581, 78.067750 
			draggable: true,
			iconUrl: '../assets/images/policeR.png',
		},
		{
			lat: 11.108792412490184,
			lng: 77.3436482436955,
			// label: 'R', 11.724581, 78.067750 
			draggable: true,
			iconUrl: '../assets/images/policeR.png',
		},
		{
			lat: 11.108708189989837,
			lng: 77.3435409553349,
			// label: 'R', 11.724581, 78.067750 
			draggable: true,
			iconUrl: '../assets/images/policeR.png',
		},
		{
			lat: 11.108645023098632,
			lng: 77.34347658231854,
			// label: 'R', 11.724581, 78.067750 
			draggable: true,
			iconUrl: '../assets/images/policeR.png',
		},
		{
			lat: 11.108602911830223,
			lng: 77.34340148046613,
			// label: 'R', 11.724581, 78.067750 
			draggable: true,
			iconUrl: '../assets/images/policeR.png',
		},
		{
			lat: 11.108592384012171,
			lng: 77.34334783628583,
			// label: 'R', 11.724581, 78.067750 
			draggable: true,
			iconUrl: '../assets/images/policeR.png',
		},
		{
			lat: 11.108560800555743,
			lng: 77.34329419210553,
			// label: 'R', 11.724581, 78.067750 
			draggable: true,
			iconUrl: '../assets/images/policeR.png',
		},
		{
			lat: 11.108541225390928,
			lng: 77.34320836141706,
			// label: 'R', 11.724581, 78.067750 
			draggable: true,
			iconUrl: '../assets/images/policeR.png',
		},
		{
			lat: 11.108499114107527,
			lng: 77.3431439884007,
			// label: 'R', 11.724581, 78.067750 
			draggable: true,
			iconUrl: '../assets/images/policeR.png',
		},
		{
			lat: 11.10843594717107,
			lng: 77.34305815771222,
			// label: 'R', 11.724581, 78.067750 
			draggable: true,
			iconUrl: '../assets/images/policeR.png',
		},
		{
			lat: 11.108404363697694,
			lng: 77.3429830558598,
			// label: 'R', 11.724581, 78.067750 
			draggable: true,
			iconUrl: '../assets/images/policeR.png',
		},
		{
			lat: 11.108362252394565,
			lng: 77.34289722517133,
			// label: 'R', 11.724581, 78.067750 
			draggable: true,
			iconUrl: '../assets/images/policeR.png',
		},
		{
			lat: 11.10828855759946,
			lng: 77.34280066564679,
			// label: 'R', 11.724581, 78.067750 
			draggable: true,
			iconUrl: '../assets/images/policeR.png',
		},
		{
			lat: 11.108246446279601,
			lng: 77.3426933772862,
			// label: 'R', 11.724581, 78.067750 
			draggable: true,
			iconUrl: '../assets/images/policeR.png',
		},
		{
			lat: 11.10820433495367,
			lng: 77.34259681776166,
			// label: 'R', 11.724581, 78.067750 
			draggable: true,
			iconUrl: '../assets/images/policeR.png',
		},
		{
			lat: 11.108183279288438,
			lng: 77.34244661405683,
			// label: 'R', 11.724581, 78.067750 
			draggable: true,
			iconUrl: '../assets/images/policeR.png',
		},
		{
			lat: 11.108141167953386,
			lng: 77.34232859686017,
			// label: 'R', 11.724581, 78.067750 
			draggable: true,
			iconUrl: '../assets/images/policeR.png',
		},
		{
			lat: 11.108120112283599,
			lng: 77.34222130849957,
			// label: 'R', 11.724581, 78.067750 
			draggable: true,
			iconUrl: '../assets/images/policeR.png',
		},
		{
			lat: 11.10810958444813,
			lng: 77.3420818336308,
			// label: 'R', 11.724581, 78.067750 
			draggable: true,
			iconUrl: '../assets/images/policeR.png',
		},
		{
			lat: 11.10808852877605,
			lng: 77.34200673177838,
			// label: 'R', 11.724581, 78.067750 
			draggable: true,
			iconUrl: '../assets/images/policeR.png',
		}
	]
	croodVellore: marker[] = [
		{
			lat: 12.916292244974647, 
			lng: 79.13259029388428,
			// label: 'R', 11.724581, 78.067750 
			draggable: true,
			iconUrl: '../assets/images/flood4.png',
		},
		{
			lat: 12.91683602770293, 
			lng:79.13241863250732,
			// label: 'R', 11.724581, 78.067750 
			draggable: true,
			iconUrl: '../assets/images/flood4.png',
		},
		{
			lat: 12.917400723898773, 
			lng: 79.13237571716309,
			draggable: true,
			iconUrl: '../assets/images/flood4.png',
		},
		{
			lat: 12.917881760392131, 
			lng: 79.13235425949097,
			draggable: true,
			iconUrl: '../assets/images/flood4.png',
		},
		{
			lat: 12.918174564760653, 
			lng: 79.13224697113037,
			draggable: true,
			iconUrl: '../assets/images/flood4.png',
		},
		{
			lat: 12.918404625095159, 
			lng: 79.13226842880249,
			draggable: true,
			iconUrl: '../assets/images/flood4.png',
		},
		{
			lat: 12.918969317745152, 
			lng: 79.13218259811401,
			draggable: true,
			iconUrl: '../assets/images/flood4.png',
		},
		{
			lat: 12.919136633840736, 
			lng: 79.13220405578613,
			draggable: true,
			iconUrl: '../assets/images/flood4.png',
		},
		{
			lat: 12.919387607773967, 
			lng: 79.13220405578613,
			draggable: true,
			iconUrl: '../assets/images/flood4.png',
		},
		{
			lat: 12.91965949591707, 
			lng: 79.13213968276978,
			draggable: true,
			iconUrl: '../assets/images/flood4.png',
		},
		{
			lat: 12.919764068200998, 
			lng: 79.13213968276978,
			draggable: true,
			iconUrl: '../assets/images/flood4.png',
		},
		{
			lat: 12.920077784790072, 
			lng: 79.13211822509766,
			draggable: true,
			iconUrl: '../assets/images/flood4.png',
		},
		{
			lat: 12.920433329781327, 
			lng: 79.1320538520813,
			draggable: true,
			iconUrl: '../assets/images/flood4.png',
		},
		{
			lat: 12.920579730512978, 
			lng: 79.13201093673706,
			draggable: true,
			iconUrl: '../assets/images/flood4.png',
		},
		{
			lat: 12.92078887426644, 
			lng: 79.13203239440918,
			draggable: true,
			iconUrl: '../assets/images/flood4.png',
		},
		{
			lat: 12.921039846539445, 
			lng: 79.13201093673706,
			draggable: true,
			iconUrl: '../assets/images/flood4.png',
		}
	]
	croodMadurai: marker[] = [
		{
			lat:9.905918880106322,
			lng: 78.1147845321832,
			// label: 'R', 11.692560, 78.112639
			draggable: true,
			iconUrl: '../assets/images/fireL.png',
		},
		{
			lat:9.905931361447278,
			lng: 78.1149369744187,
			// label: 'R', 11.692560, 78.112639
			draggable: true,
			iconUrl: '../assets/images/fireL.png',
		},
		{
			lat:9.905950801438761,
			lng: 78.11508149316387,
			// label: 'R', 11.692560, 78.112639
			draggable: true,
			iconUrl: '../assets/images/fireL.png',
		},
		{
			lat:9.905980666675381,
			lng: 78.11533593742558,
			// label: 'R', 11.692560, 78.112639
			draggable: true,
			iconUrl: '../assets/images/fireL.png',
		},
		{
			lat:9.905901441029167,
			lng: 78.11569133668036,
			// label: 'R', 11.692560, 78.112639
			draggable: true,
			iconUrl: '../assets/images/fireL.png',
		},
		{
			lat:9.905946413330721,
			lng: 78.1160998115057,
			// label: 'R', 11.692560, 78.112639
			draggable: true,
			iconUrl: '../assets/images/fireL.png',
		},
		{
			lat:9.905922974975782,
			lng: 78.11641991141039,
			// label: 'R', 11.692560, 78.112639
			draggable: true,
			iconUrl: '../assets/images/fireL.png',
		},
		{
			lat:9.905893167413929,
			lng: 78.11675786974627,
			// label: 'R', 11.692560, 78.112639
			draggable: true,
			iconUrl: '../assets/images/fireL.png',
		},
		{
			lat:9.905893167413929,
			lng: 78.11675786974627,
			// label: 'R', 11.692560, 78.112639
			draggable: true,
			iconUrl: '../assets/images/fireL.png',
		},
		{
			lat:9.905450152864384 ,
			lng: 78.11780976987654,
			// label: 'R', 11.692560, 78.112639
			draggable: true,
			iconUrl: '../assets/images/fireL.png',
		},
		{
			lat:9.905450152864384  ,
			lng: 78.11819600797469,
			// label: 'R', 11.692560, 78.112639
			draggable: true,
			iconUrl: '../assets/images/fireL.png',
		},
		{
			lat:9.905407877257858   ,
			lng: 78.11868953443343,
			// label: 'R', 11.692560, 78.112639
			draggable: true,
			iconUrl: '../assets/images/fireL.png',
		},
		{
			lat:9.905302188217727    ,
			lng: 78.11931180692488,
			// label: 'R', 11.692560, 78.112639
			draggable: true,
			iconUrl: '../assets/images/fireL.png',
		},
		{
			lat:9.905217636961112     ,
			lng: 78.11995553708846,
			// label: 'R', 11.692560, 78.112639
			draggable: true,
			iconUrl: '../assets/images/fireL.png',
		},
		{
			lat:9.905217636961112     ,
			lng: 78.11995553708846,
			// label: 'R', 11.692560, 78.112639
			draggable: true,
			iconUrl: '../assets/images/fireL.png',
		}
	]
	croodMadurai1: marker[] = [
		{
			lat: 9.909919776682882,
			lng: 78.11982679105574,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/pd.png',
		},
		{
			lat: 9.909729539002754,
			lng: 78.11996609828645,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/pd.png',
		},
		{
			lat: 9.909581576286362,
			lng: 78.11994464061434,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/pd.png',
		},
		{
			lat: 9.909225210452108,
			lng: 78.12010574079329,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/pd.png',
		},
		{
			lat: 9.908971559650247,
			lng: 78.12017011380965,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/pd.png',
		},
		{
			lat: 9.908781321420097,
			lng: 78.12017011380965,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/pd.png',
		},
		{
			lat: 9.908612220678462, 
			lng: 78.12032031751448,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/pd.png',
		},
		{
			lat: 9.908485395065014, 
			lng: 78.12032031751448,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/pd.png',
		},
		{
			lat: 9.908274018933641, 
			lng: 78.12040614820296,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/pd.png',
		},
		{
			lat: 9.908104917930462, 
			lng: 78.12047052121932,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/pd.png',
		},
		{
			lat: 9.907914679197685, 
			lng: 78.1205563519078,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/pd.png',
		},
		{
			lat: 9.907724440354567, 
			lng: 78.12059926725203,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/pd.png',
		},
		{
			lat: 9.90759761439787, 
			lng: 78.12066364026839,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/pd.png',
		},
		{
			lat: 9.90678909777125, 
			lng: 78.12096404767806,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/pd.png',
		},
		{
			lat: 9.906619996002602, 
			lng: 78.12102842069442,
			// label: 'R', 11.724581, 78.067750
			draggable: true,
			iconUrl: '../assets/images/pd.png',
		}
	]
}
// just an interface for type safety.
interface marker {
	lat: number;
	lng: number;
	label?: string;
	draggable: boolean;
	iconUrl: string;
}
$.fn.liScroll = function (settings) {
	settings = $.extend({
		travelocity: 0.05
	}, settings);
	return this.each(function () {
		var $strip = $(this);
		$strip.addClass("newsticker")
		var stripHeight = 1;
		$strip.find("li").each(function (i) {
			stripHeight += $(this, i).outerHeight(true); // thanks to Michael Haszprunar and Fabien Volpi

		});
		var $mask = $strip.wrap("<div class='mask'></div>");
		var $tickercontainer = $strip.parent().wrap("<div class='tickercontainer'></div>");
		var containerHeight = $strip.parent().parent().height();	//a.k.a. 'mask' width 	
		$strip.height(stripHeight);
		var totalTravel = stripHeight;
		var defTiming = totalTravel / settings.travelocity;	// thanks to Scott Waye		
		function scrollnews(spazio, tempo) {
			$strip.animate({ top: '-=' + spazio }, tempo, "linear", function () { $strip.css("top", containerHeight); scrollnews(totalTravel, defTiming); });
		}
		scrollnews(totalTravel, defTiming);
		$strip.hover(function () {
			$(this).stop();
		},
			function () {
				var offset = $(this).offset();
				var residualSpace = offset.top + stripHeight;
				var residualTime = residualSpace / settings.travelocity;
				scrollnews(residualSpace, residualTime);
			});
	});
};

$(function () {
	$("ul#ticker01").liScroll();
	$('#tab1.tab-pane.active').on('click', 'p', function () {
		$(this).addClass('selected').siblings().removeClass('selected');
	});
	$('#tab2.tab-pane.active').on('click', 'p', function () {
		$(this).addClass('selected').siblings().removeClass('selected');
	});
	$('#tab3.tab-pane.active').on('click', 'p', function () {
		$(this).addClass('selected').siblings().removeClass('selected');
	});
	$('#tab4.tab-pane.active').on('click', 'p', function () {
		$(this).addClass('selected').siblings().removeClass('selected');
	});
	$('#tab5.tab-pane.active').on('click', 'p', function () {
		$(this).addClass('selected').siblings().removeClass('selected');
	});
	$('#tab6.tab-pane.active').on('click', 'p', function () {
		$(this).addClass('selected').siblings().removeClass('selected');
	});

});

$(window).scroll(function () {
	var scroll = $(window).scrollTop();

	//>=, not <=
	if (scroll >= 100) {
		//clearHeader, not clearheader - caps H
		$("#incidentMapArea111").addClass("darkHeader");
	}
	if (scroll <= 10) {
		//clearHeader, not clearheader - caps H
		$("#incidentMapArea111").removeClass("darkHeader");
	}
})






