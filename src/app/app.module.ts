import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { MaterialModule } from './../material/material.module';
import { HttpModule } from '@angular/http';
import { RoutingModule } from './app-routig/routing-module';
import {NgxChartsModule} from '@swimlane/ngx-charts';
import { Ng2GoogleChartsModule } from 'ng2-google-charts';
import {GoogleChart} from './directives/angular2-google-chart.directive';
import { ChartModule } from 'angular2-highcharts';
import { HighchartsStatic } from 'angular2-highcharts/dist/HighchartsService';
import { AppComponent } from './app.component';
import { DigitalComponent } from './digital/digital.component';
import { HeaderComponentComponent } from './header-component/header-component.component';
import { FooterComponentComponent } from './footer-component/footer-component.component';
import { SliderComponentComponent } from './slider-component/slider-component.component';
import { LoginComponentComponent } from './login-component/login-component.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PressureComponent } from './pressure/pressure.component';
import { HumidityComponent } from './humidity/humidity.component';
import { TemperatureComponent } from './temperature/temperature.component';
import { MapComponent } from './map/map.component';
import { AgmCoreModule } from '@agm/core';
//import { AgmJsMarkerClustererModule } from '@agm/js-marker-clusterer';

import { DeviceInfoComponent } from './device-info/device-info.component';
import { PortalComponent } from './portal/portal.component';
import { CityComponent } from './city/city.component';
import { SmartCitiesDashboardComponent } from './smart-cities-dashboard/smart-cities-dashboard.component';
import { WeatherDashboardComponent } from './weather-dashboard/weather-dashboard.component';
import { DigitalAssetDashboardComponent } from './digital-asset-dashboard/digital-asset-dashboard.component';
import { IncidentDashboardComponent } from './incident-dashboard/incident-dashboard.component';
import { NaturalComponent } from './natural/natural.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { TabsAreaComponent } from './tabs-area/tabs-area.component';
import { ParentComponent } from './parent/parent.component';
import { NestComponent } from './nest/nest.component';
import { NetStreamingComponent } from './net-streaming/net-streaming.component';
import { EnvironmentComponent } from './environment/environment.component';
import { DialogDataExampleDialogComponent } from './dialog-data-example-dialog/dialog-data-example-dialog.component';
import { IcccComponent } from './iccc/iccc.component';
import { StateLevelComponent } from './state-level/state-level.component'; 

import {GetDigitalAssetType} from './digital-asset-dashboard/digital-asset-dashboard.services';
import { EnviromentServiceDataService} from './environment/enviroment-service-data.service'



// import { ChartModule } from 'angular-highcharts';
declare var require: any;
export function highchartsFactory() {
  const hc = require('highcharts/highstock');
  const dd = require('highcharts/modules/exporting');
  dd(hc);
  return hc;
  }
@NgModule({
  declarations: [
    AppComponent,
    DigitalComponent,
    HeaderComponentComponent,
    FooterComponentComponent,
    SliderComponentComponent,
    LoginComponentComponent,
    DashboardComponent,
    GoogleChart,
    PressureComponent,
    HumidityComponent,
    TemperatureComponent,
    MapComponent,
    DeviceInfoComponent,
    PortalComponent,
    CityComponent,
    SmartCitiesDashboardComponent,
    WeatherDashboardComponent,
    DigitalAssetDashboardComponent,
    IncidentDashboardComponent,
    NaturalComponent,
    TabsAreaComponent,
    ParentComponent,
    NestComponent,
    NetStreamingComponent,
    EnvironmentComponent,
    DialogDataExampleDialogComponent,
    IcccComponent,
    StateLevelComponent
  ],
  imports: [
    
    BrowserModule,
    RoutingModule,
    Ng2GoogleChartsModule,
    BrowserAnimationsModule,
    MaterialModule,
    HttpModule,
    NgxChartsModule,
    ChartModule,
    FormsModule,
    ReactiveFormsModule,
    AgmCoreModule.forRoot({
      // please get your own API key here:
      // https://developers.google.com/maps/documentation/javascript/get-api-key?hl=en
      apiKey: 'AIzaSyAYZQRDl6BO70jUXhVUAyqcCbcgaR0trAM'
    }),
    
  ],
  providers: [
    {
    provide: HighchartsStatic,
    useFactory: highchartsFactory,
    },
    GetDigitalAssetType,
    EnviromentServiceDataService
    ],
  bootstrap: [AppComponent]
})
export class AppModule { }
platformBrowserDynamic().bootstrapModule(AppModule)