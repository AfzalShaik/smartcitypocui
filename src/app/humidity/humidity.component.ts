import { Component, OnInit } from '@angular/core';
import { GraphsService } from '../../services/graphs/graphs.service';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxChartsModule } from '@swimlane/ngx-charts';

@Component({
  selector: 'app-humidity',
  templateUrl: './humidity.component.html',
  styleUrls: ['./humidity.component.css'],
  providers: [GraphsService]
})
export class HumidityComponent implements OnInit {

  airPreData: any;
  messageSuccess: boolean;
  single: any[];
  single1: any[];
  multi: any[];
  colorScheme1: any;
  view: any[] = [200, 250];

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = true;
  showLegend = false;
  showXAxisLabel = true;
  xAxisLabel = '';
  showYAxisLabel = true;
  yAxisLabel = '';
  showGridLines = true;
  animations = false;
  yScaleMax = 200;
  activeEntries = {};
  colorScheme = {
    domain: ['#C7B42C']
  };
  constructor(private GS: GraphsService) {
    // Object.assign(this, { single });
    setInterval(() => { this.getStreaming(); }, 1000 * 6 * 1);
  }
  onSelect(event) {
    // console.log('eventttt', single);
    // if (event.value === 100) {
    //   this.colorScheme = {
    //     domain: ['#C7B42C', '#A10A28', '#C7B42C', '#AAAAAA']
    //   };
    // }
    // var that = this; // no need of this line
    this.messageSuccess = true;

    // setTimeout(() => {
    //   this.multiCall();
    //   this.constructor();
    // }, 6000);
  }
  // tslint:disable-next-line:member-ordering
  customColors = [
    {
      name: 'Humidity',
      value: ['#A10A28']
    },
    {
      name: 'Humidity',
      value: ['#C7B42C']
    }
  ];

  ngOnInit() {
    // this.GS.getAirData().subscribe(data => {
    //   for (let i = 0; i < data.length; i++) {
    //     // console.log('000000000 ', data);
    //     const dataAir = {
    //       name: Number('Pressure'),
    //       value: Number(data[i].airPressure)
    //     };
    //     this.airPreData.push(dataAir);
    //   }
    //   // console.log('airrrrrrrrrrrrrrrrrrrr ', this.airPreData);
    // });
}
getStreaming() {
  this.GS.getHumidity().subscribe(data => {
    // console.log('pressure000000000000000000', data);
    if (data[0].value > 150) {
      // alert('Air pressure got increased' + data[0].value);
      this.colorScheme1 = {
        domain: ['#A10A28']
      };
      this.colorScheme = this.colorScheme1;
    } else if (data[0].value > 100 && data[0].value < 150) {
      this.colorScheme = {
        domain: ['#ffff99']
      };
    } else {
      this.colorScheme = {
        domain: ['#a6ff4d']
      };
    }
    this.single = data;
  });
}

}
