import { Component, NgModule, OnInit } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgxChartsModule } from '@swimlane/ngx-charts';

@Component({
  selector: 'app-device-info',
  templateUrl: './device-info.component.html',
  styleUrls: ['./device-info.component.css']
})
export class DeviceInfoComponent implements OnInit {

  
  single: any[];
  multi: any[];

  view: any[] = [350, 300];

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = false;
  showXAxisLabel = true;
  xAxisLabel = 'Device';
  showYAxisLabel = true;
  yAxisLabel = '';

  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };

  constructor() {
    // Object.assign(this, { single })
    this.single = [
      {
        'name': 'Active',
        'value': 6
      },
      {
        'name': 'In Active',
        'value': 4
      },
      {
        'name': 'Emergency',
        'value': 10
      }
    ];
  }
  ngOnInit() {
  }
  onSelect(event) {
    console.log(event);
  }

}
