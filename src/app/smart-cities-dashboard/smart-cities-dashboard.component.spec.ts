import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmartCitiesDashboardComponent } from './smart-cities-dashboard.component';

describe('SmartCitiesDashboardComponent', () => {
  let component: SmartCitiesDashboardComponent;
  let fixture: ComponentFixture<SmartCitiesDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmartCitiesDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmartCitiesDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
