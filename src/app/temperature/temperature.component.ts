import { Component, NgModule, OnInit } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { GraphsService } from './../../services/graphs/graphs.service';

@Component({
  selector: 'app-temperature',
  templateUrl: './temperature.component.html',
  styleUrls: ['./temperature.component.css'],
  providers: [GraphsService]
})
export class TemperatureComponent implements OnInit {

  colorName: string;
  color: number;
  seriesData: number;
  randomNumber: number;
  single: any[];
  multi: any[];
  multi1: any[];
  view: any[] = [200, 250];

  // options
  showXAxis = false;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Heat Input';
  showYAxisLabel = true;
  yAxisLabel = '';

  colorScheme = {
    domain: ['#5AA454', '#C7B42C', '#A10A28', '#AAAAAA']
  };

  constructor(private GS: GraphsService) {
    setInterval(() => { this.getStreaming(); }, 1000 * 6 * 1);
    // Object.assign(this, { multi });

  }
// ............................
options: Object;
onChartload (e) {
  e.context.renderer.image('http://www.clker.com/cliparts/p/W/e/k/U/d/blank-fundraising-thermometer.svg', 24, 0, 110, 510).add();

}
// .............................
  onSelect(event) {
    console.log(event);
  }

  ngOnInit() {
  }
  getStreaming() {
    // alert('ji');
    this.GS.getHeat().subscribe(data => {
      console.log('temppppppppppppppppppp', data);
      this.multi1 = data;
    });

    // .........................graph.......................
    this.randomNumber = Math.floor(Math.random() * (2.5 - 1)) + 1;
    this.seriesData = 4 + this.randomNumber;
    if (this.seriesData > 7.5) {
      this.colorName = '#ff0000';
    } else if (this.seriesData > 5 && this.seriesData < 7.5) {
      this.colorName = '#ffff33';
    } else {
      this.colorName = '#80ff80';
    }
    this.options = {
      chart: {
          type: 'column',
          marginBottom: 72
      },
      credits: {
          enabled: false
      },
      title: '',
      legend: {
          enabled: false
      },
      exporting: {
          enabled: false
      },
      yAxis: {
          min: 0,
          max: 10,
          title: null,
          align: 'right'
      },
      xAxis: {
          labels: false
      },
      series: [{
          data: [this.seriesData],
          color: this.colorName
       }]
  };
    // ........................................
  }
}
