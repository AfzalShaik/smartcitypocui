import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
declare var $: any;
@Component({
  selector: 'app-header-component',
  templateUrl: './header-component.component.html',
  styleUrls: ['./header-component.component.css']
})
export class HeaderComponentComponent implements OnInit {

  constructor(private router: Router) {
    
    $(document).ready(function () {
    $("#headActive").on('click', 'li', function () {
      $("#headActive li.active").removeClass("active");
      // adding classname 'active' to current click li 
      $(this).addClass("active");
    });
  });
   }

  ngOnInit() {
    // this.router.navigate(['weather/' + 'Coimbatore']);
    
  }
  dashbord() {
    this.router.navigate(['parent/iccc/Coimbatore']);
  }
}
