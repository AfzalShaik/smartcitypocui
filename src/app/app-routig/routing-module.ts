
import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { DigitalComponent } from '../digital/digital.component';
import { PortalComponent } from '../portal/portal.component';
import { SmartCitiesDashboardComponent} from '../smart-cities-dashboard/smart-cities-dashboard.component';

import { AppComponent } from '../app.component';
import { SliderComponentComponent } from '../slider-component/slider-component.component';
import { CityComponent } from '../city/city.component';
import {WeatherDashboardComponent } from '../weather-dashboard/weather-dashboard.component';
import {DigitalAssetDashboardComponent } from '../digital-asset-dashboard/digital-asset-dashboard.component';
import {IncidentDashboardComponent} from '../incident-dashboard/incident-dashboard.component';
import {NaturalComponent } from '../natural/natural.component';
import { TabsAreaComponent } from '../tabs-area/tabs-area.component';
import { ParentComponent } from '../parent/parent.component';
import { NestComponent } from '../nest/nest.component';
import { NetStreamingComponent } from '../net-streaming/net-streaming.component';
import { EnvironmentComponent } from '../environment/environment.component';
import { IcccComponent } from '../iccc/iccc.component';
import {StateLevelComponent } from '../state-level/state-level.component';
const routing: Routes = [
        { path: '', component: SliderComponentComponent },
        { path: 'digital', component: DigitalComponent },
        { path: 'state', component: StateLevelComponent },
        { path: 'nestt', component: NestComponent },
        { path: 'portal', component: PortalComponent },
        { path: 'city', component: CityComponent},
        {path: 'smartCity', component: SmartCitiesDashboardComponent},
        {path: 'net', component: NetStreamingComponent},
        // {path: 'weather/:id', component: WeatherDashboardComponent},
        // {path: 'digitalAsset/:id', component: DigitalAssetDashboardComponent},
        {path: 'incident1/Coimbatore', component: IncidentDashboardComponent},
        {path: 'incidents/:id', component: IncidentDashboardComponent},
        {path: 'natural/:id', component:NaturalComponent},
        // {path: 'environment/:id', component:EnvironmentComponent},
        // {path: 'tabsArea', component:TabsAreaComponent},
        {
                path: 'parent', component: ParentComponent,
                children: [
                  { path: 'iccc/:id', component: IcccComponent },
                  { path: 'tabsArea/:id', component: TabsAreaComponent },
                  { path: 'digitalAsset/:id', component: DigitalAssetDashboardComponent },
                  { path: 'statlevel/:id', component: StateLevelComponent },
                  { path: 'environment/:id', component: EnvironmentComponent },
                  { path: 'environment/:id/:status', component: EnvironmentComponent },
               
                ]
              }

];



export const RoutingModule: ModuleWithProviders = RouterModule.forRoot(routing);
