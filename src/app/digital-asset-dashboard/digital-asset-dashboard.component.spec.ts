import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DigitalAssetDashboardComponent } from './digital-asset-dashboard.component';

describe('DigitalAssetDashboardComponent', () => {
  let component: DigitalAssetDashboardComponent;
  let fixture: ComponentFixture<DigitalAssetDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DigitalAssetDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DigitalAssetDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
