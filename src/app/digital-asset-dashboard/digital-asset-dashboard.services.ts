import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http'
import { Observable } from 'rxjs/Observable';  
//import {BaseURL} from '../../common-url/common-url';
// Observable class extensions
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
// Observable operators
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';

import {AppSetting} from './API-URL';
declare var $: any;
@Injectable()
export class GetDigitalAssetType {

  headers: Headers;
  options: RequestOptions;

  //private url=BaseURL.APIURL+"/RestaurantAdmins/loginWithPIN"
  constructor(private http: Http) {
    $(document).ready(function(){
      $('[data-toggle="tooltip"]').tooltip();   
  });
    this.headers = new Headers({
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    });
    this.options = new RequestOptions({ headers: this.headers });
  }

  getSmartParking(cityname:string): Observable<any> {
    return this.http.get(AppSetting.API_ENDPOINT+'SmartParkings?filter={"where":{"cityName":"'+cityname+'"}}', this.options).map(this.extractData).catch(this.handleError);
  };
  getSmartPole(cityname:string): Observable<any> {
    return this.http.get(AppSetting.API_ENDPOINT+'SmartPoles?filter={"where":{"cityName":"'+cityname+'"}}', this.options).map(this.extractData).catch(this.handleError);
  };
  getSmartWater(cityname:string): Observable<any> {
    return this.http.get(AppSetting.API_ENDPOINT+'SmartWaters?filter={"where":{"cityName":"'+cityname+'"}}', this.options).map(this.extractData).catch(this.handleError);
  };
  getSmartWIFi(cityname:string): Observable<any> {
    return this.http.get(AppSetting.API_ENDPOINT+'SmartWifis?filter={"where":{"cityName":"'+cityname+'"}}', this.options).map(this.extractData).catch(this.handleError);
  };
  getSmartWastManag(cityname:string): Observable<any> {
    return this.http.get(AppSetting.API_ENDPOINT+'WasteManagements?filter={"where":{"cityName":"'+cityname+'"}}', this.options).map(this.extractData).catch(this.handleError);
  };

  getSmartTrasp(cityname:string): Observable<any> {
    return this.http.get(AppSetting.API_ENDPOINT+'SmartTransports?filter={"where":{"cityName":"'+cityname+'"}}', this.options).map(this.extractData).catch(this.handleError);
  };


  
  private extractData(res: Response) {
    let body = res.json();
    return body || {};
  };
  private handleError(error: any) {
    let errorsample = error.json();
    let errMsg = (errorsample.error.message) ? errorsample.error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    return Observable.throw(errMsg);
  };

  
}
