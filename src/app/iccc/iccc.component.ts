import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { GraphsService } from '../../services/graphs/graphs.service';
declare var $: any;
@Component({
  selector: 'app-iccc',
  templateUrl: './iccc.component.html',
  styleUrls: ['./iccc.component.css'],
  providers: [GraphsService]
})
export class IcccComponent implements OnInit {
  sensorObj: {};
  newsTraffic: any;
  trafficArray: any[];
  fiberObject: any;
  floodObject: any;
  eventObject: any;
  trafficObject: any;
  bombObject: any;
  markersaz: ({ lat: number; lng: number; draggable: boolean; iconUrl: string; city: string; severity: string; type: string; label?: undefined; } | { lat: number; lng: number; label: string; draggable: boolean; iconUrl: string; city: string; severity: string; type: string; })[];
  textAlert: string;
  cityArray: string[];
  selectedValue: any;
  city: string;
  city1: any = [];
  totalPath: any;
  batteryHealth: any;
  smokeTest: any;
  coState: any;
  fireObject: any;
  // city: any;
  severity: string;
  type: string;
  cityName: any;
  area: any;
  areaName: any;
  paramData: any;
  zoom: any = 8.5;
  markersArray: any[];
  afzal: any[];
  // initial center position for the map 11.1271225!4d78.6568942
  lat: number = 11.3272889;
  lng: number = 78.07734;
  zoomControl: boolean = true;
  markers: marker[] = [

    {
      lat: 11.0168,
      lng: 76.9558,
      // label: 'R',
      draggable: true,
      iconUrl: '',
      city: '',
      severity: '',
      type: '',
    },
    {
      lat: 9.9252,
      lng: 78.1198,
      // label: 'R',
      draggable: true,
      iconUrl: '',
      city: '',
      severity: '',
      type: '',
    },
    {
      lat: 11.6643,
      lng: 78.1460,
      label: 'Salem',
      draggable: true,
      iconUrl: '',
      city: 'Salem',
      severity: '',
      type: '',
    },
    {
      lat: 10.7870,
      lng: 79.1378,
      // label: 'R',
      draggable: true,
      iconUrl: '',
      city: '',
      severity: '',
      type: '',
    },
    {
      lat: 10.7905,
      lng: 78.7047,
      // label: 'R',
      draggable: true,
      iconUrl: '',
      city: '',
      severity: '',
      type: '',
    },
    {
      lat: 12.9165,
      lng: 79.1325,
      // label: 'R',
      draggable: true,
      iconUrl: '',
      city: '',
      severity: '',
      type: '',
    },
    {
      lat: 8.7139,
      lng: 77.7567,
      // label: 'R',
      draggable: true,
      iconUrl: '',
      city: '',
      severity: '',
      type: '',
    },
    {
      lat: 11.1085,
      lng: 77.3411,
      // label: 'R',
      draggable: true,
      iconUrl: '',
      city: '',
      severity: '',
      type: '',
    },
    {
      lat: 8.7642,
      lng: 78.1348,
      // label: 'R',
      draggable: true,
      iconUrl: '',
      city: '',
      severity: '',
      type: '',
    },
    // salem incidents

    {
      lat: 11.684514277128242,
      lng: 77.969970703125,
      label: 'Tharamangalam',
      draggable: false,
      iconUrl: './assets/images/green.png',
      city: 'Salem',
      severity: 'Low',
      type: 'Fire',
    },
    {
      lat: 11.883477699238618,
      lng: 78.0523681640625,
      label: 'Gundikkal',
      draggable: false,
      iconUrl: './assets/images/yellow.png',
      city: 'Salem',
      severity: 'High',
      type: 'Fire',
    },
    {
      lat: 11.883477699238618,
      lng: 78.2666015625,
      draggable: false,
      label: 'Nanthan Chedu R.F',
      iconUrl: './assets/images/red.png',
      city: 'Salem',
      severity: 'Very High',
      type: 'Fire',
    },
    {
      lat: 11.754436756202463,
      lng: 78.3270263671875,
      draggable: false,
      label: 'Velampatti R.F',
      iconUrl: './assets/images/yellow.png',
      city: 'Salem',
      severity: 'High',
      type: 'Fire',
    },
    {
      lat: 11.496173992078278,
      lng: 78.1622314453125,
      draggable: false,
      iconUrl: './assets/images/green.png',
      city: 'Salem',
      severity: 'Low',
      type: 'Fire',
      label: 'Rasipuram',
    },
    {
      lat: 11.560762096743941,
      lng: 77.958984375,
      draggable: false,
      iconUrl: './assets/images/red.png',
      city: 'Salem',
      severity: 'Very High',
      type: 'Fire',
      label: 'Tamil Nadu 637102',
    },
    {
      lat: 11.749058732924906,
      lng: 77.772216796875,
      draggable: false,
      iconUrl: './assets/images/yellow.png',
      city: 'Salem',
      severity: 'High',
      type: 'Fire',
      label: 'Palamalli R.F.',
    },
    {
      lat: 11.964097286892557,
      lng: 77.8875732421875,
      draggable: false,
      iconUrl: './assets/images/red.png',
      city: 'Salem',
      severity: 'Very High',
      type: 'Fire',
      label: 'Tamil Nadu 636810',
    },
    {
      lat: 12.11452277111836,
      lng: 78.1787109375,
      draggable: false,
      iconUrl: './assets/images/yellow.png',
      city: 'Salem',
      severity: 'High',
      type: 'Fire',
      label: 'Annasagaram',
    },
    {
      lat: 12.028575662342247,
      lng: 78.3984375,
      draggable: false,
      iconUrl: './assets/images/green.png',
      city: 'Salem',
      severity: 'Low',
      type: 'Fire',
      label: 'Thurinihalli',
    },
    {
      lat: 11.861975103494535,
      lng: 78.497314453125,
      draggable: false,
      iconUrl: './assets/images/red.png',
      city: 'Salem',
      severity: 'Very High',
      type: 'Fire',
      label: 'Nachikutti R.F.',
    },
    {
      lat: 11.662996112308047,
      lng: 78.5247802734375,
      draggable: false,
      iconUrl: './assets/images/yellow.png',
      city: 'Salem',
      severity: 'High',
      type: 'Fire',
      label: 'Tamil Nadu 636117',
    },
    {
      lat: 11.533852191510546,
      lng: 78.3819580078125,
      draggable: false,
      iconUrl: './assets/images/green.png',
      city: 'Salem',
      severity: 'Low',
      type: 'Fire',
      label: 'Tamil Nadu 636202',
    },
    // Thanjavur Data
    {
      lat: 10.89264801504421,
      lng: 79.7003173828125,
      label: 'Ramanathapuram Chief',
      draggable: false,
      iconUrl: './assets/images/green.png',
      city: 'Thanjavur',
      severity: 'Low',
      type: 'Flood',
    },

    {
      lat: 10.520218543737046,
      lng: 79.1729736328125,
      label: 'Perumbur IInd Sethi',
      draggable: false,
      iconUrl: './assets/images/red.png',
      city: 'Thanjavur',
      severity: 'Very High',
      type: 'Flood',
    },

    {
      lat: 10.698394077836666,
      lng: 79.0191650390625,
      label: 'Kurumpundi',
      draggable: false,
      iconUrl: './assets/images/yellow.png',
      city: 'Thanjavur',
      severity: 'High',
      type: 'Flood',
    },
    {
      lat: 10.679501603153845,
      lng: 79.04800415039062,
      label: 'Sennampatti',
      draggable: false,
      iconUrl: './assets/images/red.png',
      city: 'Thanjavur',
      severity: 'Very High',
      type: 'Flood',
    },
    {
      lat: 10.683550089556267,
      lng: 79.11666870117188,
      label: 'Kurungulam Keelpathi',
      draggable: false,
      iconUrl: './assets/images/green.png',
      city: 'Thanjavur',
      severity: 'Low',
      type: 'Flood',
    },
    {
      lat: 11.129897277912116,
      lng: 79.6014404296875,
      label: 'Alivoikkal',
      draggable: false,
      iconUrl: './assets/images/red.png',
      city: 'Thanjavur',
      severity: 'Very High',
      type: 'Flood',
    },
    {
      lat: 10.606619641865379,
      lng: 79.5025634765625,
      label: 'Raghavambalpuram Part',
      draggable: false,
      iconUrl: './assets/images/yellow.png',
      city: 'Thanjavur',
      severity: 'High',
      type: 'Flood',
    },
    {
      lat: 10.73212771142289,
      lng: 79.21279907226562,
      label: 'Kattur',
      draggable: false,
      iconUrl: './assets/images/red.png',
      city: 'Thanjavur',
      severity: 'Very High',
      type: 'Flood',
    },
    {
      lat: 10.606619641865379,
      lng: 79.3377685546875,
      label: 'Poondi',
      draggable: false,
      iconUrl: './assets/images/red.png',
      city: 'Thanjavur',
      severity: 'Very High',
      type: 'Flood',
    },
    {
      lat: 10.900739195404606,
      lng: 79.21829223632812,
      label: 'Sarapojirajapuram',
      draggable: false,
      iconUrl: './assets/images/yellow.png',
      city: 'Thanjavur',
      severity: 'High',
      type: 'Flood',
    },
    {
      lat: 11.269999503022248,
      lng: 79.2388916015625,
      label: 'Gopurajapuram',
      draggable: false,
      iconUrl: './assets/images/yellow.png',
      city: 'Thanjavur',
      severity: 'High',
      type: 'Flood',
    },
    {
      lat: 10.956023030580504,
      lng: 79.354248046875,
      label: 'Viluthiyur',
      draggable: false,
      iconUrl: './assets/images/red.png',
      city: 'Thanjavur',
      severity: 'Low',
      type: 'Flood',
    },
    {
      lat: 10.755064433001635,
      lng: 79.29107666015625,
      label: 'Kambarnatham',
      draggable: false,
      iconUrl: './assets/images/green.png',
      city: 'Thanjavur',
      severity: 'low',
      type: 'Flood',
    },
    {
      lat: 11.038255468266152,
      lng: 79.7003173828125,
      label: 'Kannathangudi West Urachi',
      draggable: false,
      iconUrl: './assets/images/yellow.png',
      city: 'Thanjavur',
      severity: 'High',
      type: 'Flood',
    },
    {
      lat: 10.730778438271534,
      lng: 79.5025634765625,
      label: 'Thennamanadu South',
      draggable: false,
      iconUrl: './assets/images/red.png',
      city: 'Thanjavur',
      severity: 'Very High',
      type: 'Flood',
    },
    {
      lat: 10.824538548432923,
      lng: 79.0740966796875,
      label: 'Thenperambur',
      draggable: false,
      iconUrl: './assets/images/yellow.png',
      city: 'Thanjavur',
      severity: 'High',
      type: 'Flood',
    },
    {
      lat: 10.65647919455053,
      lng: 79.20483731523439,
      label: 'Rajandram',
      draggable: false,
      iconUrl: './assets/images/yellow.png',
      city: 'Thanjavur',
      severity: 'High',
      type: 'Flood',
    },
    {
      lat: 11.189179572173773,
      lng: 79.464111328125,
      label: 'Nellithope',
      draggable: false,
      iconUrl: './assets/images/green.png',
      city: 'Thanjavur',
      severity: 'Low',
      type: 'Flood',
    },
    {
      lat: 11.108337084308145,
      lng: 79.0576171875,
      label: 'Thekkur',
      draggable: false,
      iconUrl: './assets/images/red.png',
      city: 'Thanjavur',
      severity: 'Very High',
      type: 'Flood',
    },
    {
      lat: 10.827910669267117,
      lng: 79.508056640625,
      label: 'Kakkarai',
      draggable: false,
      iconUrl: './assets/images/yellow.png',
      city: 'Thanjavur',
      severity: 'High',
      type: 'Flood',
    },
    // Tiruchirapalli Data

    {
      lat: 10.590421288241638,
      lng: 78.37646484375,
      label: 'Airport Officer Quarters',
      draggable: false,
      iconUrl: './assets/images/yellow.png',
      city: 'Tiruchirapalli',
      severity: 'High',
      type: 'Traffic',
    },
    {
      lat: 10.941191793456523,
      lng: 78.760986328125,
      label: 'Irungalur',
      draggable: false,
      iconUrl: './assets/images/yellow.png',
      city: 'Tiruchirapalli',
      severity: 'High',
      type: 'Traffic',
    },
    {
      lat: 10.997816151968104,
      lng: 78.71429443359375,
      label: 'Thathamangalam',
      draggable: false,
      iconUrl: './assets/images/red.png',
      city: 'Tiruchirapalli',
      severity: 'Very High',
      type: 'Traffic',
    },
    {
      lat: 10.763159330300516,
      lng: 78.2281494140625,
      label: 'Gundur',
      draggable: false,
      iconUrl: './assets/images/green.png',
      city: 'Tiruchirapalli',
      severity: 'Low',
      type: 'Traffic',
    },
    {
      lat: 10.755064433001635,
      lng: 78.651123046875,
      label: 'Panjappur',
      draggable: false,
      iconUrl: './assets/images/red.png',
      city: 'Tiruchirapalli',
      severity: 'Very High',
      type: 'Traffic',
    },
    {
      lat: 10.701092906770022,
      lng: 78.80767822265625,
      label: 'Elandapatti',
      draggable: false,
      iconUrl: './assets/images/green.png',
      city: 'Tiruchirapalli',
      severity: 'Low',
      type: 'Traffic',
    },
    {
      lat: 11.119117380425525,
      lng: 78.255615234375,
      label: 'Palaiyur',
      draggable: false,
      iconUrl: './assets/images/green.png',
      city: 'Tiruchirapalli',
      severity: 'Low',
      type: 'Traffic',
    },
    {
      lat: 11.00860051288406,
      lng: 78.48358154296875,
      label: 'Valavandhi East',
      draggable: false,
      iconUrl: './assets/images/red.png',
      city: 'Tiruchirapalli',
      severity: 'Very High',
      type: 'Traffic',
    },
    {
      lat: 10.846793844137943,
      lng: 78.42315673828125,
      label: 'Sivayam North',
      draggable: false,
      iconUrl: './assets/images/red.png',
      city: 'Tiruchirapalli',
      severity: 'Very High',
      type: 'Traffic',
    },
    {
      lat: 10.695695224886434,
      lng: 78.46435546875,
      label: 'K.Periapatti North',
      draggable: false,
      iconUrl: './assets/images/red.png',
      city: 'Tiruchirapalli',
      severity: 'Very High',
      type: 'Traffic',
    },
    {
      lat: 11.140676776401113,
      lng: 78.69781494140625,
      label: 'Tamil Nadu 621118',
      draggable: false,
      iconUrl: './assets/images/red.png',
      city: 'Tiruchirapalli',
      severity: 'Very High',
      type: 'Traffic',
    },
    {
      lat: 11.084079962450854,
      lng: 78.49456787109375,
      label: 'Tamil Nadu 621205',
      draggable: false,
      iconUrl: './assets/images/yellow.png',
      city: 'Tiruchirapalli',
      severity: 'High',
      type: 'Traffic',
    },
    {
      lat: 11.135287077054238,
      lng: 78.0523681640625,
      label: 'Tamil Nadu 621211',
      draggable: false,
      iconUrl: './assets/images/yellow.png',
      city: 'Tiruchirapalli',
      severity: 'High',
      type: 'Traffic',
    },
    {
      lat: 10.838701200796804,
      lng: 78.2830810546875,
      label: 'Tamil Nadu 639120',
      draggable: false,
      iconUrl: './assets/images/red.png',
      city: 'Tiruchirapalli',
      severity: 'Very High',
      type: 'Traffic',
    },
    {
      lat: 10.849491343257862,
      lng: 78.53851318359375,
      label: 'Tamil Nadu 639110',
      draggable: false,
      iconUrl: './assets/images/green.png',
      city: 'Tiruchirapalli',
      severity: 'Low',
      type: 'Traffic',
    },
    {
      lat: 10.860281096281666,
      lng: 78.6566162109375,
      label: 'Mutharasanallur',
      draggable: false,
      iconUrl: './assets/images/yellow.png',
      city: 'Tiruchirapalli',
      severity: 'High',
      type: 'Traffic',
    },
    {
      lat: 10.563422129963454,
      lng: 78.7225341796875,
      label: 'Navalpattu',
      draggable: false,
      iconUrl: './assets/images/red.png',
      city: 'Tiruchirapalli',
      severity: 'Very High',
      type: 'Traffic',
    },
    {
      lat: 10.679501603153845,
      lng: 78.870849609375,
      label: 'Sengalur',
      draggable: false,
      iconUrl: './assets/images/yellow.png',
      city: 'Tiruchirapalli',
      severity: 'High',
      type: 'Traffic',
    },
    {
      lat: 10.862978473644851,
      lng: 78.8323974609375,
      label: 'Tamil Nadu 621703',
      draggable: false,
      iconUrl: './assets/images/red.png',
      city: 'Tiruchirapalli',
      severity: 'Very High',
      type: 'Traffic',
    },
    {
      lat: 10.655209552534227,
      lng: 78.60992431640625,
      label: 'Kathalur',
      draggable: false,
      iconUrl: './assets/images/green.png',
      city: 'Tiruchirapalli',
      severity: 'Low',
      type: 'Traffic',
    },
    // Tiruppur Data
    {
      lat: 10.984335146101955,
      lng: 77.3492431640625,
      label: 'Tamil Nadu 641664',
      draggable: false,
      iconUrl: './assets/images/green.png',
      city: 'Tiruppur',
      severity: 'Low',
      type: 'Public Event',
    },
    {
      lat: 11.086775297648769,
      lng: 77.5360107421875,
      label: 'Tamil Nadu 638701',
      draggable: false,
      iconUrl: './assets/images/red.png',
      city: 'Tiruppur',
      severity: 'Very High',
      type: 'Public Event',
    },
    {
      lat: 11.016688524459864,
      lng: 77.5250244140625,
      label: 'Tamil Nadu 638701',
      draggable: false,
      iconUrl: './assets/images/yellow.png',
      city: 'Tiruppur',
      severity: 'High',
      type: 'Public Event',
    },
    {
      lat: 11.329253026617318,
      lng: 77.4371337890625,
      label: 'Tamil Nadu 638110',
      draggable: false,
      iconUrl: './assets/images/red.png',
      city: 'Tiruppur',
      severity: 'Very High',
      type: 'Public Event',
    },
    {
      lat: 11.388494275632903,
      lng: 77.27783203125,
      label: 'Andipalayam',
      draggable: false,
      iconUrl: './assets/images/green.png',
      city: 'Tiruppur',
      severity: 'Low',
      type: 'Public Event',
    },
    {
      lat: 11.323866848954395,
      lng: 77.255859375,
      label: 'Vemandampalayam',
      draggable: false,
      iconUrl: './assets/images/yellow.png',
      city: 'Tiruppur',
      severity: 'High',
      type: 'Public Event',
    },
    {
      lat: 11.243062041947773,
      lng: 77.45361328125,
      label: 'Kavuthampalayam',
      draggable: false,
      iconUrl: './assets/images/yellow.png',
      city: 'Tiruppur',
      severity: 'High',
      type: 'Public Event',
    },
    {
      lat: 10.935798432254016,
      lng: 77.398681640625,
      label: 'Tamil Nadu 641667',
      draggable: false,
      iconUrl: './assets/images/yellow.png',
      city: 'Tiruppur',
      severity: 'High',
      type: 'Public Event',
    },
    {
      lat: 10.962764256386823,
      lng: 77.5689697265625,
      label: 'Tamil Nadu 638701',
      draggable: false,
      iconUrl: './assets/images/red.png',
      city: 'Tiruppur',
      severity: 'Very High',
      type: 'Public Event',
    },
    {
      lat: 10.838701200796804,
      lng: 77.3931884765625,
      label: 'Nandavanampalayam',
      draggable: false,
      iconUrl: './assets/images/green.png',
      city: 'Tiruppur',
      severity: 'Low',
      type: 'Public Event',
    },
    {
      lat: 10.87107045949965,
      lng: 77.6019287109375,
      label: 'Kambiliampatty',
      draggable: false,
      iconUrl: './assets/images/red.png',
      city: 'Tiruppur',
      severity: 'Very High',
      type: 'Public Event',
    },
    {
      lat: 10.77395218849665,
      lng: 77.6513671875,
      label: 'Tamil Nadu 638661',
      draggable: false,
      iconUrl: './assets/images/yellow.png',
      city: 'Tiruppur',
      severity: 'High',
      type: 'Public Event',
    },
    {
      lat: 10.849491343257862,
      lng: 77.5250244140625,
      label: 'Tamil Nadu 638703',
      draggable: false,
      iconUrl: './assets/images/yellow.png',
      city: 'Tiruppur',
      severity: 'High',
      type: 'Public Event',
    },
    // vellore data
    {
      lat: 12.838581915230742,
      lng: 79.1290283203125,
      label: 'Sathumadurai',
      draggable: false,
      iconUrl: './assets/images/red.png',
      city: 'Vellore',
      severity: 'Very High',
      type: 'Flood',
    },
    {
      lat: 12.849293356308959,
      lng: 79.2169189453125,
      label: 'Punganur',
      draggable: false,
      iconUrl: './assets/images/yellow.png',
      city: 'Vellore',
      severity: 'High',
      type: 'Flood',
    },
    {
      lat: 12.865359661408899,
      lng: 79.222412109375,
      label: 'Ayilam',
      draggable: false,
      iconUrl: './assets/images/yellow.png',
      city: 'Vellore',
      severity: 'High',
      type: 'Flood',
    },
    {
      lat: 13.00990996390651,
      lng: 79.332275390625,
      label: 'Ammur',
      draggable: false,
      iconUrl: './assets/images/yellow.png',
      city: 'Vellore',
      severity: 'High',
      type: 'Flood',
    },

    {
      lat: 12.940322128384626,
      lng: 78.9532470703125,
      label: 'Tamil Nadu 635803',
      draggable: false,
      iconUrl: './assets/images/green.png',
      city: 'Vellore',
      severity: 'Low',
      type: 'Flood',
    },
    {
      lat: 12.768946439455956,
      lng: 79.3597412109375,
      label: 'Padi',
      draggable: false,
      iconUrl: './assets/images/red.png',
      city: 'Vellore',
      severity: 'Very High',
      type: 'Flood',
    },
    {
      lat: 13.122279801556267,
      lng: 79.046630859375,
      label: 'Athiyur R.F',
      draggable: false,
      iconUrl: './assets/images/red.png',
      city: 'Vellore',
      severity: 'Very High',
      type: 'Flood',
    },
    {
      lat: 12.672496436545655,
      lng: 78.9532470703125,
      label: 'Tamil Nadu 632105',
      draggable: false,
      iconUrl: './assets/images/red.png',
      city: 'Vellore',
      severity: 'Very High',
      type: 'Flood',
    },
    {
      lat: 12.634978224715454,
      lng: 79.0960693359375,
      label: 'Punganur R.F',
      draggable: false,
      iconUrl: './assets/images/red.png',
      city: 'Vellore',
      severity: 'Very High',
      type: 'Flood',
    },
    {
      lat: 13.047372256948787,
      lng: 79.3707275390625,
      label: 'Tamil Nadu 632509',
      draggable: false,
      iconUrl: './assets/images/yellow.png',
      city: 'Vellore',
      severity: 'low',
      type: 'Flood',
    },
    {
      lat: 12.758231584069794,
      lng: 78.90380859375,
      label: 'Jarthankollai',
      draggable: false,
      iconUrl: './assets/images/yellow.png',
      city: 'Vellore',
      severity: 'low',
      type: 'Flood',
    },
    {
      lat: 12.672496436545655,
      lng: 79.200439453125,
      label: 'Tamil Nadu 632512',
      draggable: false,
      iconUrl: './assets/images/red.png',
      city: 'Vellore',
      severity: 'Very High',
      type: 'Flood',
    },
    {
      lat: 13.165073873513027,
      lng: 79.03564453125,
      label: 'Tamil Nadu 632517',
      draggable: false,
      iconUrl: './assets/images/red.png',
      city: 'Vellore',
      severity: 'Very High',
      type: 'Flood',
    },
    {
      lat: 12.619080032585641,
      lng: 78.7857211777009,
      label: 'Karunkali',
      draggable: false,
      iconUrl: './assets/images/green.png',
      city: 'Vellore',
      severity: 'low',
      type: 'Flood',
    },
    {
      lat: 12.806444855044726,
      lng: 78.79119873046875,
      label: 'Kilmurungai',
      draggable: false,
      iconUrl: './assets/images/green.png',
      city: 'Vellore',
      severity: 'low',
      type: 'Flood',
    },
    {
      lat: 13.031682541134396,
      lng: 78.80496285273694,
      label: 'Jangalapalli R.F',
      draggable: false,
      iconUrl: './assets/images/red.png',
      city: 'Vellore',
      severity: 'Very High',
      type: 'Flood',
    },
    // Madurai Data
    {
      lat: 9.817698206207377,
      lng: 78.10733106918633,
      label: 'Jangalapalli R.F',
      draggable: false,
      iconUrl: './assets/images/red.png',
      city: 'Madurai',
      severity: 'Very High',
      type: 'Fire',
    },
    {
      lat: 9.812285443786015,
      lng: 78.03042670944706,
      label: 'Vidathakulam',
      draggable: false,
      iconUrl: './assets/images/red.png',
      city: 'Madurai',
      severity: 'Very High',
      type: 'Fire',
    },
    {
      lat: 10.017907794431856,
      lng: 77.91507026413456,
      label: 'Tamil Nadu 625207',
      draggable: false,
      iconUrl: './assets/images/red.png',
      city: 'Madurai',
      severity: 'Very High',
      type: 'Fire',
    },
    {
      lat: 9.833935962367388,
      lng: 77.97000190475956,
      label: 'Tamil Nadu 625706',
      draggable: false,
      iconUrl: './assets/images/yellow.png',
      city: 'Madurai',
      severity: 'High',
      type: 'Fire',
    },
    {
      lat: 9.92593481025564,
      lng: 78.29409858444706,
      label: 'Idayapatty',
      draggable: false,
      iconUrl: './assets/images/red.png',
      city: 'Madurai',
      severity: 'Very High',
      type: 'Fire',
    },
    {
      lat: 10.023317182108967,
      lng: 78.23916694382206,
      label: 'Tamil Nadu 625104',
      draggable: false,
      iconUrl: './assets/images/green.png',
      city: 'Madurai',
      severity: 'low',
      type: 'Fire',
    },
    {
      lat: 9.687767593760574,
      lng: 78.20620795944706,
      label: 'Neivilakkunedungulam',
      draggable: false,
      iconUrl: './assets/images/red.png',
      city: 'Madurai',
      severity: 'Very High',
      type: 'Fire',
    },
    {
      lat: 9.70942620776938,
      lng: 77.86013862350956,
      label: 'Vairavi Ammapatti',
      draggable: false,
      iconUrl: './assets/images/yellow.png',
      city: 'Madurai',
      severity: 'High',
      type: 'Fire',
    },
    {
      lat: 9.660692360993922,
      lng: 78.21170112350956,
      label: 'Sirukulam',
      draggable: false,
      iconUrl: './assets/images/red.png',
      city: 'Madurai',
      severity: 'Very High',
      type: 'Fire',
    },
    {
      lat: 9.595702926731999,
      lng: 78.24466010788456,
      label: 'Kundukulam',
      draggable: false,
      iconUrl: './assets/images/green.png',
      city: 'Madurai',
      severity: 'low',
      type: 'Fire',
    },
    {
      lat: 10.147707616739345,
      lng: 78.29409858444706,
      label: 'Kesampatti',
      draggable: false,
      iconUrl: './assets/images/yellow.png',
      city: 'Madurai',
      severity: 'High',
      type: 'Fire',
    },
    {
      lat: 10.028726458886894,
      lng: 78.19522163132206,
      label: 'Tamil Nadu 625301',
      draggable: false,
      iconUrl: './assets/images/yellow.png',
      city: 'Madurai',
      severity: 'High',
      type: 'Fire',
    },
    {
      lat: 10.06658894754863,
      lng: 77.85464545944706,
      label: 'Nagamalai R.F',
      draggable: false,
      iconUrl: './assets/images/green.png',
      city: 'Madurai',
      severity: 'low',
      type: 'Fire',
    },
    {
      lat: 10.055771569714675,
      lng: 77.76126169133931,
      label: 'Tamil Nadu 624220',
      draggable: false,
      iconUrl: './assets/images/green.png',
      city: 'Madurai',
      severity: 'low',
      type: 'Fire',
    },
    {
      lat: 9.882644605099618,
      lng: 78.34353708196431,
      label: 'Iluppaikkudi',
      draggable: false,
      iconUrl: './assets/images/red.png',
      city: 'Madurai',
      severity: 'Very High',
      type: 'Fire',
    },
    {
      lat: 9.704011850711474,
      lng: 78.01944040227681,
      label: 'Tamil Nadu 625701',
      draggable: false,
      iconUrl: './assets/images/yellow.png',
      city: 'Madurai',
      severity: 'High',
      type: 'Fire',
    },
    {
      lat: 9.617367625764626,
      lng: 78.09634469915181,
      label: 'Thonugal',
      draggable: false,
      iconUrl: './assets/images/red.png',
      city: 'Madurai',
      severity: 'Very High',
      type: 'Fire',
    },
    {
      lat: 9.617367625764626,
      lng: 78.12381051946431,
      label: 'melapudupatti',
      draggable: false,
      iconUrl: './assets/images/red.png',
      city: 'Madurai',
      severity: 'Very High',
      type: 'Fire',
    },
    // Thoothukudi Data
    {
      lat: 8.721011856956677,
      lng: 78.13204761802501,
      label: 'Mullakadu',
      draggable: false,
      iconUrl: './assets/images/red.png',
      city: 'Thoothukudi',
      severity: 'Very High',
      type: 'Fire',
    },
    {
      lat: 8.655850339560901,
      lng: 78.09359546958751,
      label: 'Tamil Nadu 628152',
      draggable: false,
      iconUrl: './assets/images/red.png',
      city: 'Thoothukudi',
      severity: 'Very High',
      type: 'Fire',
    },
    {
      lat: 8.75901749858703,
      lng: 78.08810230552501,
      label: 'Tamil Nadu 628101',
      draggable: false,
      iconUrl: './assets/images/red.png',
      city: 'Thoothukudi',
      severity: 'Very High',
      type: 'Fire',
    },
    {
      lat: 8.639558193948728,
      lng: 77.98922535240001,
      label: 'Tamil Nadu 628752',
      draggable: false,
      iconUrl: './assets/images/yellow.png',
      city: 'Thoothukudi',
      severity: 'High',
      type: 'Fire',
    },
    {
      lat: 8.568950778498552,
      lng: 77.93978687583751,
      label: 'Tamil Nadu 628617',
      draggable: false,
      iconUrl: './assets/images/red.png',
      city: 'Thoothukudi',
      severity: 'Very High',
      type: 'Fire',
    },
    {
      lat: 8.85672841804904,
      lng: 78.06612964927501,
      label: 'Swaminatham',
      draggable: false,
      iconUrl: './assets/images/green.png',
      city: 'Thoothukudi',
      severity: 'low',
      type: 'Fire',
    },
    {
      lat: 8.62425284775043,
      lng: 78.06612964927501,
      label: 'Authoor',
      draggable: false,
      iconUrl: './assets/images/red.png',
      city: 'Thoothukudi',
      severity: 'Very High',
      type: 'Fire',
    },
    {
      lat: 8.569938423372715,
      lng: 78.11007496177501,
      label: 'Kayalpatnam',
      draggable: false,
      iconUrl: './assets/images/yellow.png',
      city: 'Thoothukudi',
      severity: 'High',
      type: 'Fire',
    },
    {
      lat: 8.488452251089136,
      lng: 77.92880054771251,
      label: 'Tamil Nadu 628701',
      draggable: false,
      iconUrl: './assets/images/red.png',
      city: 'Thoothukudi',
      severity: 'Very High',
      type: 'Fire',
    },
    {
      lat: 8.472152935517316,
      lng: 77.87386890708751,
      label: 'Tamil Nadu 628704',
      draggable: false,
      iconUrl: './assets/images/green.png',
      city: 'Thoothukudi',
      severity: 'low',
      type: 'Fire',
    },
    {
      lat: 8.551022422132794,
      lng: 78.08810230552501,
      label: 'Tamil Nadu 628201',
      draggable: false,
      iconUrl: './assets/images/yellow.png',
      city: 'Thoothukudi',
      severity: 'High',
      type: 'Fire',
    },
    {
      lat: 8.447798576439306,
      lng: 77.93978687583751,
      label: 'Pannamparai',
      draggable: false,
      iconUrl: './assets/images/yellow.png',
      city: 'Thoothukudi',
      severity: 'High',
      type: 'Fire',
    },
    {
      lat: 8.436931295659502,
      lng: 77.86288257896251,
      label: 'Tamil Nadu 628704',
      draggable: false,
      iconUrl: './assets/images/green.png',
      city: 'Thoothukudi',
      severity: 'low',
      type: 'Fire',
    },
    {
      lat: 8.99076436600749,
      lng: 78.17599293052501,
      label: 'Velidupatti',
      draggable: false,
      iconUrl: './assets/images/green.png',
      city: 'Thoothukudi',
      severity: 'low',
      type: 'Fire',
    },
    {
      lat: 9.06129120189539,
      lng: 78.09908863365001,
      label: 'Tamil Nadu 628712',
      draggable: false,
      iconUrl: './assets/images/red.png',
      city: 'Thoothukudi',
      severity: 'Very High',
      type: 'Fire',
    },
    {
      lat: 8.865952471521425,
      lng: 77.96175953208751,
      label: 'Tamil Nadu 628301',
      draggable: false,
      iconUrl: './assets/images/yellow.png',
      city: 'Thoothukudi',
      severity: 'High',
      type: 'Fire',
    },
    {
      lat: 9.031358840876253,
      lng: 78.03866382896251,
      label: 'Tamil Nadu 628401',
      draggable: false,
      iconUrl: './assets/images/red.png',
      city: 'Thoothukudi',
      severity: 'Very High',
      type: 'Fire',
    },
    {
      lat: 9.069332278600294,
      lng: 78.28585621177501,
      label: 'Tamil Nadu 628901',
      draggable: false,
      iconUrl: './assets/images/red.png',
      city: 'Thoothukudi',
      severity: 'Very High',
      type: 'Fire',
    },
    // // Tirunelveli Data
    {
      lat: 8.71656940352897,
      lng: 77.73653980552501,
      label: 'Palayamkottai',
      draggable: false,
      iconUrl: './assets/images/red.png',
      city: 'Tirunelveli',
      severity: 'Very High',
      type: 'Fire',
    },
    {
      lat: 8.711139645512432,
      lng: 77.65963550865001,
      label: 'Tamil Nadu 627010',
      draggable: false,
      iconUrl: './assets/images/red.png',
      city: 'Tirunelveli',
      severity: 'Very High',
      type: 'Fire',
    },
    {
      lat: 8.673129134468331,
      lng: 77.79147144615001,
      label: 'Krishnapuram',
      draggable: false,
      iconUrl: './assets/images/red.png',
      city: 'Tirunelveli',
      severity: 'Very High',
      type: 'Fire',
    },
    {
      lat: 8.743717009304707,
      lng: 77.84640308677501,
      label: 'Vadavallanadu',
      draggable: false,
      iconUrl: './assets/images/yellow.png',
      city: 'Tirunelveli',
      severity: 'High',
      type: 'Fire',
    },
    {
      lat: 8.651407114191278,
      lng: 77.81893726646251,
      label: 'Tamil Nadu 628809',
      draggable: false,
      iconUrl: './assets/images/red.png',
      city: 'Tirunelveli',
      severity: 'Very High',
      type: 'Fire',
    },
    {
      lat: 8.586233546972167,
      lng: 77.74203296958751,
      label: 'Tamil Nadu 627151',
      draggable: false,
      iconUrl: './assets/images/green.png',
      city: 'Tirunelveli',
      severity: 'low',
      type: 'Fire',
    },
    {
      lat: 8.597096587865959,
      lng: 77.62118336021251,
      label: 'Tennirkulam',
      draggable: false,
      iconUrl: './assets/images/red.png',
      city: 'Tirunelveli',
      severity: 'Very High',
      type: 'Fire',
    },
    {
      lat: 8.9228413155773,
      lng: 77.60470386802501,
      label: 'Tamil Nadu 627854',
      draggable: false,
      iconUrl: './assets/images/yellow.png',
      city: 'Tirunelveli',
      severity: 'High',
      type: 'Fire',
    },
    {
      lat: 8.559074605730002,
      lng: 77.65963550865001,
      label: 'Tamil Nadu 627152',
      draggable: false,
      iconUrl: './assets/images/red.png',
      city: 'Tirunelveli',
      severity: 'Very High',
      type: 'Fire',
    },
    {
      lat: 8.868570445329603,
      lng: 77.57174488365001,
      label: 'Kavalkutti Parambai R.F',
      draggable: false,
      iconUrl: './assets/images/green.png',
      city: 'Tirunelveli',
      severity: 'low',
      type: 'Fire',
    },
    {
      lat: 8.629683861533211,
      lng: 77.49484058677501,
      label: 'Malaiyankulam',
      draggable: false,
      iconUrl: './assets/images/yellow.png',
      city: 'Tirunelveli',
      severity: 'High',
      type: 'Fire',
    },
    {
      lat: 9.042208824147403,
      lng: 77.79147144615001,
      label: 'Tamil Nadu 628716',
      draggable: false,
      iconUrl: './assets/images/yellow.png',
      city: 'Tirunelveli',
      severity: 'High',
      type: 'Fire',
    },
    {
      lat: 8.572407401304085,
      lng: 77.52230640708751,
      label: 'Kalakadu R.F',
      draggable: false,
      iconUrl: './assets/images/green.png',
      city: 'Tirunelveli',
      severity: 'low',
      type: 'Fire',
    },
    {
      lat: 8.507220274389969,
      lng: 77.64315601646251,
      label: 'Tamil Nadu 627502',
      draggable: false,
      iconUrl: './assets/images/green.png',
      city: 'Tirunelveli',
      severity: 'low',
      type: 'Fire',
    },
    {
      lat: 8.919881285282186,
      lng: 77.42892261802501,
      label: 'Keelapavoor',
      draggable: false,
      iconUrl: './assets/images/red.png',
      city: 'Tirunelveli',
      severity: 'Very High',
      type: 'Fire',
    },
    {
      lat: 9.1423088829084,
      lng: 77.65414234458751,
      label: 'Palankottai',
      draggable: false,
      iconUrl: './assets/images/yellow.png',
      city: 'Tirunelveli',
      severity: 'High',
      type: 'Fire',
    },
    {
      lat: 9.10976709941839,
      lng: 77.51132007896251,
      label: 'Therkku Sankarankovil',
      draggable: false,
      iconUrl: './assets/images/red.png',
      city: 'Tirunelveli',
      severity: 'Very High',
      type: 'Fire',
    },
    {
      lat: 8.561543654175592,
      lng: 77.50582691490001,
      label: 'Kalakadu R.F',
      draggable: false,
      iconUrl: './assets/images/red.png',
      city: 'Tirunelveli',
      severity: 'Very High',
      type: 'Fire',
    },
    // Coimbatore Data 
    {
      lat: 10.914144108191373,
      lng: 76.97873257890626,
      label: 'Eachanari',
      draggable: false,
      iconUrl: './assets/images/red.png',
      city: 'Coimbatore',
      severity: 'Very High',
      type: 'Fire',
    },
    {
      lat: 10.968077242204261,
      lng: 76.76449918046876,
      label: 'Tamil Nadu 641109',
      draggable: false,
      iconUrl: './assets/images/yellow.png',
      city: 'Coimbatore',
      severity: 'High',
      type: 'Fire',
    },
    {
      lat: 11.237594436910063,
      lng: 76.81393765703126,
      label: 'Nilgiri Eastern Slope R.F',
      draggable: false,
      iconUrl: './assets/images/red.png',
      city: 'Coimbatore',
      severity: 'Very High',
      type: 'Fire',
    },
    {
      lat: 11.302321124788058,
      lng: 77.09408902421876,
      label: 'Kuppanur',
      draggable: false,
      iconUrl: './assets/images/red.png',
      city: 'Coimbatore',
      severity: 'Very High',
      type: 'Fire',
    },
    {
      lat: 10.844016403133768,
      lng: 76.89084195390626,
      label: 'Pichanur',
      draggable: false,
      iconUrl: './assets/images/green.png',
      city: 'Coimbatore',
      severity: 'Low',
      type: 'Fire',
    },
    {
      lat: 10.671404468527436,
      lng: 77.03366421953126,
      label: 'Tamil Nadu 642002',
      draggable: false,
      iconUrl: './assets/images/yellow.png',
      city: 'Coimbatore',
      severity: 'High',
      type: 'Fire',
    },
    {
      lat: 10.881779525100363,
      lng: 77.09958218828126,
      label: 'Bogampatti',
      draggable: false,
      iconUrl: './assets/images/green.png',
      city: 'Coimbatore',
      severity: 'Low',
      type: 'Fire',
    },
  ];

  clickedMarker(label: any, index: number) {
    console.log(`clicked the marker: ${JSON.stringify(label) || index}`);
    this.area = label.label;
    this.city = label.city;
    this.type = label.type;
    this.severity = label.severity;
  }

  mapClicked($event: MouseEvent) {
    console.log('helloooooooooo', $event);
  }
  mouseOver($event: MouseEvent, label: any, city: any, severity: any, type: any, infoWindow, gm) {
    this.area = label;
    this.city = city;
    this.type = type;
    this.severity = severity;
    console.log('overrrrrrrrrrrrr ', label, city, severity, type);
    if (gm.lastOpen != null) {
      gm.lastOpen.close();
    }

    gm.lastOpen = infoWindow;

    infoWindow.open();
  }

  markerDragEnd(m: marker, $event: MouseEvent) {
    console.log('dragEnd', m, $event);
  }
  constructor(private route: ActivatedRoute, private router: Router, private GS: GraphsService) {
    setInterval(() => { this.getAlerts(''); }, 1000 * 1 * 1);
    setInterval(() => { this.getSmokeSensorData(); }, 1000 * 1 * 1);
  }

  ngOnInit() {
    this.paramData = this.route.params.subscribe(params => {
      this.areaName = params.id;
    });
    this.getTwitter();
    let eventSource = window['EventSource'];
    // let source = new eventSource('https://developer-api.nest.com' + '?auth=' + 'c.XMyCgbDrcOlb0Go9kn7TdC7KSdovJ33cak2m0peNlx6q1KXvQoa58u9JAPBjDlHyXBuXqVGSbC2gLj9aqpIN8EZhtqoj7xzuO4GCSRc4Xcp13oefka35WfbnXztFxLqqboT7kkM45VgerYqd');
    let source = new eventSource('https://developer-api.nest.com' + '?auth=' + 'c.cNtVnrcVVRJu8VnGSr7mgShXcPRBgxUJrxQhgVRKJ28fqaqJBgpNZsB5krf5cf2nUpVCyfyZMwDBWj9kXYpj44V6CU3HEbMStqFKKwJ9HcokyCTVI3zXzXLQgUZbiHZJf69qsntLxBBkkHsu');
    source.addEventListener('put', function (e) {
      // console.log('putttttttttttttt ', JSON.parse(e.data).data.devices.smoke_co_alarms['Wct2ztIDRfDOyNlmmprffOxgySsCgM0k']);
      // debugger;
      this.totalPath = JSON.parse(e.data).data.devices.smoke_co_alarms['Wct2ztIDRfDOyNlmmprffOxgySsCgM0k'];
      this.batteryHealth = this.totalPath['battery_health']
      this.smokeTest = this.totalPath['smoke_alarm_state']
      this.coState = this.totalPath['co_alarm_state']
      console.log('battery health ', this.batteryHealth, this.smokeTest, this.coState);
      if (this.smokeTest == 'emergency' || this.coState == 'emergency ') {
        $('#popUpSmoke').modal('show');
      }
      this.totalPath1 = JSON.parse(e.data).data.devices.cameras['AZR_ev4x0z7P8or8XNkSBiMImWHDIZau_gAQr4CBARQa1h2qd6y4ag'].last_event;
      this.hasSound = this.totalPath1.has_sound;
      this.hasPerson = this.totalPath1.has_person;
      this.hasMotion = this.totalPath1.has_motion;
      if (this.hasSound == true && this.hasPerson == true && this.hasMotion == true) {
        $('#waterlevelPopUpCam').modal('show');
      }
    });

  }
  getSmokeSensorData() {
    this.GS.getRealSensor().subscribe(data => {
      
      if (data.length > 0) {
        for(let i =0; i < data.length; i++) {
          this.sensorObj = {
            "citizenId": "5af03877aa9cae2e42c1e62b",
            "citizenName": "sairam",
            "citizenMobileNumber": "7799849440",
            "areaName": "R S Puram",
            "department": "Home",
            "description": "Smoke Sensor" + data[i].id,
            "alertType": "Fire",
            "city": "Coimbatore",
            "read": "No",
            "count": 1
          }
          this.GS.postTwitter(this.sensorObj).subscribe(data => {
            console.log('postt', JSON.stringify(data));
          });
        }
        
      } else {
        
      }
    });
  }
  getTwitter() {
    this.GS.getTwitterData('TrafficDemo').subscribe(data => {

      let statuses = data.data.statuses;
      // console.log('statuTrafficDemosssssss ', statuses);
      if (statuses.length > 0) {
        for (let i = 0; i < statuses.length; i++) {
          // debugger;
          let all = statuses[i].text;
          this.newsTraffic = all;
          // fire
          let expr = 'Traffic';
          let expr1 = 'traffic';
          let trafficVar1 = all.search(expr);
          let trafficVar111 = all.search(expr1);
          let trafficPostObj = {};
          let res = all.replace("#", "");
          trafficPostObj = {
            "citizenId": "5af03877aa9cae2e42c1e62b",
            "citizenName": "sairam",
            "citizenMobileNumber": "7799849440",
            "areaName": "Gandhipuram",
            "department": "Police",
            "description": res,
            "alertType": "City Traffic",
            "city": "Coimbatore",
            "read": "No",
            "count": 1
          }
          console.log('objjj ', JSON.stringify(trafficPostObj));
          this.GS.postTwitter(trafficPostObj).subscribe(dataPost => {
            // if (trafficVar1 >= 0 || trafficVar111 >= 0 && dataPost[i].read == 'No') {
            // $('#waterlevelPopUpTraffic').modal('show');
            //     this.markersArray = [];
            // this.markersArray = this.markers;
            //     this.markersArray.push({
            //       lat: 10.841398772901652,
            //       lng: 77.10916983398442,
            //       id: data[i].id,
            //       label: 'Tamil Nadu 606803',
            //       draggable: false,
            //       iconUrl: './assets/images/reds.gif',
            //       city: 'Coimbatore',
            //       severity: 'Very High',
            //       type: 'City Traffic',
            //     });
            // }
          });
        }
        this.getCityIncidents();
      }
    });
  }
  getCityIncidents() {
    this.GS.getIncidents('City Traffic').subscribe(data => {
      if (data.length > 0) {
        this.trafficArray = [];
        for (var i = 0; i < data.length; i++) {
          this.trafficArray.push(data[i])
        }
      }
    });
  }
  smokeDetail() {
    $('#popUpSmoke').modal('hide');
    this.markersArray = [];
    this.markersArray = this.markers;
    this.markersArray.push({
      lat: 11.083461849849636,
      lng: 77.02677237304692,
      label: 'Bogampatti',
      label1: 'Fire',
      draggable: false,
      iconUrl: './assets/images/reds.gif',
      city: 'Bharathiyar',
      severity: 'Very High',
      type: 'Fire',
    });

  }
  incidentChange(x) {
    console.log('xxxxxxxxxxxxx ', x);
    // debugger;
    if (x.city == 'Coimbatore') {
      if (x.type == 'Surveillance') {
        this.router.navigate(['/parent/tabsArea/' + 'CoimbatoreBomb']);
        this.bombObject = x;
        this.bombObject.read = 'Yes';
        this.GS.updateIncident(this.bombObject).subscribe(data => {
          // console.log('update incidenttttttttt ', data);
        });
      } if (x.type == 'City Traffic') {
        this.router.navigate(['/parent/tabsArea/' + 'CoimbatoreTraffic']);
        this.trafficObject = x;
        this.trafficObject.read = 'Yes';
        console.log('cuty cutyyyyyyyyyyy ', this.trafficObject);
        this.GS.updateIncident(this.trafficObject).subscribe(data => {
        });
      } if (x.type == 'Smart public events management') {
        this.router.navigate(['/parent/tabsArea/' + 'CoimbatoreEvent']);
        this.eventObject = x;
        this.eventObject.read = 'Yes';
        this.GS.updateIncident(this.eventObject).subscribe(data => {
        });
      }
      if (x.type == 'Flooding') {
        this.router.navigate(['/parent/tabsArea/' + 'CoimbatoreFlood']);
        this.floodObject = x;
        this.floodObject.read = 'Yes';
        this.GS.updateIncident(this.floodObject).subscribe(data => {
        });
      }
      if (x.type == 'Fiber') {
        this.router.navigate(['/parent/tabsArea/' + 'CoimbatoreFiber']);
        this.fiberObject = x;
        this.fiberObject.read = 'Yes';
        this.GS.updateIncident(this.fiberObject).subscribe(data => {
        });
      }
      if (x.type == 'Fire') {
        this.router.navigate(['/parent/tabsArea/' + 'Bharathiyar']);
        this.fireObject = x;
        this.fireObject.read = 'Yes';
        this.GS.updateIncident(this.fireObject).subscribe(data => {
        });
      }
    } else {
      this.router.navigate(['/parent/tabsArea/' + x.city]);
    }
    this.getAlerts(x);
  }
  // get alert
  getAlerts(x) {
    this.GS.getAlerts().subscribe(data => {
      // debugger;
      this.markersArray = [];
      this.markersArray = this.markers;
      for (var i = 0; i < data.length; i++) {
        // debugger;
        // alert(data[i].alertType + data[i].city); 
        if (data[i].city == 'Coimbatore') {
          // debugger;
          if (data[i].read == 'No' && data[i].alertType == "Surveillance") {
            // alert(data[i].id)
            this.markersArray.push({
              lat: 10.924392969217713,
              lng: 76.79056631835942,
              label: 'Tamil Nadu 606803',
              id: data[i].id,
              draggable: false,
              iconUrl: './assets/images/reds.gif',
              city: 'Coimbatore',
              severity: 'Very High',
              type: 'Surveillance',
            });
          }

          if (data[i].read == 'No' && data[i].alertType == "City Traffic") {

            this.markersArray.push({
              lat: 10.841398772901652,
              lng: 77.10916983398442,
              id: data[i].id,
              label: 'Tamil Nadu 606803',
              draggable: false,
              iconUrl: './assets/images/reds.gif',
              city: 'Coimbatore',
              severity: 'Very High',
              type: 'City Traffic',
            });
            console.log('idddd', data[i].id)
          }
          if (data[i].read == 'No' && data[i].alertType == "Smart public events management") {
            this.markersArray.push({
              lat: 10.740953629420865,
              lng: 77.14212881835942,
              label: 'Tamil Nadu 606803',
              id: data[i].id,
              draggable: false,
              iconUrl: './assets/images/reds.gif',
              city: 'Coimbatore',
              severity: 'Very High',
              type: 'Smart public events management',
            });
          }
          if (data[i].read == 'No' && data[i].alertType == "Flooding") {
            this.markersArray.push({
              lat: 11.175089468312557,
              id: data[i].id,
              lng: 76.95810782226567,
              label: 'Tamil Nadu 606803',
              draggable: false,
              iconUrl: './assets/images/reds.gif',
              city: 'Coimbatore',
              severity: 'Very High',
              type: 'Flooding',
            });
          }
          if (data[i].read == 'No' && data[i].alertType == "Fiber") {
            this.markersArray.push({
              lat: 11.320556115852538,
              id: data[i].id,
              lng: 76.96085440429692,
              label: 'Tamil Nadu 606803',
              draggable: false,
              iconUrl: './assets/images/reds.gif',
              city: 'Coimbatore',
              severity: 'Very High',
              type: 'Fiber',
            });
          }
         
          if (data[i].read == 'No' && data[i].alertType == "Fire") {
            // debugger;
            this.markersArray.push({
              lat: 10.662063628424413,
              id: data[i].id,
              lng: 76.63886883015414,
              label: 'Bharathiyar',
              draggable: false,
              iconUrl: './assets/images/reds.gif',
              city: 'Coimbatore',
              severity: 'Very High',
              type: 'Fire',
            });
          }
        }
        if (data[i].city == 'Salem') {
          if (data[i].read == 'No') {
            this.markersArray.push({
              lat: 11.665069122575556,
              lng: 78.27921377929692,
              label: 'Gurthirayan R.F',
              draggable: false,
              iconUrl: './assets/images/reds.gif',
              city: 'Salem',
              severity: 'Very High',
              type: 'Fire',
            });
          }
        }
        if (data[i].city == 'Thanjavur') {
          if (data[i].read == 'No') {
            this.markersArray.push({
              lat: 10.843477706331905,
              lng: 79.23477172851562,
              label: 'Vadalur',
              draggable: false,
              iconUrl: './assets/images/reds.gif',
              city: 'Thanjavur',
              severity: 'Very High',
              type: 'Flood',
            });
          }
        }
        if (data[i].city == 'Tiruchirapalli') {
          if (data[i].read == 'No') {
            this.markersArray.push({
              lat: 10.975627907607832,
              lng: 78.59232413085942,
              label: 'Vellanur',
              draggable: false,
              iconUrl: './assets/images/reds.gif',
              city: 'Tiruchirapalli',
              severity: 'Very High',
              type: 'Traffic',
            });
          }
        }
        if (data[i].city == 'Tiruppur') {
          if (data[i].read == 'No') {
            this.markersArray.push({
              lat: 11.118499341986917,
              lng: 77.44699942382817,
              label: 'Talamalai R.F',
              draggable: false,
              iconUrl: './assets/images/reds.gif',
              city: 'Tiruppur',
              severity: 'Very High',
              type: 'Rally',
            });
          }
        }
        if (data[i].city == 'Madurai') {
          if (data[i].read == 'No') {
            this.markersArray.push({
              lat: 10.271601166444524,
              lng: 78.01694058671876,
              label: 'Sirumalai R.F',
              draggable: false,
              iconUrl: './assets/images/reds.gif',
              city: 'Madurai',
              severity: 'Very High',
              type: 'Fire',
            });
          }
        }
        if (data[i].city == 'Vellore') {
          if (data[i].read == 'No') {
            this.markersArray.push({
              lat: 12.968789722777961,
              lng: 79.18287293737069,
              label: 'Tamil Nadu 606803',
              draggable: false,
              iconUrl: './assets/images/reds.gif',
              city: 'Vellore',
              severity: 'Very High',
              type: 'Flood',
            });
          }
        }

      }
    });
    // }

  }

}
interface marker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
  iconUrl: string;
  city: string;
  severity: any;
  type: any;
}
interface marker1 {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
  iconUrl: string;
  city: string;
  severity: any;
  type: any;
}